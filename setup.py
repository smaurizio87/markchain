from setuptools import setup, find_packages

setup(
    name="markchain",
    version="0.1",
    description="",
    packages=find_packages(),
    package_data={
        "": ["*.txt", "*.md", "*.rst"]
    }
)

author = "Stefano Maurizio"
author_email = "smaurizio87@protonmail.com"
description = "A module for Markov Chain Monte Carlo simulations"
