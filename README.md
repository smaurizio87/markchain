# Pybayes

Pybayes is a small Python library for Bayesian simulations.


## Installation

You can simply install it via [pip](https://pip.pypa.io/en/stable/)

        pip install markchain

## Usage

TODO

## Contributing

Contributions are welcome.

## License

[GPL 3](https://www.gnu.org/licenses/gpl-3.0.html)
