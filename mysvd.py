import numpy as nn
from jax import numpy as np

a = np.array([[2, 1], [1, 3]])


u, s, vt = np.linalg.svd(a)

def build_m(a):
    out = []
    zero = 0.0*a[0]
    for row in a:
        r = np.concatenate((zero, row))
        out.append(r)
    for row in a.T:
        r = np.concatenate((row, zero))
        out.append(r)
    return np.array(out)

m_a = build_m(a)

q1, r1 = np.linalg.qr(m_a)


def eigen_qr(A, iterations=500):
    Ak = np.copy(A)
    n = Ak.shape[0]
    QQ = np.eye(n)
    for k in range(iterations):
        # s_k is the last item of the first diagonal
        s = Ak.item(n-1, n-1)
        smult = s * np.eye(n)
        # pe perform qr and subtract smult
        Q, R = np.linalg.qr(np.subtract(Ak, smult))
        # we add smult back in
        Ak = np.add(R @ Q, smult)
        QQ = QQ @ Q
    return Ak, QQ

ak, qq = eigen_qr(a)
s1 = np.diag(ak)
print(s)
print(s1)

# print(u)
# print(vt)
#
# print(q1)
# print(r1)
# q2, r2 = np.linalg.qr(np.dot(a, a.T))
# print(q2, r2)
#
# q3, r3 = np.linalg.qr(np.dot(a.T, a))
# print(q2, r2)
