from jax import numpy as np
import pandas as pd
from jax import jit
import numpy as nn
from matplotlib import pyplot as plt
from markchain.distributions import GaussianProcess, \
    MVDiagonalNormal, Normal, Uniform, Model, Data, Uniform, Exp, Expon, Distribution
from markchain.classes import HamiltonianMC, SeriesMC, StochasticDualAverage
from markchain.utils import plot_trace, trace_summary
from markchain.linalg import cholesky, cho_solve
from markchain.gpkernel import RationalQuadratic, WhiteNoise




def k0(x, sigma_0, sigma_1, theta):
    w = np.array([[(x[a] - x[b])@(x[a]-x[b]) for a in range(len(x))]
                    for b in range(len(x))])
    z = sigma_1**2*(1+w/(4*theta))**(-2) + sigma_0**2*np.diag(np.array([1.0]*len(x)))
    return z

sz = 20
N = 10000
W = 25000
N_sample = 1000

X = nn.sort(nn.random.default_rng().normal(loc=0, scale=1, size=int(sz)).reshape((int(sz), 1))
            )
# Y = 0.2*nn.random.normal(loc=0, scale=cv) + 0.5*X + 0.4*nn.sin(X)
# print(Y)

s0 = -0.01
s1 = 0.6
th = 2.0
w = k0(X, s0, s1, th).reshape((int(sz), int(sz)))

Y = nn.random.multivariate_normal(mean=np.zeros(sz),
                                  cov=w)

log_sigma_0 = Normal(var_name='log_sigma_0', loc=-2, scale=3)
log_sigma_1 = Normal(var_name='log_sigma_1', loc=-1, scale=3)
log_sigma_2 = Normal(var_name='log_sigma_2', loc=-1, scale=3)
sigma_0 = Exp(var_name='sigma_0', var=log_sigma_0)
sigma_1 = Exp(var_name='sigma_1', var=log_sigma_1)
sigma_2 = Exp(var_name='sigma_2', var=log_sigma_2)
log_theta = Normal(var_name='log_theta', loc=0, scale=2)
theta = Exp(var_name='theta', var=log_theta)

k1 = WhiteNoise(sigma='sigma_0')

k2 = RationalQuadratic(sigma='sigma_1', theta='theta', alpha='sigma_2')

k = k1 + k2

y = GaussianProcess(var_name='y',
                    sigma_0='sigma_0',
                    sigma_1='sigma_1',
                    theta='theta',
                    sigma_2='sigma_2',
                    kernel=k,
                    shape=sz,
                    X=X)

data = Data(var_name='y', var_values=Y)

model = Model([log_sigma_0, log_sigma_1, log_sigma_2, sigma_0,
               sigma_1, sigma_2, log_theta,
               theta, y],
              var_list=['log_sigma_0', 'log_sigma_1',
                        'log_sigma_2',
                        'log_theta'],
              data=data)

z0 = np.array([0., 0., 0., 0.])
print(model.log_posterior(z0))



sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                                start_x=z0,
                                find_start=True,
                                var_names=model.var_list,
                                decorrelate=True,
                                renormalize_mass=True)
# #
# sampler.delta = 1e-1
# sampler.L_mean = 15
#

sampler.sample(N, warmup=W)
#
trace = sampler.points_dict()

print(pd.DataFrame(trace_summary(trace)))


fig = plot_trace(trace)
fig.savefig('trace_rat.eps')
plt.close(fig)

pts0 = sampler.points()

pts = pts0.T[nn.random.choice(range(len(pts0)), N_sample)].T
print(np.shape(pts))

v = model.fill_trace_new(pts)

z = np.linspace(1.05*np.min(X), 1.05*np.max(X), 200).reshape((200, 1))

w = y.sample_fstar(X, Y, z, v).T

print('plot')
mn = [np.median(elem) for elem in w]
qq05 = [np.quantile(elem, 0.1) for elem in w]
qq95 = [np.quantile(elem, 0.9) for elem in w]


Xpl = X.reshape((sz))
z1 = z.reshape(np.shape(z)[0])
#
mxx = 1.5*np.max(np.abs(Y))
fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(Xpl, Y, color='k', marker='x')
ax.plot(z1, mn, color='r', ls='--')
ax.fill_between(z1, qq05, qq95, color='r', alpha=0.6)
ax.set_ylim(-mxx, mxx)
fig.savefig('posterior_rat.pdf')
plt.close(fig)

#
# print('K')

# K00exp = np.array([[(X[a] - X[b])@(X[a]-X[b]) for a in range(len(X))]
#                 for b in range(len(X))])
#
# Kxexp = np.array([[(z[a] - X[b])@(z[a]-X[b]) for a in range(len(z))]
#                 for b in range(len(X))])
# Kxxexp = np.array([[(z[a] - z[b])*(z[a]-z[b]) for a in range(len(z))]
#                 for b in range(len(z))])
#
# def frnd(params):
#     mn = 0
#     sigma_0 = np.exp(params[0])
#     sigma_1 = np.exp(params[1])
#     theta = np.exp(params[2])
#     alpha = np.exp(params[3])
#     K00a = sigma_1**2/(1+K00exp/(2*theta**2*alpha))**alpha + sigma_0**2*np.diag(np.array([1.0]*len(X)))
#     Kx = sigma_1**2/(1+Kxexp/(2*theta**2*alpha))**alpha
#     Kxx = sigma_1**2/(1+Kxxexp/(2*theta**2*alpha))**alpha
#     # L = linalg.inv(K00a)
#     # w = np.dot(L, Y)
#     # mean = np.dot(Kx.T, w)
#     Lcho = cholesky(K00a)
#     alpha = cho_solve(Lcho, Y)
#     mn = np.dot(Kx.T, alpha)
#     v = cho_solve(Lcho, Kx)
#     # cov0 = Kxx - np.dot(Kx.T, np.dot(L, Kx))
#     covn = Kxx - np.dot(v.T, Kx)
#     # cvn = np.linalg.inv(LL)
#     # z = nn.random.default_rng().multivariate_normal(mean=mean, cov=cov)
#     return (mn, covn)
#
# # z = frnd(pts[-1])
# # print(z)
#
# ff = jit(frnd)
# w0 = [ff(elem) for elem in pts]
#
# # w0 = np.array([ff(z, elem, X) for elem in pts])
# mn = np.array([0]*len(z))
# cv = np.diag(1+mn)
#
# w = np.array([nn.random.default_rng().multivariate_normal(mean=elem[0],
#                                                           cov=elem[1])
#               for elem in w0]).T
#
# mn = [np.median(elem) for elem in w]
# qq05 = [np.quantile(elem, 0.1) for elem in w]
# qq95 = [np.quantile(elem, 0.9) for elem in w]
#
#
# Xpl = X.reshape((sz))
# #
# mxx = 1.5*np.max(np.abs(Y))
# fig = plt.figure()
# ax = fig.add_subplot(111)
# ax.scatter(Xpl, Y, color='k', marker='x')
# ax.plot(z, mn, color='r', ls='--')
# ax.fill_between(z, qq05, qq95, color='r', alpha=0.6)
# ax.set_ylim(-mxx, mxx)
# fig.savefig('posterior_rat.pdf')
# plt.close(fig)
