from abc import ABC, abstractmethod
from collections import defaultdict
from jax import numpy as np
import pandas as pd
from jax.scipy import linalg
from jax import jit
import numpy as nn
from matplotlib import pyplot as plt
from markchain.distributions import GaussianProcess, \
    MVDiagonalNormal, Normal, Uniform, Model, Data, Uniform, Exp, Expon, Distribution
from markchain.classes import HamiltonianMC, SeriesMC, StochasticDualAverage
from markchain.utils import plot_trace, trace_summary
from markchain.linalg import cholesky, cho_solve
from markchain.gpkernel import RBF, WhiteNoise, PeriodicKernel




def k0(x, sigma_0, sigma_1, theta):
    w = np.array([[np.sin(np.pi*(x[a] - x[b]))@np.sin(np.pi*(x[a]-x[b])) for a in range(len(x))]
                    for b in range(len(x))])
    z = sigma_1**2*np.exp(-2*w/theta**2) + sigma_0**2*np.diag(np.array([1.0]*len(x)))
    return z

sz = 20
N = 10000
W = 10000
N_sample = 5000

X = nn.sort(nn.random.default_rng().normal(loc=0, scale=1, size=int(sz)).reshape((int(sz), 1))
            )
# Y = 0.2*nn.random.normal(loc=0, scale=cv) + 0.5*X + 0.4*nn.sin(X)
# print(Y)

s0 = -0.02
s1 = 0.8
th = 2.0
w = k0(X, s0, s1, th).reshape((int(sz), int(sz)))

Y = nn.random.multivariate_normal(mean=np.zeros(sz),
                                  cov=w)

log_sigma_0 = Normal(var_name='log_sigma_0', loc=-1, scale=5)
log_sigma_1 = Normal(var_name='log_sigma_1', loc=-1, scale=5)
sigma_0 = Exp(var_name='sigma_0', var=log_sigma_0)
sigma_1 = Exp(var_name='sigma_1', var=log_sigma_1)
log_theta = Normal(var_name='log_theta', loc=0, scale=3)
theta = Exp(var_name='theta', var=log_theta)

k1 = WhiteNoise(sigma='sigma_0')
k2 = PeriodicKernel(sigma='sigma_1', lenght='theta', period=1)

k = k1 + k2

y = GaussianProcess(var_name='y', kernel=k,
                    sigma_0='sigma_0',
                    sigma_1='sigma_1',
                    theta='theta',
                    period=1,
                    shape=sz,
                    X=X)

data = Data(var_name='y', var_values=Y)

model = Model([log_sigma_0, log_sigma_1, sigma_0,sigma_1, log_theta,
               theta, y],
              var_list=['log_sigma_0', 'log_sigma_1', 'log_theta'],
              data=data)

z0 = np.array([0., 0., 0.])
print(model.log_posterior(z0))



sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                                start_x=z0,
                                find_start=True,
                                var_names=model.var_list,
                                decorrelate=True,
                                renormalize_mass=True)
# #
# sampler.delta = 1e-1
# sampler.L_mean = 15
#

sampler.sample(N, warmup=W)
#
trace = sampler.points_dict()

print(pd.DataFrame(trace_summary(trace)))


fig = plot_trace(trace)
fig.savefig('trace_prova.eps')
plt.close(fig)

pts0 = sampler.points()

pts = pts0.T[nn.random.choice(range(len(pts0)), N_sample)]
print(np.shape(pts))



z = np.linspace(1.05*np.min(X), 1.05*np.max(X), 200)

pts = pts0.T[nn.random.choice(range(len(pts0)), N_sample)].T
print(np.shape(pts))

v = model.fill_trace_new(pts)

z = np.linspace(1.05*np.min(X), 1.05*np.max(X), 200).reshape((200, 1))

w = y.sample_fstar(X, Y, z, v).T

print('plot')
mn = [np.median(elem) for elem in w]
qq05 = [np.quantile(elem, 0.1) for elem in w]
qq95 = [np.quantile(elem, 0.9) for elem in w]


Xpl = X.reshape((sz))
z1 = z.reshape(np.shape(z)[0])
#
mxx = 1.5*np.max(np.abs(Y))
fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(Xpl, Y, color='k', marker='x')
ax.plot(z1, mn, color='r', ls='--')
ax.fill_between(z1, qq05, qq95, color='r', alpha=0.6)
ax.set_ylim(-mxx, mxx)
fig.savefig('posterior_prova.pdf')
plt.close(fig)
