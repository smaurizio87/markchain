from abc import ABC, abstractmethod
from collections import defaultdict
from jax import numpy as np
import pandas as pd
from jax.scipy import linalg
from jax import jit
import numpy as nn
from matplotlib import pyplot as plt
from markchain.distributions import GaussianProcess, \
    MVDiagonalNormal, Normal, Uniform, Model, Data, Uniform, Exp, Expon, Distribution
from markchain.classes import HamiltonianMC, SeriesMC, StochasticDualAverage
from markchain.utils import plot_trace, trace_summary
from markchain.linalg import cholesky, cho_solve
from markchain.gpkernel import RBF, WhiteNoise, Minimum




def k0(x, sigma_0, sigma_1):
    w = np.array([[np.minimum(x[a][0],x[b][0]) for a in range(len(x))]
                    for b in range(len(x))])
    z = sigma_1*w
    return z

sz = 20
N = 50000
W = 50000
N_sample = 5000

#X = nn.sort(nn.random.default_rng().normal(loc=0, scale=1, size=int(sz)).reshape((int(sz), 1))
#            )
#
#X = X - X[0] + 1
X = np.arange(1, sz+1).reshape((sz, 1))
print(X)
# Y = 0.2*nn.random.normal(loc=0, scale=cv) + 0.5*X + 0.4*nn.sin(X)
# print(Y)

s0 = 1
s1 = np.exp(-0.2)
w = k0(X, s0, s1).reshape((int(sz), int(sz)))

Y = nn.random.multivariate_normal(mean=np.zeros(sz),
                                  cov=w)

log_sigma_1 = Normal(var_name='log_sigma_1', loc=0, scale=2)
sigma_1 = Exp(var_name='sigma_1', var=log_sigma_1)

k2 = Minimum(sigma='sigma_1')


y = GaussianProcess(var_name='y', kernel=k2,
                    sigma_1='sigma_1',
                    shape=sz,
                    X=X)

data = Data(var_name='y', var_values=Y)

model = Model([log_sigma_1, sigma_1,
               y],
              var_list=['log_sigma_1'],
              data=data)

z0 = np.array([0.])
print(model.log_posterior(z0))



sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                                start_x=z0,
                                find_start=True,
                                var_names=model.var_list,
                                decorrelate=True,
                                renormalize_mass=True)
# #
# sampler.delta = 1e-1
# sampler.L_mean = 15
#

sampler.sample(N, warmup=W)
#
trace = sampler.points_dict()

print(pd.DataFrame(trace_summary(trace)))


fig = plot_trace(trace)
fig.savefig('trace_prova.eps')
plt.close(fig)

pts0 = sampler.points()

pts = pts0.T[nn.random.choice(range(len(pts0)), N_sample)]
print(np.shape(pts))



z = np.linspace(1.05*np.min(X), 1.05*np.max(X), 200)

print('K')

K00exp = np.array([[np.minimum(X[a][0], X[a][0]) for a in range(len(X))]
                for b in range(len(X))])

Kxexp = np.array([[np.minimum(z[a], X[b][0]) for a in range(len(z))]
                for b in range(len(X))])
Kxxexp = np.array([[np.minimum(z[a],z[b]) for a in range(len(z))]
                for b in range(len(z))])

def frnd(params):
    mn = 0
    sigma_1 = np.exp(params[0])
    K00a = sigma_1*K00exp + 1e-4*np.eye(len(X))
    Kx = sigma_1*Kxexp
    Kxx = sigma_1*Kxxexp
    # L = linalg.inv(K00a)
    # w = np.dot(L, Y)
    # mean = np.dot(Kx.T, w)
    Lcho = cholesky(K00a)
    alpha = cho_solve(Lcho, Y)
    mn = np.dot(Kx.T, alpha)
    v = cho_solve(Lcho, Kx)
    # cov0 = Kxx - np.dot(Kx.T, np.dot(L, Kx))
    covn = Kxx - np.dot(v.T, Kx)
    # cvn = np.linalg.inv(LL)
    # z = nn.random.default_rng().multivariate_normal(mean=mean, cov=cov)
    return (mn, covn)

# z = frnd(pts[-1])
# print(z)

ff = jit(frnd)
w0 = [ff(elem) for elem in pts]

# w0 = np.array([ff(z, elem, X) for elem in pts])
mn = np.array([0]*len(z))
cv = np.diag(1+mn)

w = np.array([nn.random.default_rng().multivariate_normal(mean=elem[0],
                                                          cov=elem[1])
              for elem in w0]).T

mn = [np.median(elem) for elem in w]
qq05 = [np.quantile(elem, 0.1) for elem in w]
qq95 = [np.quantile(elem, 0.9) for elem in w]


Xpl = X.reshape((sz))
#
mxx = 1.5*np.max(np.abs(Y))
fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(Xpl, Y, color='k', marker='x')
ax.plot(z, mn, color='r', ls='--')
ax.fill_between(z, qq05, qq95, color='r', alpha=0.6)
ax.set_ylim(-mxx, mxx)
fig.savefig('posterior_prova.pdf')
plt.close(fig)
