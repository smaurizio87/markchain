from jax import jit
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from markchain.distributions import Uniform, Model, Data, Normal, StudentT, Expon, Gamma, Exp
from markchain.classes import HamiltonianMC, StochasticDualAverage
from markchain.utils import trace_summary, plot_trace

N = 50000
W = 50000

palette = sns.color_palette("viridis")

df = pd.read_csv('./aghi_misura_declinazione.csv', sep=",")
print(df.head())

for NS in ['A', 'B']:
    for trattamento in [1, 2]:
        df_red = df[["NS", "ago", "trattamento", "angolo_gradi_normalizzato"]]

        df_red = df_red[
            (df_red.NS == NS) & (df_red.trattamento == trattamento)]
        print(df_red.head())

        var = df_red['angolo_gradi_normalizzato'].values/180
        theta = Uniform(var_name="theta", low=-1)
        phi = Uniform(var_name="phi", high=2)
        # log_nu = Normal(var_name='log_nu', loc=0, scale=5)
        nu = Expon(var_name='nu', scale=5)
        x = StudentT(var_name='x', loc='theta', scale='phi', df='nu')

        model = Model([theta, phi, nu, x], var_list=['theta', 'phi', 'nu'],
                    data=Data(var_name='x', var_values=var))

        sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                                var_names=model.var_list,
                                renormalize_mass=True,
                                find_start=True,
                                # decorrelate=True,
                                start_x=np.array([0.5, 0.5, 1]))
        sampler.L_mean = 2
        sampler.n_step_mean = 2
        sampler.delta = 5e-2

        sampler.sample(N, warmup=W)
        trace = sampler.points_dict()

        ww = sampler.points()
        z = np.arange(len(ww[0]))

        wa = ww.transpose()
        f_aux = jit(model.f_deterministic)
        y = [f_aux(elem) for elem in wa]
        posterior_predictive = [model.predictive(elem)[0] for elem in y]
        x_pred = posterior_predictive

        print(NS, trattamento)
        print(pd.DataFrame(trace_summary(trace)))
        z = np.arange(N)
        fig = plot_trace(trace)
        # fig, ax = plt.subplots(nrows=3, ncols=2)
        # ax[0][0].plot(z, trace['theta'])
        # ax[1][0].plot(z, trace['phi'])
        # ax[2][0].plot(z, trace['nu'])

        # ax[0][1].hist(trace['theta'], bins=40, density=True)
        # ax[1][1].hist(trace['phi'], bins=40, density=True)
        # ax[2][1].hist(trace['nu'], bins=40, density=True)
        # fig.tight_layout()

        fig.savefig(f'trace_{NS}_{trattamento}.eps')
        plt.close(fig)


        fig, ax = plt.subplots(nrows=2)
        ax[0].hist(x_pred, label='x', color=palette[0], density=True, bins=np.arange(-1, 1, 5e-2))
        ax[1].hist(var, color='grey', label='data', density=True, bins=np.arange(-1, 1, 5e-2))
        ax[0].set_xlim([-1, 1])
        ax[1].set_xlim([-1, 1])
        legend = fig.legend()

        fig.tight_layout()
        fig.savefig(f'posterior_predictive_{NS}_{trattamento}.pdf')
        plt.close(fig)
