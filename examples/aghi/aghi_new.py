import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from markchain.distributions import Uniform, Model, Data, Normal, StudentT, Expon, Gamma, Exp
from markchain.classes import HamiltonianMC, MultiTrace
from markchain.utils import trace_summary, plot_trace

N = 50000
W = 50000

df = pd.read_csv('./aghi_misura_declinazione.csv', sep=",")
print(df.head())

palette = sns.color_palette("viridis")

for NS in ['A', 'B']:
    for trattamento in [1, 2]:
        df_red = df[["NS", "ago", "trattamento", "angolo_gradi_normalizzato"]]

        df_red = df_red[(df_red.NS == NS) & (df_red.trattamento == trattamento)]
        print(df_red.head())

        var = df_red['angolo_gradi_normalizzato'].values/180
        theta = Uniform(var_name="theta", low=-1)
        phi = Uniform(var_name="phi", high=2)
        log_nu = Normal(var_name='log_nu', loc=0, scale=5)
        nu = Exp(var_name='nu', var=log_nu)
        x = StudentT(var_name='x', loc='theta', scale='phi', nu='nu')

        model = Model([theta, phi, log_nu, nu, x], var_list=['theta', 'phi', 'log_nu'],
                    data=Data(var_name='x', var_values=var))

        sampler = HamiltonianMC(log_pdf=model.log_posterior,
                                var_names=model.var_list,
                                renormalize_mass=True,
                                find_start=True,
                                start_x=np.array([0.5, 0.5, 0]))

        smp = MultiTrace(sampler, 4)

        traces = smp.sample(4, N, warmup=W)

        fig = plot_trace(traces, palette=palette, alpha=0.5)

        fig.savefig(f'trace_{NS}_{trattamento}.eps')
        plt.close(fig)

        fig.savefig(f'trace_{NS}_{trattamento}.eps')
        plt.close(fig)
