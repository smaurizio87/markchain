from jax import numpy as np
import numpy as nn
from markchain.distributions import GaussianMixture, StickBreaking, Beta,\
    Model, Data, Gamma, Normal
from markchain.classes import HamiltonianMC, SeriesMC, StochasticDualAverage

K = 6
N = 80000
W = 80000

z = Beta(var_name='z', a=1, b=np.array([2]*K), shape=K)
w = StickBreaking(var=z, var_name='w')

mu = Normal(var_name='mu', loc=0, scale=1, shape=K)
sig = Gamma(var_name='sig', a=1, b=1, shape=K)

y = GaussianMixture(var_name='y', loc='mu', scale='sig',
                    c='w')

d0 = nn.concatenate([nn.random.normal(loc=i/2.0, scale=0.2, size=2*i) for i in range(3)])
print(d0)

model = Model([z, w, mu, sig, y],
              var_list=['z', 'mu', 'sig'],
              data=Data(var_name='y', var_values=d0))

z0 = np.array([0.3]*K + list(range(K)) + [0.2]*K)

print(model.log_posterior(z0))

sampler = StochasticDualAverage(log_pdf=model.log_posterior, start_x=z0,
                                decorrelate=True
                        )
sampler.sample(N, warmup=W)
