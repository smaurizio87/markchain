from jax import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from markchain.distributions import *
from markchain.classes import HamiltonianMC, SeriesMC, StochasticDualAverage
from markchain.utils import plot_trace

N = 40000

data = pd.read_csv('./efronmorris.csv', sep='\t')
at_bats, hits = data[["At-Bats", "Hits"]].to_numpy().T

at_bats = at_bats[::-1]
hits = hits[::-1]
names = data['LastName'].values[::-1]
h = hits
b = at_bats

class Faux(Deterministic1D):
    def __init__(self, var_name, var):
        super().__init__(var, var_name)

        def f(x):
            return 1-x
        self.f = f

phi = Uniform(var_name='phi', low=0, high=1)

kappa_log = Expon(var_name='kappa_log', scale=2.)
kappa = Exp(var_name='kappa', var=kappa_log)

alpha = ProdList(var_name='alpha', var_list=[phi, kappa])
min_beta = Faux(var_name='min_beta', var=phi)

beta = ProdList(var_name='beta', var_list=[min_beta, kappa])
y = BetaBinom(var_name=f'y', a='alpha', b='beta', n=at_bats)

model = Model([phi, kappa_log, kappa, alpha, min_beta, beta, y],
              var_list=['phi', 'kappa_log'],
              data=[Data(var_name='y', var_values=hits)
                    for k in range(len(names))])

masses = np.diag(np.array([0.8, 0.2]))
start_x = np.array([0.25, 5.0])
print(model.log_posterior(start_x))
sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                                start_x=start_x,
                                var_names=model.var_list,
                                find_start=True,
                                renormalize_mass=True,
                                decorrelate=True
                                )

sampler.cnt = 500
sampler.sample(N=N, warmup=N)
print(sampler.m)
print(sampler.delta, sampler.L, sampler.cnt)
print(sampler.x_temp)
trace = sampler.points_dict()
z = np.arange(len(trace['phi']))

fig, ax = plt.subplots(nrows=2, ncols=2)
for k, elem in enumerate(['phi', 'kappa_log']):
    ax[k, 0].plot(z, trace[elem])
    ax[k, 1].hist(trace[elem], bins=30, density=True)
fig.tight_layout()
fig.savefig('trace_conj.eps')
plt.close(fig)

fig = plot_trace(trace)
fig.savefig('trace_new.eps')
plt.close(fig)

