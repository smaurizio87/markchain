import pandas as pd
import numpy as nn
from jax import numpy as np
from matplotlib import pyplot as plt
from markchain.distributions import Normal, Model, Data, Gamma, Exp
from markchain.classes import HamiltonianMC, ProvaMC
from markchain.utils import plot_trace, trace_summary

num = 300

r = 5

cv = np.arange(0, r, 1)+0.5
mn = cv-0.5

data = nn.random.default_rng().multivariate_normal(mean=mn, cov=np.diag(cv), size=num)

mu = Normal(var_name='mu', loc=0, scale=2, shape=r)
log_sigma = Normal(var_name='log_sigma', loc=0, scale=10, shape=r)
sigma = Exp(var_name='sigma', var=log_sigma)
x = Normal(var_name='x', loc='mu', scale='sigma', shape=r)

model = Model([mu, log_sigma, sigma, x], var_list=['mu', 'log_sigma'],
              data=Data(var_name='x', var_values=data))

sampler = ProvaMC(log_pdf=model.log_posterior,
                  start_x=np.array([0.0]*r + [0.0]*r),
                  var_names=model.var_list)

N = 35000
W = 35000

sampler.delta = 2e-1
sampler.L_mean = 1
sampler.cnt = 300

sampler.sample(N, warmup=W)

trc = sampler.points()

trace = {f'mu_{k}': trc[k] for k in range(r)}
trace.update({f"sig_{k}": trc[r+k] for k in range(r)})

fig = plot_trace(trace)
fig.tight_layout()
fig.savefig('prova_a.eps')
plt.close(fig)
