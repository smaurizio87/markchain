import numpy as nn
import seaborn as sns
import pandas as pd
from matplotlib import pyplot as plt
from jax import numpy as np
from markchain.distributions import Beta, Binom, Model, Data, Normal, Exp, Linear, Logistic, SumList, Expon, Log, Pow, ProdList, Gamma
from markchain.classes import SeriesMC

y = np.array([
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,
    1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,  2,  2,  1,  5,  2,
    5,  3,  2,  7,  7,  3,  3,  2,  9, 10,  4,  4,  4,  4,  4,  4,  4,
    10,  4,  4,  4,  5, 11, 12,  5,  5,  6,  5,  6,  6,  6,  6, 16, 15,
    15,  9,  4
])
n = np.array([
    20, 20, 20, 20, 20, 20, 20, 19, 19, 19, 19, 18, 18, 17, 20, 20, 20,
    20, 19, 19, 18, 18, 25, 24, 23, 20, 20, 20, 20, 20, 20, 10, 49, 19,
    46, 27, 17, 49, 47, 20, 20, 13, 48, 50, 20, 20, 20, 20, 20, 20, 20,
    48, 19, 19, 19, 22, 46, 49, 20, 20, 23, 19, 22, 20, 20, 20, 52, 46,
    47, 24, 14
])
r = len(n)
n = n[-r:]
y = y[-r:]

# mu = Normal(var_name='mu', loc=0, scale=10)
# mu0 = Normal(var_name='mu0', loc=0, scale=10)
# phi = Linear(var=mu, var_name='phi', coeffs=m_v)
# phi0 = Linear(var=mu0, var_name='phi0', coeffs=m_0)
# x = SumList(var_name='x', var_list=[phi, phi0])
# theta = Logistic(var=x, var_name='theta')

# ab0 = Expon(var_name='ab0', scale=10)
# ab1 = Expon(var_name='ab1', scale=10)
#
# inv_ab1 = Pow(var_name='inv_ab1', var=ab1, n=-1)
# abratio = ProdList(var_name='abratio', var_list=[ab0, inv_ab1])
# absum = SumList(var_name='absum', var_list=[ab0, ab1])
#
# a = Log(var_name='a', var=abratio)
# b = Log(var_name='b', var=absum)
a = Expon(var_name='a', scale=5)
b = Expon(var_name='b', scale=5)
thetas = Beta(var_name=f"theta", a='a', b='b', shape=r)
x = Binom(var_name=f"y", p=f"theta", n=n)

data = Data(var_name=f"y", var_values=y)

model = Model([a, b, thetas, x], var_list=['a', 'b', 'theta'], data=data)

z0 = np.array([2, 10]+list([0.1]*len(y)))
print(model.log_posterior(z0))
masses = np.diag(np.array([0.01, 0.01]+[1]*len(y)))

sampler = SeriesMC(log_pdf=model.log_posterior,
                   start_x = z0,
                   integrator='leapfrog',
                   var_names=model.var_list,
                   renormalize_mass=True,
                   group_masses=[0,1,2]
                   )

sampler.delta=.1
sampler.L = 1
sampler.cnt = 100
W = 150000
N = 150000
sampler.sample(N, warmup=W)
print(sampler.L, sampler.delta, sampler.cnt)
print(np.diag(sampler.m))
print(sampler.x_temp)

z = sampler.points_dict()
abval0 = z['a']
abval1 = z['b']

xx = np.arange(len(abval0))
fig, ax = plt.subplots(nrows=2, ncols=2)
ax[0, 0].plot(xx, abval0)
ax[1, 0].plot(xx, abval1)
ax[0, 1].hist(abval0, bins=50, density=True)
ax[1, 1].hist(abval1, bins=50, density=True)
fig.tight_layout()
fig.savefig('trace.eps')
plt.close(fig)

dd = {r'l_1': np.log(abval0+abval1),
      r'l_2': np.log(abval0/abval1)}
trace = sampler.points()
nn.savetxt('trace.csv', trace)

fig = plt.figure()
ax = fig.add_subplot(111)
sns.kdeplot(dd, x=r'l_1', y=r'l_2', levels=100,
            cmap='mako', ax=ax, fill=True, alpha=0.7)
fig.savefig('kde.pdf')
plt.close(fig)
