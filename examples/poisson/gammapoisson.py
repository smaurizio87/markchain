import numpy as nn
import pandas as pd
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from markchain.distributions import Gamma, Poisson, Model, Data, Normal, Exp, Expon, Pow
from markchain.classes import Gibbs, HamiltonianMC, StochasticDualAverage
from markchain.utils import plot_trace, trace_summary
from statsmodels.graphics.tsaplots import plot_acf
# from markchain.distributions import ZeroInflatedPoisson as Poisson

palette = sns.color_palette("viridis")


data = nn.random.poisson(lam=10, size=700)
print(data)

mu = Gamma('mu', a=4, b=0.1)
x = Poisson(var_name='x', k='mu')

model = Model([mu, x], var_list=['mu'],
              data=Data(var_name='x', var_values=data))


sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                        start_x=np.array([2]),
                        var_names=model.var_list,
                        J=model.d_log_posterior)
sampler.delta = 1e-1
sampler.L_mean = 4
# sampler.sample(200, warmup=100)
# print(sampler.points())

# sampler = Gibbs(pdf=model.log_posterior, start_x=np.array([0.2]))
t0 = timer()
sampler.sample(10000, warmup=10000)
t1 = timer()
print(f"{t1-t0}")

trace = sampler.points_dict()
print(pd.DataFrame(trace_summary(trace)))

fig = plot_trace(trace)
fig.savefig('trace.eps')
plt.close(fig)

w = sampler.points()
print(f"{np.mean(w[0]**2)} {np.median(w[0]**2)} {np.std(w[0]**2)}")
print(f"{np.quantile(w[0]**2, 0.1)} {np.quantile(w[0]**2, 0.9)}")
z = np.arange(len(w[0]))
fig, ax = plt.subplots(nrows=4, ncols=1)
ax[0].plot(z, w[0], color=palette[0])
ax[1].hist(w[0], density=True, bins=80, color=palette[0])
ax[2].hist(w[0]**2, density=True, bins=200, color=palette[0])
plot_acf(w[0], ax=ax[3], color=palette[0])
fig.tight_layout()
fig.savefig('trace_gamma_poisson.eps')
plt.close(fig)

f_aux = jit(model.f_deterministic)

ww = sampler.points()
wa = ww.transpose()
y = [f_aux(elem) for elem in wa]
print(np.shape(y))
posterior_predictive = [model.predictive(elem)[0] for elem in y]
print(np.shape(posterior_predictive))


fig, ax = plt.subplots(nrows=2)
ax[0].hist(data, density=True, bins=40, color=palette[0])
ax[1].hist(posterior_predictive, density=True, bins=40, color=palette[0])
fig.tight_layout()
legend = fig.legend()
fig.savefig('posterior_predictive.eps')
plt.close(fig)
