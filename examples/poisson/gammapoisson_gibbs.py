import numpy as nn
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from matplotlib import pyplot as plt
from markchain.distributions import Gamma, Poisson, Model, Data, Normal, Exp, Expon, Pow
from markchain.classes import Gibbs, HamiltonianMC

palette = sns.color_palette("viridis")

# TODO: MAKE ME WORK!!!!!


data = nn.random.poisson(lam=5, size=300)

mu0 = Normal('mu0', loc=0, scale=5)
mu = Pow(var_name='mu', var=mu0, n=2)
x = Poisson(var_name='x', k='mu')

model = Model([mu0, mu, x], var_list=['mu0'], extra_vars=['mu'],
              data=Data(var_name='x', var_values=data))


sampler = Gibbs(log_pdf=model.log_posterior, start_x=np.array([.1]))
sampler.delta = 3e-3
sampler.L = 4
# sampler.sample(200, warmup=100)
# print(sampler.points())

# sampler = Gibbs(pdf=model.log_posterior, start_x=np.array([0.2]))
t0 = timer()
sampler.sample(2000, warmup=2000)
t1 = timer()
print(f"{t1-t0}")
# w = sampler.points()
# print(f"{np.mean(w[0]**2)} {np.median(w[0]**2)} {np.std(w[0]**2)}")
# print(f"{np.quantile(w[0]**2, 0.1)} {np.quantile(w[0]**2, 0.9)}")
# z = np.arange(len(w[0]))
# fig, ax = plt.subplots(nrows=3, ncols=1)
# ax[0].plot(z, w[0], color=palette[0])
# ax[1].hist(w[0], density=True, bins=200, color=palette[0])
# ax[2].hist(w[0]**2, density=True, bins=200, color=palette[0])
# fig.savefig('trace_gamma_poisson_gibbs.eps')
# plt.close(fig)
