from matplotlib import pyplot as plt
from jax import numpy as np
from markchain.distributions import Normal, HalfCauchy, Model, Data
from markchain.classes import SeriesMC, HamiltonianMC
from markchain.classes import SeriesMC, HamiltonianMC, StochasticDualAverage

J = 8
y = np.array([28, 8, -3, 7, -1, 1, 18, 12])[:J]
sigma = np.array([15, 10, 16, 11, 9, 11, 10, 18])[:J]

mu = Normal(var_name='mu', loc=0, scale=5)
tau = HalfCauchy(var_name='tau', loc=0, scale=5)

theta = Normal(var_name='theta', loc='mu', scale='tau', shape=J)

Y = Normal(var_name='y', loc='theta', scale=sigma)

model = Model([mu, tau, theta, Y],
              var_list=['mu', 'tau', 'theta'],
              data = Data(var_name='y', var_values=y))


z0 = np.array([1, 0.1]+list(1 + 0*y[:J]))
print(model.log_posterior(z0))
m = np.diag(np.array([0.2, 1]+[0.2]*J))


sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                   start_x=z0,
                   var_names=model.var_list,
                   integrator='third_order',
                   )

N=60000
W=60000

sampler.sample(N=N, warmup=W)
print(sampler.delta, sampler.L, sampler.cnt)
print(sampler.x_temp)
print(np.diag(sampler.m))
trace = sampler.points_dict()
x = np.arange(N)
fig, ax = plt.subplots(nrows=2, ncols=2)
ax[0, 0].plot(x, trace['mu'])
ax[1, 0].plot(x, trace['tau'])

ax[0, 1].hist(trace['mu'], density=True, bins=50)
ax[1, 1].hist(trace['tau'], density=True, bins=50)
fig.savefig('trace_new.eps')
plt.close(fig)
