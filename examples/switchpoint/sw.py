from matplotlib import pyplot as plt
import seaborn as sns
from jax import jit
import numpy as np
from markchain.distributions import Uniform, Poisson, Expon, Model, Data, \
    IfElse, Minus, Sum, Beta, Prod
from markchain.classes import HamiltonianMC, SeriesMC, StochasticDualAverage

N = 35000
W = 35000

eps = 1e-10

s = 1.+eps
bins = 30


disaster_data_all = np.array([4, 5, 4, 0, 1, 4, 3, 4, 0, 6, 3, 3, 4, 0, 2, 6,
                           3, 3, 5, 4, 5, 3, 1, 4, 4, 1, 5, 5, 3, 4, 2, 5,
                           2, 2, 3, 4, 2, 1, 3, np.nan, 2, 1, 1, 1, 1, 3, 0, 0,
                           1, 0, 1, 1, 0, 0, 3, 1, 0, 3, 2, 2, 0, 1, 1, 1,
                           0, 1, 0, 1, 0, 0, 0, 2, 1, 0, 0, 0, 1, 1, 0, 2,
                           3, 3, 1, np.nan, 2, 1, 1, 1, 1, 2, 4, 2, 0, 0, 1, 4,
                           0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1])


years = np.arange(1851, 1962)

x0 = np.arange(len(disaster_data_all))/len(disaster_data_all)

x = x0[~np.isnan(disaster_data_all)]
disaster_data = disaster_data_all[~np.isnan(disaster_data_all)]

one = np.ones(shape=len(disaster_data))

def ff(y):
    z = y[1]*np.heaviside(y[0]-x, 0) + y[2]*np.heaviside(x-y[0], 0)
    return z


r1 = Expon(var_name='r1', scale=1.0+eps)
r2 = Expon(var_name='r2', scale=1.0-eps)
r1a = Prod(var_name='r1a', var=r1, arg=one, shape=len(x))
r2a = Prod(var_name='r2a', var=r2, arg=one, shape=len(x))
u = Beta(var_name='u', a=s, b=s)
m = Sum(var_name='m', var=u, arg=-x, shape=len(x))
v = IfElse('v', [m, r1a, r2a], shape=len(x))
y = Poisson(var_name='y', x='v')

data = Data(var_name='y', var_values=disaster_data)

model = Model([u, r1, r2, r1a, r2a, m, v, y],
              var_list=['u', 'r1', 'r2'],
              data=data)

z0 = np.array([0.4, 3., 0.9])

sampler = StochasticDualAverage(log_pdf=model.log_posterior, start_x=z0,
                        renormalize_mass=True,
                        find_start=True,
                        var_names=model.var_list)

sampler.delta = 1e-2
sampler.L_mean = 10
sampler.cnt = 200
sampler.sample(N, warmup=W)

trace = sampler.points_dict()

r = np.arange(N)
fig, ax = plt.subplots(nrows=3, ncols=2)
ax[0, 0].plot(r, trace['u']*len(disaster_data))
ax[1, 0].plot(r, trace['r1'])
ax[2, 0].plot(r, trace['r2'])
ax[0, 0].set_ylabel('u', rotation=0)
ax[1, 0].set_ylabel(r'$r_1$', rotation=0)
ax[2, 0].set_ylabel(r'$r_2$', rotation=0)
ax[0, 1].hist(trace['u']*len(disaster_data), bins=bins, density=True)
ax[1, 1].hist(trace['r1'], bins=bins, density=True)
ax[2, 1].hist(trace['r2'], bins=bins, density=True)

fig.tight_layout()
fig.savefig('trace.eps')
plt.close(fig)


y = sampler.points()

wa = y.transpose()
print(wa[0])
q = model.f_deterministic(wa[0])
print(np.shape(wa))
print(q)

f_aux = jit(model.f_deterministic)
r = [f_aux(elem) for elem in wa]
y1 = np.transpose([model.predictive(elem) for elem in r])

y1mn = np.array([np.mean(elem) for elem in y1])

y1l = np.array([np.quantile(elem, 0.1) for elem in y1])

y1h = np.array([np.quantile(elem, 0.9) for elem in y1])


fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(x, y1mn, color='k')
ax.scatter(x, disaster_data, color='r', marker='x')

ax.fill_between(x, y1l, y1h, color='r', alpha=0.5)
fig.savefig('posterior_predictive.pdf')
plt.close(fig)

plt.figure(figsize=(9,7))
sns.jointplot(data=trace,
              x='r1', y='r2', kind="hex", color="#4CB391")
plt.xlabel("early_rate")
plt.ylabel("late_rate");
fig = plt.gcf()
fig.savefig('kde.pdf')
plt.close(fig)
#
