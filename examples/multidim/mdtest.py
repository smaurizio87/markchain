import numpy as nn
import pandas as pd
from markchain.utils import trace_summary
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from markchain.distributions import Beta, Binom, Model, Data, Normal, Exp
from markchain.classes import Gibbs, HamiltonianMC

palette = sns.color_palette("viridis")

M = 51
N = 8

data = nn.random.normal(loc=10, scale=2, size=(M, N))

theta = Normal(var_name='theta', loc=np.array([10]*N),
               scale=np.array([5]*N),
               shape = N)
log_phi = Normal(var_name='log_phi', loc=0, scale=3)
phi = Exp(var_name='phi', var=log_phi)
x = Normal(var_name='x', loc='theta', scale='phi')

model = Model([theta, log_phi, phi, x], var_list=['theta', 'log_phi'],
              extra_vars=['phi'],
              data=Data(var_name='x', var_values=data))


x0 = nn.random.normal(loc=4, scale=1, size=N+1)
print(model.log_posterior(x0))
