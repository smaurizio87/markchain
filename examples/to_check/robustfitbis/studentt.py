import numpy as nn
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from markchain.distributions import Beta, Binom, Model, Data, Normal, StudentT, Exp, Linear, SumList, Cauchy, Pow
from markchain.classes import Gibbs, HamiltonianMC
from scipy.stats import t, norm

palette = sns.color_palette("viridis")

size = 150

x_v = np.linspace(-2, 2, size)#t(loc=2, scale=1, df=4).rvs(size)
m_v = nn.random.normal(loc=5, scale=0.4, size=size)
y_v = x_v*m_v + 0.6*nn.random.normal(size=size) + nn.random.normal(loc=5, scale=0.6, size=size)

ones = np.ones(size)

mu = Normal(var_name='mu', loc=0, scale=20)
x0 = Normal(var_name='x0', loc=0, scale=20)

log_sigma = Cauchy(var_name='log_sigma', loc=10, scale=1)
sigma = Pow(var_name='sigma', var=log_sigma, n=2)

x = Linear(var_name='x', var=mu, coeffs=x_v)
w = Linear(var_name='w', var=x0, coeffs=ones)
z = SumList(var_name='z', var_list=[x, w])
y = Normal(var_name='y', loc='x', scale='sigma')

model = Model([x0, mu, log_sigma, sigma, x, w, z, y],
              var_list=['x0','mu', 'log_sigma'],
              extra_vars=['sigma', 'x', 'w', 'z'],
              data=Data(var_name='y', var_values=y_v))


sampler = HamiltonianMC(log_pdf=model.log_posterior, start_x=np.array([1, 5, 3]),
                        masses=np.diag(np.array([0.0001, 0.002, 0.01])),
                        J=model.d_log_posterior,
                        integrator='leapfrog')
sampler.delta = 1e-2
sampler.L = 2
# sampler.sample(200, warmup=100)
# print(sampler.points())

# sampler = Gibbs(pdf=model.log_posterior, start_x=np.array([0.2]))
t0 = timer()
sampler.sample(60000, warmup=60000)
t1 = timer()
print(f"{t1-t0}")
w = sampler.points()

ys = np.array([nn.random.normal(loc=w[0]+elem*w[1], scale=np.exp(w[2])) for elem in x_v])
ym = np.array([np.quantile(elem, 0.1) for elem in ys])
yp = np.array([np.quantile(elem, 0.9) for elem in ys])

print(f"{np.mean(w[0])} {np.median(w[0])} {np.std(w[0])}")
print(f"{np.quantile(w[0], 0.1)} {np.quantile(w[0], 0.9)}")

print(f"{np.mean(w[1])} {np.median(w[1])} {np.std(w[1])}")
print(f"{np.quantile(w[1], 0.1)} {np.quantile(w[1], 0.9)}")

print(f"{np.mean(w[2])} {np.median(w[2])} {np.std(w[2])}")
print(f"{np.quantile(w[2], 0.1)} {np.quantile(w[2], 0.9)}")

z = np.arange(len(w[0]))
fig, ax = plt.subplots(nrows=2, ncols=3)
ax[0, 0].plot(z, w[0], color=palette[0])
ax[0, 1].plot(z, w[1], color=palette[0])
ax[0, 2].plot(z, w[2], color=palette[0])
ax[1, 0].hist(w[0], density=True, bins=30, color=palette[0])
ax[1, 1].hist(w[1], density=True, bins=30, color=palette[0])
ax[1, 2].hist(w[2], density=True, bins=30, color=palette[0])
fig.tight_layout()
fig.savefig('trace_student.eps')
plt.close(fig)

zz = np.arange(np.min(x_v), np.max(x_v), 1e-3)


y_va = np.array([np.mean(w[0] + elem*w[1]) for elem in x_v])

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.scatter(x_v, y_v, color='k', marker='x')
ax1.scatter(x_v, y_va, color=palette[0])
ax1.fill_between(x_v, ym, yp, color=palette[1], alpha=0.5)
# ax1.plot(zz, np.median(w[0]) + zz*np.median(w[1]), color=palette[0])
# ax1.fill_between(zz, zz*mn, zz*mx, color=palette[1], alpha=0.5)
fig1.savefig('student_fit.pdf')
plt.close(fig1)
