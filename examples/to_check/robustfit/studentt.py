import numpy as nn
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from markchain.distributions import Beta, Binom, Model, Data, Normal, StudentT, Exp, Linear
from markchain.classes import Gibbs, HamiltonianMC
from scipy.stats import t

palette = sns.color_palette("viridis")

size = 50

x_v = t(loc=2, scale=1, df=4).rvs(size)
m_v = 5 + 0.4*nn.random.standard_t(df=5, size=size)
y_v = x_v*m_v

mu = Normal(var_name='mu', loc=0, scale=50)
log_sigma = Normal(var_name='log_sigma', loc=0, scale=3)
sigma = Exp(var_name='sigma', var=log_sigma)

log_sigma = Normal(var_name='log_sigma', loc=0, scale=3)
sigma = Exp(var_name='sigma', var=log_sigma)

log_df = Normal(var_name='log_df', loc=0, scale=1)
df = Exp(var_name='df', var=log_df)

x = Linear(var_name='x', var=mu, coeffs=x_v)
y = StudentT(var_name='y', loc='x', scale='sigma', df='df')

model = Model([mu, log_sigma, sigma, log_df, df, x, y],
              var_list=['mu', 'log_sigma', 'log_df'],
              extra_vars=['sigma', 'df', 'x'],
              data=Data(var_name='y', var_values=y_v))

sampler = HamiltonianMC(log_pdf=model.log_posterior, start_x=np.array([10, 1, 0]),
                        masses=np.diag(np.array([0.2, 0.1, .1])),
                        J=model.d_log_posterior,
                        integrator='third_order')
sampler.delta = 1e-2
sampler.L = 3
# sampler.sample(200, warmup=100)
# print(sampler.points())

# sampler = Gibbs(pdf=model.log_posterior, start_x=np.array([0.2]))
t0 = timer()
sampler.sample(50000, warmup=5000)
t1 = timer()
print(f"{t1-t0}")
w = sampler.points()

print(f"{np.mean(w[0])} {np.median(w[0])} {np.std(w[0])}")
print(f"{np.quantile(w[0], 0.1)} {np.quantile(w[0], 0.9)}")

print(f"{np.mean(w[1])} {np.median(w[1])} {np.std(w[1])}")
print(f"{np.quantile(w[1], 0.1)} {np.quantile(w[1], 0.9)}")

print(f"{np.mean(w[2])} {np.median(w[2])} {np.std(w[2])}")
print(f"{np.quantile(w[2], 0.1)} {np.quantile(w[2], 0.9)}")

print(f"{np.mean(y_v)} {np.std(y_v)} {np.log(np.std(y_v))}")
print(f"{np.mean(m_v)} {np.std(m_v)} {np.log(np.std(m_v))}")

z = np.arange(len(w[0]))
fig, ax = plt.subplots(nrows=2, ncols=3)
ax[0, 0].plot(z, w[0], color=palette[0])
ax[0, 1].plot(z, w[1], color=palette[0])
ax[0, 2].plot(z, w[2], color=palette[0])
ax[1, 0].hist(w[0], density=True, bins=30, color=palette[0])
ax[1, 1].hist(w[1], density=True, bins=30, color=palette[0])
ax[1, 2].hist(w[2], density=True, bins=30, color=palette[0])
fig.tight_layout()
fig.savefig('trace_student.eps')
plt.close(fig)

zz = np.arange(np.min(x_v), np.max(x_v), 1e-3)

za = t(loc=w[0], scale=np.exp(w[1]), df=np.exp(w[2])).rvs()
mn = np.quantile(za, 0.1)
mx = np.quantile(za, 0.9)

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.scatter(x_v, y_v, color='k', marker='x')
ax1.plot(zz, zz*np.median(w[0]), color=palette[0])
ax1.fill_between(zz, zz*mn, zz*mx, color=palette[1], alpha=0.5)
fig1.savefig('student_fit.pdf')
plt.close(fig1)
