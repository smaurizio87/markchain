from jax import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from markchain.distributions import *
from markchain.classes import HamiltonianMC, SeriesMC

N = 80000

data = pd.read_csv('./efronmorris.csv', sep='\t')
at_bats, hits = data[["At-Bats", "Hits"]].to_numpy().T

at_bats = at_bats[::-1]
hits = hits[::-1]
names = data['LastName'].values[::-1]
h = hits
b = at_bats

class Faux(Deterministic1D):
    def __init__(self, var_name, var):
        super().__init__(var, var_name)

        def f(x):
            return 1-x
        self.f = f

phi = Uniform(var_name='phi', low=0, high=1)

kappa_log = Expon(var_name='kappa_log', scale=1/1.5)
kappa = Exp(var_name='kappa', var=kappa_log)

alpha = ProdList(var_name='alpha', var_list=[phi, kappa])
min_beta = Faux(var_name='min_beta', var=phi)
beta = ProdList(var_name='beta', var_list=[min_beta, kappa])
theta = Beta(var_name=f'theta', a='alpha', b='beta', shape=len(h))
y = Binom(var_name='y', n=at_bats, p=f'theta')

model = Model([phi, kappa_log, kappa, alpha, min_beta, beta, theta, y],
              var_list=['phi', 'kappa_log', 'theta'],
              data=Data(var_name='y', var_values=hits))

masses = np.diag(np.array([0.8, 0.2]+[1]*len(names)))
start_x = np.array([0.3,  0.5]+list(0.*h/b + 0.3))
print(model.log_posterior(start_x))
sampler = SeriesMC(log_pdf=model.log_posterior,
                   start_x=start_x,
                   var_names=model.var_list,
                   group_masses=[0, 1, 2],
                   renormalize_mass=True,
                   )
sampler.delta = 0.02
sampler.L = 10
sampler.cnt = 50
sampler.thr_ks = 0.1
sampler.sample(N=N, warmup=N)
print(sampler.m)
print(sampler.delta, sampler.L, sampler.cnt)
print(sampler.x_temp)
trace = sampler.points_dict()
z = np.arange(len(trace['phi']))

fig, ax = plt.subplots(nrows=2, ncols=2)
for k, elem in enumerate(['phi', 'kappa_log']):
    ax[k, 0].plot(z, trace[elem])
    ax[k, 1].hist(trace[elem], bins=30, density=True)
fig.tight_layout()
fig.savefig('trace.eps')
plt.close(fig)
# th = {k: trace[f"theta_{k}"] for k in range(len(names))}
# fig, ax = plt.subplots(nrows=6, ncols=3)
# for k, elem in enumerate(th):
#     i = k % 6
#     j = k // 6
#     ax[i, j].hist(th[k], bins=30, density=True)
#     ax[i, j].set_title(names[k])
#     print(names[k], hits[k]/at_bats[k], np.mean(th[k]), np.quantile(th[k], 0.05), np.quantile(th[k], 0.95))
# fig.tight_layout()
# fig.savefig('thetas.eps')
# plt.close(fig)
# plt.close(fig)
