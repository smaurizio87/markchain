import pandas as pd
import numpy as nn
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from markchain.distributions import Beta, Binom, Model, Data, Normal, StudentT, Exp
from markchain.classes import Gibbs, HamiltonianMC, StochasticDualAverage
from markchain.utils import plot_trace, trace_summary

palette = sns.color_palette("viridis")


data = 5 + 0.4*nn.random.standard_t(df=5, size=20)

mu = Normal(var_name='mu', loc=0, scale=50)
log_sigma = Normal(var_name='log_sigma', loc=0, scale=3)
sigma = Exp(var_name='sigma', var=log_sigma)

log_sigma = Normal(var_name='log_sigma', loc=0, scale=3)
sigma = Exp(var_name='sigma', var=log_sigma)

log_df = Normal(var_name='log_df', loc=0, scale=1)
df = Exp(var_name='df', var=log_df)

x = StudentT(var_name='x', loc='mu', scale='sigma', df='df')

model = Model([mu, log_sigma, sigma, log_df, df, x], var_list=['mu', 'log_sigma', 'log_df'],
              extra_vars=['sigma', 'df'],
              data=Data(var_name='x', var_values=data))




sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                        start_x=np.array([5, 1, 0]),
                        var_names=model.var_list,
                                integrator='third_order',
                                decorrelate=False,
                        find_start=True)
sampler.delta = 2e-1
sampler.L_mean = 1
sampler.cnt = 100
# sampler.sample(200, warmup=100)
# print(sampler.points())

# sampler = Gibbs(pdf=model.log_posterior, start_x=np.array([0.2]))
t0 = timer()
sampler.sample(50000, warmup=50000)
trace = sampler.points_dict()
print(pd.DataFrame(trace_summary(trace)))
fig = plot_trace(trace)
fig.savefig('trace.eps')
plt.close(fig)

t1 = timer()
print(f"{t1-t0}")
w = sampler.points()
print(f"{np.mean(w[0])} {np.median(w[0])} {np.std(w[0])}")
print(f"{np.quantile(w[0], 0.1)} {np.quantile(w[0], 0.9)}")

print(f"{np.mean(w[1])} {np.median(w[1])} {np.std(w[1])}")
print(f"{np.quantile(w[1], 0.1)} {np.quantile(w[1], 0.9)}")

print(f"{np.mean(w[2])} {np.median(w[2])} {np.std(w[2])}")
print(f"{np.quantile(w[2], 0.1)} {np.quantile(w[2], 0.9)}")

z = np.arange(len(w[0]))
fig, ax = plt.subplots(nrows=2, ncols=3)
ax[0, 0].plot(z, w[0], color=palette[0])
ax[0, 1].plot(z, w[1], color=palette[0])
ax[0, 2].plot(z, w[2], color=palette[0])
ax[1, 0].hist(w[0], density=True, bins=30, color=palette[0])
ax[1, 1].hist(w[1], density=True, bins=30, color=palette[0])
ax[1, 2].hist(w[2], density=True, bins=30, color=palette[0])
fig.tight_layout()
fig.savefig('trace_student.eps')
plt.close(fig)
