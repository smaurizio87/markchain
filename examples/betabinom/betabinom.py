import os

os.environ['CUDA_VISIBLE_DEVICES']=''

import numpy as nn
import pandas as pd
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from markchain.distributions import Beta, Binom, Model, Data
from markchain.classes import HamiltonianMC, StochasticDualAverage
from markchain.utils import trace_summary

palette = sns.color_palette("viridis")

N = 5000


data = nn.random.choice(2, 80, p=[0.7, 0.3])

theta = Beta(var_name='theta', a=0.5, b=0.5)
x = Binom(var_name='x', p='theta', n=1)

model = Model([theta, x], data=Data(var_name='x', var_values=data))

f_aux = jit(model.f_deterministic)

sampler_prior = StochasticDualAverage(log_pdf=model.log_prior,
                                      start_x=np.array([0.5]),
                                masses=np.diag(np.array([1.0])),
                                      var_names=model.var_list)

sampler_prior.delta = 3e-2
sampler_prior.L = 3
# sampler.sample(200, warmup=100)
# print(sampler.points())

# sampler = Gibbs(pdf=model.log_posterior, start_x=np.array([0.2]))
print('Sampling prior')
t0 = timer()
sampler_prior.sample(N, warmup=N)
t1 = timer()
print(f"{t1-t0}")
w_prior = sampler_prior.points_dict()
recap = pd.DataFrame.from_dict(trace_summary(w_prior))
print(recap)
z = np.arange(len(w_prior['theta']))


wp = sampler_prior.points()
wpa = wp.transpose()
yp = [f_aux(elem) for elem in wpa]
prior_predictive = [model.predictive(elem)[0] for elem in yp]


sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                                start_x=np.array([0.5]),
                                masses=np.diag(np.array([1.0])),
                        var_names=model.var_list)
sampler.delta = 3e-2
sampler.L = 8
# sampler.sample(200, warmup=100)
# print(sampler.points())

# sampler = Gibbs(pdf=model.log_posterior, start_x=np.array([0.2]))
print('Sampling posterior')
t0 = timer()
sampler.sample(N, warmup=N)
t1 = timer()
print(f"{t1-t0}")
w = sampler.points_dict()
recap = pd.DataFrame.from_dict(trace_summary(w))
print(recap)
z = np.arange(len(w['theta']))

ww = sampler.points()
wa = ww.transpose()
y = [f_aux(elem) for elem in wa]
posterior_predictive = [model.predictive(elem)[0] for elem in y]


fig, ax = plt.subplots(nrows=2, ncols=2)
ax[0, 0].plot(z, w['theta'], color=palette[0], label='posterior')
ax[0, 1].hist(w['theta'], density=True, bins=30, color=palette[0])

ax[1, 0].plot(z, w_prior['theta'], color=palette[0], label='prior')
ax[1, 1].hist(w_prior['theta'], density=True, bins=30, color=palette[0])
fig.tight_layout()
legend = fig.legend()
fig.savefig('trace_beta_binom_prova.eps')
plt.close(fig)


# posterior_predictive = x.sample_from_trace(w)
fig, ax = plt.subplots(nrows=3)

d0 = {'y': prior_predictive,
      'kind': ['prior']*len(prior_predictive)}
d1 = {'y': posterior_predictive,
      'kind': ['posterior']*len(posterior_predictive)}
d2 = {'y': data,
      'kind': ['data']*len(data)}

df0 = pd.DataFrame().from_dict(d0)
df1 = pd.DataFrame().from_dict(d1)
df2 = pd.DataFrame().from_dict(d2)

df = pd.concat([df0, df1, df2])

fig = plt.figure()
ax = fig.add_subplot(111)

g = sns.histplot(data=df, x="y", hue='kind', palette='winter', ax=ax,
                 common_norm=False, shrink=-0.5, binwidth=0.3,
                 stat='probability', multiple='dodge',
                 hue_order=['data', 'prior', 'posterior'])

sns.move_legend(g, "upper center")
ax.set_xticks([0, 1])
fig.savefig('predictive_prova.eps')
plt.close(fig)
