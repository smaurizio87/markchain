import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from markchain.distributions import GaussianMixture, \
Normal, Data, Model, Dirichlet, DirichletNew, Expon, Exp, Beta, PowerLaw
from markchain.classes import HamiltonianMC, StochasticDualAverage
from markchain.utils import plot_trace, trace_summary


N = 20000
W = 20000

mu = Normal(var_name='mu', loc=np.array([-1, 1]), scale=np.array([.4, .4]),
            shape=2)

log_sig = Normal(var_name='log_sig', loc=np.array([-1, -1]), scale=np.array([0.5, 0.5]), shape=2)
sig = Exp(var_name='sig', var=log_sig)
v = DirichletNew(var_name='v', alpha=np.array([1., 2.]))


w = GaussianMixture(var_name='w', loc='mu',
                    scale='sig', c=np.array([0.2, 0.8]), shape=2)

data = np.array([-1.2, 1.3, 1.1, 0.9, -0.8, 1.4, 1.2])

model = Model([mu, log_sig, sig, v, w], var_list=['mu', 'log_sig', 'v'],
              data=Data(var_name='w', var_values=data))


z0 = np.array([-1, 1, -0.5, -0.5, 0.2])


print(model.log_posterior(z0))

sampler = StochasticDualAverage(log_pdf=model.log_posterior, start_x=z0, find_start=True)
sampler.delta = 0.2
sampler.cnt = 500
sampler.sample(N, warmup=W)

trc = sampler.points()

trace = {'mu_0': trc[0], 'mu_1': trc[1], 'l0': np.exp(trc[2]), 'l1': np.exp(trc[3]),
         'c0': trc[4]}


print(trace_summary(pd.DataFrame(trace)))

fig = plot_trace(trace)
fig.savefig('trace.eps')
plt.close(fig)
