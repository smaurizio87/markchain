from jax import numpy as np
from jax import jit
import numpy as nn
import pandas as pd
from matplotlib import pyplot as plt
from markchain.distributions import Normal, GaussianProcess, Gamma, Exp, Model, Data
from markchain.gpkernel import RBF, WhiteNoise, PeriodicKernel, ConstantKernel,\
    LinearKernel, RationalQuadratic, Matern0, Matern1, Matern2, KernelApply, OrnsteinUhlenbeck
from markchain.classes import HamiltonianMC, SeriesMC, StochasticDualAverage
from markchain.utils import plot_trace, trace_summary
from markchain.linalg import cholesky, cho_solve

df = pd.read_csv('./sea_arrivals_ascii.csv', sep=',')

N = 80000
W = 80000
N_sample = 5000

print(df)
print(df.head())

df['data_date'] = df['data_date'].astype(str)
print(df['data_date'])

date_vals = df['data_date'].values.astype(str)
print(date_vals)

df['yyyymm'] = [elem[:-3] for elem in date_vals]
print(df)

df1 = df[['yyyymm', 'individuals']]
print(df1)
df_group = df1.groupby(['yyyymm']).sum().reset_index()

print(df_group)

fig = plt.figure()
ax = fig.add_subplot(211)
ax1 = fig.add_subplot(212)
ax.plot(df_group['yyyymm'], df_group['individuals'])
ax.set_xticks([str(elem)+'-01' for elem in range(2016, 2024)])

ax1.plot(df_group['yyyymm'], nn.log(df_group['individuals']))
ax1.set_xticks([str(elem)+'-01' for elem in range(2016, 2024)])
fig.tight_layout()
fig.savefig('gp.eps')
plt.close(fig)

Y0 = np.log(df_group['individuals'].values)
sz0 = len(Y0)
X0 = np.arange(sz0).reshape((int(sz0), 1))

Y = np.log(df_group['individuals'].values[:-6])
sz = len(Y)
X = np.arange(sz).reshape((int(sz), 1))




log_sigma_0 = Normal(var_name='log_sigma_0', loc=-1, scale=5)
log_sigma_1 = Normal(var_name='log_sigma_1', loc=-1, scale=3)
log_sigma_2 = Normal(var_name='log_sigma_2', loc=2, scale=3)
sigma_0 = Exp(var_name='sigma_0', var=log_sigma_0)
sigma_1 = Exp(var_name='sigma_1', var=log_sigma_1)
sigma_2 = Exp(var_name='sigma_2', var=log_sigma_2)
log_theta = Normal(var_name='log_theta', loc=1, scale=2)
theta = Exp(var_name='theta', var=log_theta)

log_theta0 = Normal(var_name='log_theta0', loc=0, scale=2)
theta0 = Exp(var_name='theta0', var=log_theta0)

log_theta1 = Normal(var_name='log_theta1', loc=-1, scale=3)
theta1 = Exp(var_name='theta1', var=log_theta1)

log_theta2 = Normal(var_name='log_theta2', loc=-1, scale=3)
theta2 = Exp(var_name='theta2', var=log_theta1)

# c = Normal(var_name='c', loc=0, scale=100)

k0 = ConstantKernel(sigma='theta0')
k1 = WhiteNoise(sigma='sigma_0')
k2 = RBF(sigma='sigma_1', theta='theta')
k3 = PeriodicKernel(sigma='sigma_2', length='theta1', period=12.0)

# k4 = LinearKernel(theta='theta2', c='c')

k = k0 + k1 + k2 + k3 #  + k4

y = GaussianProcess(var_name='y', kernel=k,
                    sigma_0='sigma_0',
                    sigma_1='sigma_1',
                    sigma_2='sigma_2',
                    theta='theta',
                    theta0='theta0',
                    theta1='theta1',
                    # theta2='theta2',
                    # c='c',
                    period=12,
                    shape=sz,
                    X=X)

data = Data(var_name='y', var_values=Y)

model = Model([log_sigma_0, log_sigma_1, sigma_0, sigma_1, log_theta,
               theta, log_sigma_2, sigma_2, log_theta1, theta1,
               log_theta0, theta0,
               # log_theta2, theta2,
               y],
              var_list=['log_sigma_0', 'log_sigma_1', 'log_theta', 'log_theta0',
                        'log_sigma_2', 'log_theta1',
                        # 'log_theta2'
                        # , 'c'
                        ],
              data=data)

z0 = np.array([0., 0., 0., 0., 0., 0.])
print(model.log_posterior(z0))



sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                                start_x=z0,
                                find_start=True,
                                var_names=model.var_list,
                                decorrelate=True,
                                renormalize_mass=True)
# #
# sampler.delta = 1e-1
# sampler.L_mean = 15
#

sampler.sample(N, warmup=W)
#
trace = sampler.points_dict()

print(pd.DataFrame(trace_summary(trace)))


fig = plot_trace(trace)
fig.savefig('trace_prova_matern.eps')
plt.close(fig)

pts0 = sampler.points()

pp = nn.array(pts0)
nn.savetxt('trace_matern.csv', pp)


pts = pts0.T[nn.random.choice(range(len(pts0)), N_sample)]
print(np.shape(pts))



z = np.linspace(1.05*np.min(X), 1.05*np.max(X), 200)

pts = pts0.T[nn.random.choice(range(len(pts0)), N_sample)].T
print(np.shape(pts))

v = model.fill_trace_new(pts)

z = np.linspace(1.05*np.min(X0), 1.05*np.max(X0), 200).reshape((200, 1))

w = y.sample_fstar(X, Y, z, v).T

print('plot')
mn = [np.median(elem) for elem in w]
qq05 = [np.quantile(elem, 0.1) for elem in w]
qq95 = [np.quantile(elem, 0.9) for elem in w]


Xpl = X0.reshape((sz0))
z1 = z.reshape(np.shape(z)[0])
#
mxx = 1.1*np.max(np.abs(Y0))
fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(Xpl, Y0, color='k', marker='x')
ax.plot(z1, mn, color='r', ls='--')
ax.fill_between(z1, qq05, qq95, color='r', alpha=0.6)
ax.axvline(x=X.reshape(-1)[-1], ymin=0, ymax=10)
ax.set_ylim(-mxx, mxx)
fig.savefig('posterior_prova_rbf_const_high.pdf')
plt.close(fig)
