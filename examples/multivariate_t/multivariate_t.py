import numpy as nn
from jax import numpy as np
from jax import jit
import pandas as pd
from markchain.distributions import MVNormal, Data, Pow, \
    Model, Normal, ProdList, Uniform
from markchain.classes import HamiltonianMC
from markchain.utils import trace_summary
import seaborn as sns
from matplotlib import pyplot as plt

# TODO: update with MVNormal

palette = sns.color_palette("viridis")

N = 40000

mu = Normal(var_name='mu', loc=0, scale=2, shape=2)

x = MVNormal(var_name='x',
                       loc='mu',
                       cov=np.diag(np.array([2, 1])), shape=2)

data = nn.random.multivariate_normal(mean=nn.array([0.5, -0.5]),
                                     cov=nn.array([[2, 0], [0, 1]]),
                                     size=200)

model = Model([mu, x],
              var_list=['mu'],
              data=Data(var_name='x', var_values=data))

start_x = nn.array([0.0, 0.0])

print(model.log_posterior(start_x))

sampler = HamiltonianMC(log_pdf=model.log_posterior, start_x=start_x,
                        renormalize_mass=True,
                        var_names=model.var_list
                        )
#
sampler.sample(N, warmup=N)
# trace = sampler.points_dict()
# print(trace)

w = sampler.points_dict()
recap = pd.DataFrame.from_dict(trace_summary(w))
print(recap)

print(w['mu'])
ww = sampler.points()
z = np.arange(len(ww[0]))

fig, ax = plt.subplots(nrows=len(ww), ncols=2)
for k, var in enumerate(ww):
    ax[k, 0].plot(z, var, color=palette[0])
    ax[k, 1].hist(var, density=True, bins=30, color=palette[0])
fig.tight_layout()
fig.savefig('trace_normal.eps')
plt.close(fig)

# wa = ww.transpose()
# f_aux = jit(model.f_deterministic)
# y = [f_aux(elem) for elem in wa]
# print(np.shape(wa))
# print(np.shape(data))
# # print(y[0])
# tmp = [model.predictive(elem)[0] for elem in y]
# print(np.shape(tmp))
#
# z_0 = [elem[0] for elem in tmp]
# z_1 = [elem[1] for elem in tmp]
#
# pred = {r'$x_0$': z_0, r'$x_1$': z_1}
# # x_pred = posterior_predictive
# #
# v_0 = [elem[0] for elem in data]
# v_1 = [elem[1] for elem in data]
# fig, ax = plt.subplots(nrows=2, ncols=1)
# # ax[0][0].hist(z_0, label=r'$x_0$', color=palette[0], density=True, bins=50)
# ax[0].hist(v_0, color='grey', label='data', density=True, bins=50)
# sns.kdeplot(data=pred, x=r"$x_0$", ax=ax[0])
# ax[0].set_xlim([-6, 6])
# # ax[1][0].hist(z_1, label=r'$x_1$', color=palette[0], density=True, bins=50)
# ax[1].hist(v_1, color='grey', label='data', density=True, bins=50)
# sns.kdeplot(data=pred, x=r"$x_1$", ax=ax[1])
# ax[1].set_xlim([-6, 6])
# fig.tight_layout()
# fig.savefig('posterior_predictive.eps')
# plt.close(fig)
#
# fig = plt.figure()
# ax = fig.add_subplot(111)
# sns.kdeplot(pred, x=r'$x_0$', y=r'$x_1$', levels=100,
#             cmap='mako', ax=ax, fill=True, alpha=0.7)
# ax.scatter(v_0, v_1, marker='x', color='k')
# ax.set_xlim([-4, 4])
# ax.set_ylim([-4, 4])
# fig.savefig('kde.pdf')
# plt.close(fig)
#
# #
# # fig = plt.figure()
# # ax = fig.add_subplot(111)
# # sns.kdeplot(w, x='theta', y='log_phi', fill=True, thresh=0, levels=100,
# #             cmap='viridis', ax=ax)
# # fig.savefig('kde.pdf')
# # plt.close(fig)
