from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from markchain.distributions import Normal, Logistic, Multinomial, Linear,\
    SumList, Data, Model, SoftMax, Log
from markchain.classes import HamiltonianMC, SeriesMC, DualAverageMC
from markchain.utils import plot_trace, trace_summary

N = 20000
W = 20000

b0 = Normal(var_name='b0', loc=np.array([0., 0., 0.]),
            scale=np.array([1., 1., 1.]))

df = pd.read_csv('./multi.csv')

x1 = df['length'].values
x0 = np.ones((len(x1)))

dct = {'I': np.array([1, 0, 0]),
       'F': np.array([0, 1, 0]),
       'O': np.array([0, 0, 1])}

print(x0)
print(x1)

yv = np.array([dct[elem] for elem in df['food'].values]).T
print(yv)

c0 = Linear(var_name='c0', var=b0, coeffs=x1)


w = Logistic(var_name='w', var=c0)

y = Multinomial(var_name='y', p='w', n=len(x0), shape=3)

data = Data(var_name='y', var_values=yv)

model = Model([b0, c0, w, y],
              var_list=['b0'], data=data)

z0 = np.array([0.0, 0.0, 0.])
print(model.log_posterior(z0))

sampler = DualAverageMC(log_pdf=model.log_posterior,
                        var_names=['b0', 'b1', 'b2'],
                        start_x=z0)

sampler.sample(N, warmup=W)
sampler.cnt = 500
sampler.delta = 1e-2
sampler.L_mean = 3

trace = sampler.points_dict()
print(pd.DataFrame(trace_summary(trace)))

fig = plot_trace(trace)
fig.savefig('trace.eps')
plt.close(fig)

