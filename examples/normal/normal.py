import numpy as nn
import pandas as pd
from markchain.utils import trace_summary
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from markchain.distributions import Beta, Binom, Model, Data, Normal, Exp
from markchain.classes import Gibbs, HamiltonianMC, DualAverageMC, StochasticDualAverage

palette = sns.color_palette("viridis")


data = nn.random.normal(loc=10, scale=2, size=1000)

theta = Normal(var_name='theta', loc=10, scale=5)
log_phi = Normal(var_name='log_phi', loc=0, scale=3)
phi = Exp(var_name='phi', var=log_phi)
x = Normal(var_name='x', loc='theta', scale='phi')

model = Model([theta, log_phi, phi, x], var_list=['theta', 'log_phi'],
              extra_vars=['phi'],
              data=Data(var_name='x', var_values=data))




sampler = StochasticDualAverage(log_pdf=model.log_posterior, start_x=np.array([10., 1.]),
                                decorrelate=False,
                        var_names=model.var_list)
# sampler.sample(200, warmup=100)
# print(sampler.points())

# sampler = Gibbs(pdf=model.log_posterior, start_x=np.array([0.2]))
t0 = timer()
sampler.sample(20000, warmup=20000)
t1 = timer()
print(f"{t1-t0}")

w = sampler.points_dict()
recap = pd.DataFrame.from_dict(trace_summary(w))
print(recap)

ww = sampler.points()
z = np.arange(len(ww[0]))
fig, ax = plt.subplots(nrows=2, ncols=2)
ax[0, 0].plot(z, ww[0], color=palette[0])
ax[0, 1].plot(z, ww[1], color=palette[1])
ax[1, 0].hist(ww[0], density=True, bins=30, color=palette[0])
ax[1, 1].hist(ww[1], density=True, bins=30, color=palette[1])

fig.tight_layout()
fig.savefig('trace_normal_prova.eps')
plt.close(fig)

wa = ww.transpose()
f_aux = jit(model.f_deterministic)
y = [f_aux(elem) for elem in wa]
posterior_predictive = [model.predictive(elem)[0][0] for elem in y]
x_pred = posterior_predictive

fig, ax = plt.subplots(nrows=2)
ax[0].hist(x_pred, label='x', color=palette[0], density=True, bins=50)
ax[1].hist(data, color='grey', label='data', density=True, bins=50)
legend = fig.legend()
fig.tight_layout()
fig.savefig('posterior_predictive_prova.eps')
plt.close(fig)

fig = plt.figure()
ax = fig.add_subplot(111)
sns.kdeplot(w, x='theta', y='log_phi', fill=True, thresh=0, levels=100,
            cmap='viridis', ax=ax)
fig.savefig('kde_ptova.pdf')
plt.close(fig)
