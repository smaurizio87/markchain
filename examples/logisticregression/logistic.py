import random
import numpy as nn
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from markchain.distributions import Beta, Binom, Model, Data, Normal, Exp, Linear, Logistic, SumList
from markchain.classes import Gibbs, HamiltonianMC, StochasticDualAverage
from markchain.utils import trace_summary, plot_trace

palette = sns.color_palette("viridis")

size = 500

data = nn.random.choice(2, size, p=[0.7, 0.3])

x_v = nn.random.normal(loc=3, scale=0.5, size=size)
m_v = nn.linspace(-2, 2, size)

x_0 = nn.random.normal(loc=0, scale=0.2, size=size)
m_0 = nn.ones(shape=size)
y_v = x_v*m_v + x_0*m_0


z = 1/(1+np.exp((-y_v)))

w = np.array([nn.random.binomial(n=size, p=elem) for elem in z])

mu = Normal(var_name='mu', loc=0, scale=50)
mu0 = Normal(var_name='mu0', loc=0, scale=50)
phi = Linear(var=mu, var_name='phi', coeffs=m_v)
phi0 = Linear(var=mu0, var_name='phi0', coeffs=m_0)
x = SumList(var_name='x', var_list=[phi, phi0])
theta = Logistic(var=x, var_name='theta')
y = Binom(var_name='y', p='theta', n=size)
#
model = Model([mu, mu0, phi, phi0, x, theta, y],
              var_list=['mu', 'mu0'],
              data=Data(var_name='y', var_values=w))

sampler = StochasticDualAverage(log_pdf=model.log_posterior, start_x=np.array([0., 0.]),
                                renormalize_mass=True,
                        var_names=model.var_list)

ztst = model.log_posterior(np.array([0.0, 0.]))
N=40000
sampler.sample(N, warmup=N)
#
trace = sampler.points_dict()
print(trace_summary(trace))

fig = plot_trace(trace)
fig.savefig('trace_prova.eps')
plt.close(fig)

ww = sampler.points()
wa = ww.transpose()
f_aux = jit(model.f_deterministic)
y = [f_aux(elem) for elem in wa]
theta_val = [elem[model.complete_vars['theta']] for elem in y]
t0 = timer()
pred = [model.predictive(elem)[0][0] for elem in y]
t1 = timer()
print(t1-t0)
rs0 = np.array(pred)
print(np.shape(pred))
rs = np.array([elem/size for elem in rs0]).transpose()
mn = np.array([np.mean(elem) for elem in rs])
ml = np.array([np.quantile(elem, 0.05) for elem in rs])
mh = np.array([np.quantile(elem, 0.95) for elem in rs])
rsref = w/size
xx = np.arange(len(ww[0]))
fig, ax = plt.subplots(nrows=2, ncols=2)
ax[0, 0].plot(xx, ww[0])
ax[0, 1].plot(xx, ww[1])
ax[1, 0].hist(ww[0], bins=50)
ax[1, 1].hist(ww[1], bins=50)
fig.tight_layout()
fig.savefig('trace_logistic_prova.eps')
plt.close(fig)

fig1 = plt.figure()
ax = fig1.add_subplot(111)
ax.scatter(m_v, rsref, marker='x', color='k')
ax.plot(m_v, mn, color='k')
ax.fill_between(m_v, ml, mh, color='r', alpha=0.5)
fig1.savefig('logistic_plot_prova.pdf')
plt.close(fig1)
