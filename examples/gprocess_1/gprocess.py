import numpy as np
import pandas as pd
from jax.scipy import linalg
from jax import jit
import numpy as nn
from matplotlib import pyplot as plt
from markchain.distributions import GaussianProcess, \
    MVDiagonalNormal, Normal, Uniform, Model, Data, Uniform, Exp, Expon
from markchain.classes import HamiltonianMC, SeriesMC
from markchain.utils import plot_trace, trace_summary
from markchain.linalg import cholesky, cho_solve


L = 50

data = pd.read_csv('./co2_mm_mlo.csv')

# X = data['decimal date'].values[: L]
# Y = data['average'].values[: L]
# print(X)
# print(Y)
#
# sz = len(X)
#
def k0(x, sigma_0, sigma_1, theta):
    w = np.array([[(x[a] - x[b])@(x[a]-x[b]) for a in range(len(x))]
                    for b in range(len(x))])
    z = sigma_1**2*np.exp(-w/theta) + sigma_0**2*np.diag(np.array([1.0]*len(x)))
    return z
# #
# # sz = 15
N = 60000
W = 10000
N_sample = 50
#
sz = 20
X = nn.sort(nn.random.default_rng().multivariate_normal(
    mean=np.array([0, 0]), cov=np.diag(np.array([1, 1])), size=int(sz)))
#             )
# Y = 0.2*nn.random.normal(loc=0, scale=cv) + 0.5*X@X + 0.4*nn.sin(X@X)
# # print(Y)
#
s0 = 0.6
s1 = 0.6
th = 1.0
w = k0(X, s0, s1, th)
print(w)
#
Y = nn.random.multivariate_normal(mean=np.zeros(sz),
                                  cov=w)
#
# print(Y)
log_sigma_0 = Normal(var_name='log_sigma_0', loc=-1, scale=4)
log_sigma_1 = Normal(var_name='log_sigma_1', loc=-1, scale=4)
sigma_0 = Exp(var_name='sigma_0', var=log_sigma_0)
sigma_1 = Exp(var_name='sigma_1', var=log_sigma_1)
log_theta = Normal(var_name='log_theta', loc=0, scale=1)
theta = Exp(var_name='theta', var=log_theta)

y = GaussianProcess(var_name='y', sigma_0='sigma_0',
                    sigma_1='sigma_1',
                    theta='theta',
                    shape=sz,
                    X=X)
#
data = Data(var_name='y', var_values=Y)
#
model = Model([log_sigma_0, log_sigma_1, sigma_0,sigma_1, log_theta,
               theta, y],
              var_list=['log_sigma_0', 'log_sigma_1', 'log_theta'],
              data=data)

z0 = np.array([0., 0., 0.])
print(model.log_posterior(z0))


sampler = HamiltonianMC(log_pdf=model.log_posterior,
                        start_x=z0,
                        find_start=True,
                        var_names=model.var_list,
                        renormalize_mass=True)
sampler.delta = 1e-3
sampler.L_mean = 1
sampler.sample(N, warmup=W)
trace = sampler.points_dict()

# # #
# # # print(pd.DataFrame(trace_summary(trace)))
# # # pts0 = sampler.points()
# # #
# # # pts = pts0.T[ nn.random.randint(np.shape(pts0.T[0]), size=N_sample)]
# # #
# # #
# # # z = np.linspace(1.05*np.min(X), 1.05*np.max(X), 50)
# # #
# # # print('K')
# # #
# # # K00exp = np.array([[(X[a] - X[b])**2 for a in range(len(X))]
# # #                 for b in range(len(X))])
# # #
# # # Kxexp = np.array([[(z[a] - X[b])**2 for a in range(len(z))]
# # #                 for b in range(len(X))])
# # # Kxxexp = np.array([[(z[a] - z[b])**2 for a in range(len(z))]
# # #                 for b in range(len(z))])
# # #
# # # def frnd(params):
# # #     mn = 0
# # #     sigma_0 = params[0]
# # #     sigma_1 = params[1]
# # #     theta = params[2]
# # #     K00a = sigma_1**2*np.exp(-K00exp/theta) + sigma_0**2*np.diag(np.array([1.0]*len(X)))
# # #     Kx = sigma_1**2*np.exp(-Kxexp/theta)
# # #     Kxx = sigma_1**2*np.exp(-Kxxexp/theta)
# # #     # L = linalg.inv(K00a)
# # #     # w = np.dot(L, Y)
# # #     # mean = np.dot(Kx.T, w)
# # #     Lcho = cholesky(K00a)
# # #     alpha = cho_solve(Lcho, Y)
# # #     mn = np.dot(Kx.T, alpha)
# # #     v = cho_solve(Lcho, Kx)
# # #     # cov0 = Kxx - np.dot(Kx.T, np.dot(L, Kx))
# # #     covn = Kxx - np.dot(v.T, Kx)
# # #     # cvn = np.linalg.inv(LL)
# # #     # z = nn.random.default_rng().multivariate_normal(mean=mean, cov=cov)
# # #     return (mn, covn)
# # #
# # #
# # # ff = jit(frnd)
# # # w0 = [ff(elem) for elem in pts]
# # #
# # # # w0 = np.array([ff(z, elem, X) for elem in pts])
# # # mn = np.array([0]*len(z))
# # # cv = np.diag(1+mn)
# # #
# # # w = np.array([nn.random.default_rng().multivariate_normal(mean=elem[0],
# # #                                                           cov=elem[1])
# # #               for elem in w0]).T
# # #
# # # mn = [np.median(elem) for elem in w]
# # # qq05 = [np.quantile(elem, 0.1) for elem in w]
# # # qq95 = [np.quantile(elem, 0.9) for elem in w]
# # #
# # # fig = plot_trace(trace)
# # # fig.savefig('trace.eps')
# # # plt.close(fig)
# # # #
# # # mxx = 1.5*np.max(np.abs(Y))
# # # fig = plt.figure()
# # # ax = fig.add_subplot(111)
# # # ax.plot(X, Y, color='k', marker='x', ls=None)
# # # ax.plot(z, mn, color='r', ls='--')
# # # ax.fill_between(z, qq05, qq95, color='r', alpha=0.6)
# # # ax.set_ylim(-mxx, mxx)
# # # fig.savefig('posterior.pdf')
# # # plt.close(fig)
