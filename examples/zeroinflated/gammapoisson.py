import numpy as nn
import pandas as pd
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from markchain.distributions import Gamma,  Model, Data, Normal, Exp, Expon, Pow, Uniform
from markchain.classes import Gibbs, HamiltonianMC, StochasticDualAverage
from markchain.distributions import ZeroInflatedPoisson as Poisson

palette = sns.color_palette("viridis")


data = nn.loadtxt('./likes.csv', dtype=int)
print(data)

mu = Gamma('mu', a=50, b=1)
p = Uniform(var_name='p')
x = Poisson(var_name='x', x='mu', p='p')

model = Model([mu, p, x], var_list=['mu', 'p'],
              data=Data(var_name='x', var_values=data))

print(model.log_posterior(np.array([1, 0.5])))

sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                        start_x=np.array([1, 0.5]),
                        find_start=True,
                        renormalize_mass=True,
                        var_names=model.var_list,
                        J=model.d_log_posterior)
sampler.delta = 3e-1
sampler.L_mean = 2
# sampler.sample(200, warmup=100)
# print(sampler.points())

# sampler = Gibbs(pdf=model.log_posterior, start_x=np.array([0.2]))
t0 = timer()
sampler.sample(80000, warmup=80000)
t1 = timer()
print(f"{t1-t0}")
trace = sampler.points_dict()
print(trace)
z = np.arange(80000)
fig, ax = plt.subplots(nrows=2, ncols=2)
ax[0, 0].plot(z, trace['mu'], color=palette[0])
ax[0, 1].hist(trace['mu'], density=True, bins=80, color=palette[0])
ax[1, 0].plot(z, trace['p'], color=palette[0])
ax[1, 1].hist(trace['p'], density=True, bins=80, color=palette[0])
fig.tight_layout()
fig.savefig('trace_gamma_poisson.eps')
plt.close(fig)

f_aux = jit(model.f_deterministic)

ww = sampler.points()
wa = ww.transpose()
y = [f_aux(elem) for elem in wa]
print(np.shape(y))
posterior_predictive = [model.predictive(elem)[0] for elem in wa]
print(np.shape(posterior_predictive))


fig, ax = plt.subplots(nrows=2)
ax[0].hist(data, density=True, bins=50, color=palette[0])
ax[1].hist(posterior_predictive, density=True, bins=50, color=palette[0])
fig.tight_layout()
legend = fig.legend()
fig.savefig('posterior_predictive.eps')
plt.close(fig)
