import seaborn as sns
import pandas as pd
from matplotlib import pyplot as plt
from jax import numpy as np
import numpy as nn
from jax import jit
from markchain.distributions import NegBinom, Gamma, Model, Data, Beta
from markchain.classes import HamiltonianMC, StochasticDualAverage

# df = pd.read_csv('./negbin.csv', sep='\t')
# print(df.head())
# y = df['Rex1'].values
# print(y)

palette = sns.color_palette("viridis")
n = 10
size = 800
N = 20000
W = 20000

y = nn.random.negative_binomial(n=n, p=0.3, size=size)

theta = Beta(var_name='theta', a=1, b=1)
k = NegBinom(var_name='k', n=n, p='theta')

model = Model([theta, k], var_list=['theta'],
              data=Data(var_name='k', var_values=y))

sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                        start_x=np.array([0.5]),
                        var_names=model.var_list)

sampler.sample(N, warmup=W)
trace = sampler.points_dict()

z = np.arange(N)

fig, ax = plt.subplots(nrows=2, ncols=1)
ax[0].plot(z, trace['theta'], color=palette[0])
ax[1].hist(trace['theta'], density=True, bins=30, color=palette[0])
fig.savefig('trace.eps')
plt.close(fig)


f_aux = jit(model.f_deterministic)

ww = sampler.points()
wa = ww.transpose()
posterior_predictive = [model.predictive(elem)[0] for elem in wa]
# print(np.shape(posterior_predictive))
#
#
fig, ax = plt.subplots(nrows=2)
ax[0].hist(y, density=True, bins=20, color=palette[0])
ax[1].hist(posterior_predictive, density=True, bins=20, color=palette[0])
fig.tight_layout()
legend = fig.legend()
fig.savefig('posterior_predictive.eps')
plt.close(fig)
