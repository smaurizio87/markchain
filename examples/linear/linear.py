from pprint import pprint
import numpy as nn
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf
from markchain.distributions import Normal, Exp, Model, Data, Linear, SumList
from markchain.classes import HamiltonianMC, StochasticDualAverage
from markchain.utils import plot_trace, trace_summary

size = 200

eps = 0.5
d0 = nn.random.normal(loc=3, size=size)
sc0 = nn.random.normal(loc=0, scale=0.1)
sc = np.exp(sc0)

w = np.linspace(-2, 2, num=size)
w0 = np.ones(size)

c1 = nn.random.normal(loc=d0+eps, scale=sc, size=size)

z1 = c1*w + d0

mu_0 = Normal(var_name='mu_0', loc=0, scale=10)
sc0 = Normal(var_name='sc0', loc=0, scale=2)
sc = Exp(var_name='sc', var=sc0)

mu_1 = Normal(var_name='mu_1', loc=0, scale=10)

x0_1 = Linear(var_name='x0_1', var=mu_1, coeffs=w)

z_0 = Linear(var_name='z_0', var=mu_0, coeffs=w0)

x_1 = SumList(var_name='x_1', var_list=[x0_1, z_0])

y_1 = Normal(var_name='y_1', loc='x_1', scale='sc')

model = Model([mu_0, sc0, sc, mu_1, x0_1, z_0, x_1, y_1],
              var_list=['mu_0', 'sc0', 'mu_1'],
              data=Data(var_name='y_1', var_values=z1))

start_x = np.array([0.0, 0.0, 0.5])

sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                        start_x=start_x,
                        renormalize_mass=True,
                        var_names = model.var_list,
                        integrator='leapfrog')
sampler.delta = 2e-1
sampler.L_mean = 2
N = 10000
sampler.sample(N, warmup=N)

trace = sampler.points_dict()
pprint(trace_summary(trace))
fig = plot_trace(trace)
fig.savefig('trace.eps')
plt.close(fig)

y = sampler.points()
z = np.arange(len(y[0]))
fig, ax = plt.subplots(nrows=3, ncols=3)
ax[0, 0].plot(z, y[0])
ax[1, 0].plot(z, y[1])
ax[2, 0].plot(z, y[2])

ax[0, 1].hist(y[0], density=True, bins=40)
ax[1, 1].hist(y[1], density=True, bins=40)
ax[2, 1].hist(y[2], density=True, bins=40)

ax[0, 0].set_title(r'$\mu_0$', rotation=0)
ax[1, 0].set_title(r'$\log(\sigma_0)$', rotation=0)

ax[2, 0].set_title(r'$\mu_1$', rotation=0)

plot_acf(y[0], ax=ax[0][2])
plot_acf(y[1], ax=ax[1][2])
plot_acf(y[2], ax=ax[2][2])

fig.tight_layout()
fig.savefig('linear.eps')
plt.close(fig)


wa = y.transpose()
f_aux = jit(model.f_deterministic)
r = [f_aux(elem) for elem in wa]

# theta_val = [elem[model.complete_vars['theta']] for elem in y]
y1 = np.transpose(
    np.array(
        [model.predictive(elem)[0] for elem in r]))
print(np.shape(y1))
y1mn = np.array([np.mean(elem) for elem in y1])

y1l = np.array([np.quantile(elem, 0.1) for elem in y1])

y1h = np.array([np.quantile(elem, 0.9) for elem in y1])

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(w, y1mn, color='k')
ax.scatter(w, z1, color='r', marker='x')

ax.fill_between(w, y1l, y1h, color='r', alpha=0.5)
fig.savefig('data.pdf')
plt.close(fig)
