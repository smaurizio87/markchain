import pandas as pd
from jax import numpy as np
import numpy as nn
from markchain.distributions import Normal, Expon, Gev, Data, Model, Exp
from markchain.classes import HamiltonianMC, MonolithicGibbs, StochasticDualAverage
from matplotlib import pyplot as plt
from markchain.utils import plot_trace


d0 = pd.read_csv('./markets/AAPL_data.csv')
d0['Date'] = pd.to_datetime(d0['date'])
d0.set_index('Date')
d0['yyyymm'] = d0['Date'].dt.year.astype('str') + d0['Date'].dt.month.astype(
    'str')
d0['diff1'] = nn.log(d0['close']).diff()

data = -np.array(d0.groupby('yyyymm').min()['diff1'].values)

print(data)

mu = Normal(var_name='mu', loc=0, scale=10)
# logsigma = Normal(var_name='logsigma', scale=10, loc=0)
sigma = Expon(var_name='sigma', scale=10)
xi = Normal(var_name='xi', loc=0, scale=50)

y = Gev(var_name='y', mu='mu', sigma='sigma', xi='xi')

model = Model([mu, sigma, xi, y], var_list=['mu', 'sigma', 'xi'],
              data=Data(var_name='x', var_values=data))

# start_x = np.array([-1.6758549, 1.0568871, 0.37226954])
start_x = np.array([0.2, 1, 1])
masses = np.diag(np.ones(len(start_x)))
print(model.log_posterior(start_x))

sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                                start_x=start_x,
                                find_start=True,
                                renormalize_mass=True,
                                var_names=model.var_list)
# sampler = MonolithicGibbs(log_pdf=model.log_posterior, start_x=start_x,
#                           distrib_type=[distr.distr_type for distr in model.distribs if not distr.is_deterministic],
#                           var_names=model.var_list)

N = 150000
warmup = 150000
sampler.kmopt = 0.25
sampler.cnt = 100
sampler.sample(N, warmup=warmup)
trace = sampler.points_dict()
trc = sampler.points()

fig = plot_trace(trace)
fig.savefig('trace.eps')
plt.close(fig)

# print(trace)
# z = np.arange(len(trace['mu']))
# fig, ax = plt.subplots(nrows=len(trace.keys()), ncols=2)
# for k, key in enumerate(trace):
#     ax[k, 0].plot(z, trace[key])
#     ax[k, 1].hist(trace[key], bins=50, density=True)
#     ax[k, 0].set_title(key)
# fig.tight_layout()
# fig.savefig('trace.eps')
# plt.close(fig)
#
nn.savetxt('trace.csv', trc)
