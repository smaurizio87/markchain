import pandas as pd
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
import numpy as nn
from markchain.distributions import Normal, SumList, Linear, Model, Gamma, Data
from markchain.classes import HamiltonianMC, StochasticDualAverage
from markchain.utils import plot_trace, trace_summary

size=200

N = 80000
W = 80000

X = np.linspace(-5, 5, num=size)
ones = np.ones(size)

x0 = nn.random.normal(loc=1, scale=0.2, size=size)
x1 = nn.random.normal(loc=2, scale=0.1, size=size)
x2 = nn.random.normal(loc=-0.5, scale=0.2, size=size)
x3 = nn.random.normal(loc=0.2, scale=0.04, size=size)

noise = nn.random.normal(loc=0, scale=1, size=size)

Y = x0*ones + x1*X + x2*X**2 + x3*X**3 + noise

fig =plt.figure()
ax = fig.add_subplot()
ax.scatter(X, Y)
fig.savefig('data.eps')
plt.close(fig)

c0 = Normal(var_name='c0', loc=0, scale=5)
c1 = Normal(var_name='c1', loc=0, scale=5)
c2 = Normal(var_name='c2', loc=0, scale=5)
c3 = Normal(var_name='c3', loc=0, scale=5)

d0 = Linear(var_name='d0', var=c0, coeffs=ones)
d1 = Linear(var_name='d1', var=c1, coeffs=X)
d2 = Linear(var_name='d2', var=c2, coeffs=X**2)
d3 = Linear(var_name='d3', var=c3, coeffs=X**3)

x = SumList(var_name='x', var_list=[d0, d1, d2, d3])
sig = Gamma(var_name='sig', a=2, b=2)
y = Normal(var_name='y', loc='x', scale='sig')

model = Model([c0, c1, c2, c3, d0, d1, d2, d3, x, sig, y],
              var_list=['c0', 'c1', 'c2', 'c3', 'sig'],
              data=Data(var_name='y', var_values=Y))

z0 = np.array([0, 0, 0, 0, 1])
masses = np.diag(np.ones(5))
print(model.log_posterior(z0))

sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                                start_x=z0,
                                var_names=model.var_list, renormalize_mass=True,
                                find_start=True, integrator='leapfrog')

sampler.delta = 1e-2
sampler.cnt = 100
sampler.L_mean = 3
sampler.sample(N, warmup=W)
pts = sampler.points_dict()

print(pd.DataFrame(trace_summary(pts)))
print(pts.keys())
s = np.arange(len(pts['c0']))

fig = plot_trace(pts)
fig.savefig('trace.eps')
plt.close(fig)
yp = sampler.points()

wa = yp.transpose()
f_aux = jit(model.f_deterministic)
r = [f_aux(elem) for elem in wa]

y_pred = np.array([model.predictive(elem)[0]
                   for elem in r]).transpose()
print(np.shape(y_pred))

f0 = np.array([np.mean(elem) for elem in y_pred])
fm = np.array([np.quantile(elem, 0.05) for elem in y_pred])
fp = np.array([np.quantile(elem, 0.95) for elem in y_pred])


fig =plt.figure()
ax = fig.add_subplot()
ax.scatter(X, Y)
ax.fill_between(X, fm, fp, alpha=0.5, color='r')
ax.plot(X, f0, color='k')
fig.savefig('pred.pdf')
plt.close(fig)
