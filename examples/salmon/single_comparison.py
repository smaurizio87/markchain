from scipy import stats
import numpy as np

for _ in range(100):
    x = np.random.random(100)
    y = np.random.random(100)
    slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)

    if p_value < 0.05:
        print(intercept, p_value, std_err)
