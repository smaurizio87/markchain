import numpy as nn
import seaborn as sns
from timeit import default_timer as timer
from jax import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from tqdm import tqdm
from markchain.distributions import Gamma, Poisson, Model, Data, Normal, Exp, Expon, Pow
from markchain.classes import PositiveSamplier, RealSamplier, MonolithicGibbs

palette = sns.color_palette("viridis")

# TODO: MAKE ME WORK!!!!!


data = nn.random.poisson(lam=5, size=300)

mu0 = Normal('mu0', loc=0, scale=5)
mu = Pow(var_name='mu', var=mu0, n=2)
x = Poisson(var_name='x', k='mu')

model = Model([mu0, mu, x], var_list=['mu0'], extra_vars=['mu'],
              data=Data(var_name='x', var_values=data))



sampler = MonolithicGibbs(model.log_posterior, np.array([0.2]),
                          distrib_type=model.distrib_type)
sampler.samplers[0] = PositiveSamplier()
sampler.sample(2000, warmup=2000)
w = sampler.points()
print(w)
