from matplotlib import pyplot as plt
import pandas as pd
from jax import numpy as np
from jax import jit
import numpy as nn
from scipy.stats import multinomial, dirichlet
from markchain.distributions import Normal, Logistic, Multinomial, Linear,\
    SumList, Data, Model, Dirichlet
from markchain.classes import HamiltonianMC, StochasticDualAverage
from markchain.utils import trace_summary, plot_trace

N = 50000
W = 50000

size = 400
n = 3
alpha0 = np.array([1/3, 1/3, 1/3])
alpha_true = np.array([0.15, 0.1, 0.75])

theta = Dirichlet(var_name='theta',
                  alpha=alpha0, shape=3)

x = Multinomial(var_name='x', n=n, p='theta', shape=3)

data = nn.random.multinomial(n=n, pvals = alpha_true, size=size)
print('data', data)

model = Model([theta, x], var_list=['theta'], data=Data(var_name='x',
                                                        var_values=data))

z0 = np.array([0.3, 0.3, 0.4])
print(model.log_posterior(z0))

def ftest(x):
    prior = dirichlet(alpha=alpha0).logpdf(x)
    lkl = multinomial.logpmf(data, n, p=x)
    return np.sum(lkl) + prior

def faux(z):
    w = np.append(z, 1-np.sum(z))
    return model.log_posterior(w)

print('ftest', ftest(z0))


sampler = StochasticDualAverage(log_pdf=faux, start_x=np.array([1/3, 1/3]),
                        find_start=True,
                        var_names=model.var_list)
#
sampler.sample(N, warmup=W)
#
# trace = sampler.points_dict()
# print(pd.DataFrame(trace_summary(trace)))
#
# fig = plot_trace(trace)
# fig.savefig('trace.eps')
# plt.close(fig)

trc = sampler.points()
w1 = trc[0]
w2 = trc[1]
print(w1)
trace = {'x0': w1, 'x1': w2}

print(pd.DataFrame(trace_summary(trace)))

fig = plot_trace(trace)
fig.savefig('trace_1.eps')
plt.close(fig)
