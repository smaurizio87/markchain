from matplotlib import pyplot as plt
import pandas as pd
from jax import numpy as np
from jax import jit
import numpy as nn
from scipy.stats import multinomial, dirichlet
from markchain.distributions import Normal, Logistic, Multinomial, Linear,\
    SumList, Data, Model, Dirichlet
from markchain.classes import HamiltonianMC, StochasticDualAverage
from markchain.utils import trace_summary, plot_trace

N = 10000
W = 10000

size = 300
n = 2
alpha0 = np.array([0.5, 0.5])

theta = Dirichlet(var_name='theta',
                  alpha=alpha0, shape=2)

x = Multinomial(var_name='x', n=n, p='theta', shape=2)

data = nn.random.multinomial(n=n, pvals = np.array([0.2, 0.8]), size=size)
print('data', data)

model = Model([theta, x], var_list=['theta'], data=Data(var_name='x',
                                                        var_values=data))

z0 = np.array([0.3, 0.7])
print(model.log_posterior(z0))

def ftest(x):
    prior = dirichlet(alpha=alpha0).logpdf(x)
    lkl = multinomial.logpmf(data, n, p=x)
    return np.sum(lkl) + prior

def faux(z):
    w = np.append(z, 1-np.sum(z))
    return model.log_posterior(w)

print('ftest', ftest(z0))


sampler = StochasticDualAverage(log_pdf=faux, start_x=np.array([0.3]),
                        find_start=True,
                        var_names=model.var_list)
#
sampler.sample(N, warmup=W)
#
trace = sampler.points_dict()
print(pd.DataFrame(trace_summary(trace)))

fig = plot_trace(trace)
fig.savefig('trace.eps')
plt.close(fig)
