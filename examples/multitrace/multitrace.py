import numpy as nn
from matplotlib import pyplot as plt
import seaborn as sns
from jax import numpy as np
import random
from multiprocessing import Process, Queue
from markchain.distributions import Model

from markchain.distributions import Model, Data, Normal, Exp
from markchain.classes import Gibbs, HamiltonianMC, MultiTrace
from markchain.utils import plot_trace, psrf



palette = sns.color_palette("viridis")

data = nn.random.normal(loc=10, scale=3, size=20)

theta = Normal(var_name='theta', loc=10, scale=5)
log_phi = Normal(var_name='log_phi', loc=0, scale=3)
phi = Exp(var_name='phi', var=log_phi)
x = Normal(var_name='x', loc='theta', scale='phi')

model = Model([theta, log_phi, phi, x], var_list=['theta', 'log_phi'],
            extra_vars=['phi'],
            data=Data(var_name='x', var_values=data))




sampler = HamiltonianMC(log_pdf=model.log_posterior, start_x=np.array([10, 1]),
                        masses=np.diag(np.array([0.2, 0.5])),J=model.d_log_posterior,
                        var_names=model.var_list,
                        find_start=False,
                        integrator='third_order')

sampler.delta = 5e-2
sampler.L = 2

smpl_list = [sampler for _ in range(4)]

mt = MultiTrace(sampler, 4)
rs = mt.sample(4, 5000, warmup=5000)

z = np.arange(len(rs[0]['theta']))

fig = plot_trace(rs, palette=palette, alpha=0.5)
fig.savefig('prova.pdf')
plt.close(fig)
