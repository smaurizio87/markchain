import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
from jax import numpy as np
from jax import jit
from markchain.distributions import Normal, WeibullHazard, Model, Data, Linear, SumList, Exp
from markchain.classes import HamiltonianMC, StochasticDualAverage
from markchain.utils import trace_summary, plot_trace


palette = sns.color_palette("viridis")

df = pd.read_csv('./veterans.csv')

# dff = df[df["status"]==1]

dff = df
dn = dff['time']

x0 = dff['age'].values
x1 = dff['trt'].values-1
x2 = dff['diagtime'].values
x3 = dff['prior'].values
c0 = np.ones(len(x0))

print(dn)
xp = np.array([k for k, elem in enumerate(x1) if elem==0])
xm = np.array([k for k, elem in enumerate(x1) if elem==1])
print(xp)
print(xm)
# df = pd.read_csv('./mastectomy.csv')
# dff = df[df['event']==1]

fig = plt.figure()
ax = fig.add_subplot(111)
ax.hist(dff['time'], bins=50)
fig.savefig('time.eps')
plt.close(fig)

# b0 = Gamma(var_name='b0', a=2, b=2)
b0 = Normal(var_name='b0', loc=0, scale=10)
b1 = Normal(var_name='b1', loc=0, scale=10)
b2 = Normal(var_name='b2', loc=0, scale=10)
w0 = Linear(var_name='w0', var=b0, coeffs=x0)
w1 = Linear(var_name='w1', var=b1, coeffs=x2)
w2 = Linear(var_name='w2', var=b2, coeffs=x3)
x = SumList(var_name='x', var_list=[w0, w1, w2])
y = Exp(var_name='y', var=x)
k = Normal(var_name='k', loc=1, scale=0.15)
p = WeibullHazard(var_name='p', delta=df['status'].values,
           scale='y', k='k')

data = [Data(var_name='p', var_values=dn)]

model = Model([b0, w0, b1, w1, w2, b2, x, y, k, p],
              var_list=['b0',  'b1', 'b2', 'k'], data=data)
z0 = np.array([0.1, 0.1, 0.1, 1])
masses = np.diag(np.array([1.0, 1.0, 1.0, 1.0]))

print(model.log_posterior(z0))

sampler = StochasticDualAverage(log_pdf=model.log_posterior, renormalize_mass=True,
                        decorrelate=True,
                        find_start=True,
                        start_x=z0, var_names=model.var_list)

sampler.delta = 0.2
sampler.sample(90000, warmup=90000)

pts = sampler.points_dict()

fig = plot_trace(pts)
fig.savefig('trace.eps')
plt.close(fig)

recap = pd.DataFrame.from_dict(trace_summary(pts))
print(recap)
# z = np.arange(len(pts['b0']))
#
# fig, ax = plt.subplots(nrows=4, ncols=2)
# ax[0, 0].plot(z, pts['b0'])
# ax[0, 1].hist(pts['b0'], density=True, bins=50)
# ax[0, 0].set_ylabel('b0')
#
# ax[1, 0].plot(z, pts['b1'])
# ax[1, 1].hist(pts['b1'], density=True, bins=50)
# ax[1, 0].set_ylabel('b1')
#
# ax[2, 0].plot(z, pts['b2'])
# ax[2, 1].hist(pts['b2'], density=True, bins=50)
# ax[2, 0].set_ylabel('b2')
#
#
# # ax[4, 0].plot(z, pts['y'])
# # ax[4, 1].hist(pts['y'], density=True, bins=50)
# # ax[4, 0].set_ylabel('y')
#
# ax[3, 0].plot(z, pts['k'])
# ax[3, 1].hist(pts['k'], density=True, bins=50)
# ax[3, 0].set_ylabel('k')
# fig.tight_layout()
# fig.savefig('trace_veterans.eps')
# plt.close(fig)

ww = sampler.points()
wa = ww.transpose()
f_aux = jit(model.f_deterministic)
y = [f_aux(elem) for elem in wa]
print(np.shape(y))
posterior_predictive = [model.predictive(elem)[0][0] for elem in y]
x_pred = [np.median(elem) for elem in posterior_predictive]
x_pred_m = [np.quantile(elem, 0.05) for elem in posterior_predictive]
x_pred_p = [np.quantile(elem, 0.95) for elem in posterior_predictive]

bins = np.linspace(0, 1000, 100)
fig, ax = plt.subplots(nrows=1)
ax.hist(dn, color='grey', label='data', bins=bins, density=True)
ax.hist(x_pred, label='x', color=palette[0], bins=bins, density=True, alpha=0.5)
ax.set_xlim([0, 1000])
legend = fig.legend()
fig.tight_layout()
fig.savefig('posterior_predictive.pdf')
plt.close(fig)

# pp = [np.mean(np.array([v for k,v in enumerate(model.predictive(elem)[0][0])
#               if k in xp]))  for elem in y]

# pm = [np.mean(np.array([v for k,v in enumerate(model.predictive(elem)[0][0])
#                if k in xm]))  for elem in y]
pp = np.take(np.array(posterior_predictive), xp)
pm = np.take(np.array(posterior_predictive), xm)

ppm = np.array([np.median(elem) for elem in pp])
pmm = np.array([np.median(elem) for elem in pm])


bins = np.linspace(0, 500, 50)
fig, ax = plt.subplots(nrows=1)
ax.hist(ppm, color='r', label='xp', bins=bins, density=True, alpha=0.5)
ax.hist(pmm, color='b', label='xm', bins=bins, density=True, alpha=0.5)
ax.set_xlim([0, 500])
legend = fig.legend()
fig.tight_layout()
fig.savefig('posterior_predictive_comp.pdf')
plt.close(fig)

# rw = [Normal(var_name=f'rw_{k}', loc=0, scale=1) for k in range(N)]
#
# beta = [SumList(var_name=f"z_{k-1}", var_list=rw[:k]) for k in range(1, N)]
# l0 = Gamma(var_name='g', a=1, b=1)
# s = [Linear(var_name=f"s_{k}", var_list=[beta[k], X]) for k in range(N-1)]
# z = [Exp(var_name=f"z_{k}", var=s) for k in range(N-1)]
# w = [Prod(var_name=f"w_{k}", var_list=[z[k], l0]) for k in range(N-1)]
