from matplotlib import pyplot as plt
import pandas as pd
from jax import numpy as np
import numpy as nn
from markchain.distributions import MVDiagonalNormal, Data, Model, Normal,\
    Exp, Expon, Distribution, Uniform, Pow, SVM, ARMA, Deterministic1D
from markchain.classes import SeriesMC, HamiltonianMC, StochasticDualAverage
from markchain.utils import plot_trace, trace_summary

N = 20000
W = 20000

sz = 90

df = pd.read_csv('./markets/^GSPC.csv')
z = np.array(nn.log(df['Close']).diff())


Y = nn.random.multivariate_normal(mean=nn.array([0.0]*sz),
                                     cov=nn.diag(nn.array([1.]*sz)),
                                     size=1)

data = Data(var_name='y',
            var_values=z[-sz:]*100)


class SVM(Deterministic1D):
    def __init__(self, var, var_name):
        super().__init__(var, var_name)
        def f(x):
            y = np.array(x)
            return np.exp(-y/2)
        self.f = f
        self.f_list = self.f
        self.df = np.exp

h0 = Normal(var_name='h0', loc=0., scale=1.)
log_sig = Normal(var_name='log_sig', scale=1.)
sig = Exp(var_name='sig', var=log_sig)
phi = Uniform(var_name='phi', low=-1., high=1.)
mu = Normal(var_name='mu', loc=0., scale=1.)
h = ARMA(var_name='h', phi='phi', mu='mu', scale='sig', shape=sz)
h1 = SVM(var_name='h1', var=h)

y = Normal(var_name='y', loc=0, scale='h1')

model = Model([h0, log_sig, sig, phi, mu, h],
              var_list=['log_sig', 'phi', 'mu', 'h'],
              data=Data(var_name='y', var_values=Y))
z0 = np.array([0., 0., 0.]+[0.0]*sz)
masses = np.array([1, 0.05, 0.1]+[1.0]*sz)
print(model.log_posterior(z0))

sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                   var_names=model.var_list,
                   decorrelate=False,
                        start_x=z0)

sampler.delta = 5e-2
sampler.L_mean = 1
sampler.L = 1
sampler.n_step_mean = 1
sampler.sample(N, warmup=W)

trace = sampler.points_dict()
d1 = {
      'log_sig': trace['log_sig'],
      'phi': trace['phi'],
      'mu': trace['mu']}

fig = plot_trace(d1)
fig.savefig('trace.eps')
plt.close(fig)
print(pd.DataFrame(trace_summary(d1)))
