import numpy as nn
import os
import pandas as pd
from jax import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from markchain.distributions import Normal, Model, Data, Exp, ARMA, Uniform
from markchain.classes import HamiltonianMC, SeriesMC, StochasticDualAverage
from markchain.utils import trace_summary, plot_trace

N = 200

N_sample = 50000
W = 50000

path_out = f'figs_new_{N}'

if not os.path.isdir(path_out):
    os.mkdir(path_out)

df = pd.read_csv('./markets/^GSPC.csv')
z = np.array(nn.log(df['Close']).diff())

print(z)

palette = sns.color_palette("viridis")

mm = 2.3
ph = 0.3
sg = 0.6

h0 = [nn.random.default_rng().normal(loc=mm, scale=np.exp(sg)/np.sqrt(1-ph**2))]
for k in range(1, N):
    h0 += [mm + ph*(h0[k-1] - mm)]

y = np.array(h0) + nn.random.normal(scale=np.exp(sg), size=N)

# y = z[-N:] #[nn.random.normal(loc=0, scale=1) for _ in range(N+1)]
# print(np.std(y))
# print(np.size(y))
# y = (y - np.mean(y))*100 # / np.std(y)
# y /= np.mean(y)

mu = Normal(var_name='mu', loc=0, scale=5)
phi = Uniform(var_name='phi', low=-1, high=1)
log_sigma = Normal(var_name='log_sigma', loc=0, scale=2)
sigma = Exp(var_name='sigma', var=log_sigma)
h = ARMA(var_name='h', mu='mu', phi='phi', sigma='sigma')

model = Model([mu, phi, log_sigma, sigma, h],
              var_list=['mu', 'phi','log_sigma'],
              data=Data(var_name='h', var_values=y))

print(model.log_posterior(np.array([0., 0., 0.])))

sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                        start_x=np.array([0., 0., 0.]),
                        decorrelate=True,
                        integrator='third_order',
                        var_names=model.var_list)

sampler.delta = 1e-2
sampler.L_mean = 3
sampler.cnt = 1000

sampler.sample(N_sample, warmup=W)


trace = sampler.points_dict()

print(pd.DataFrame(trace_summary(trace)))

fig = plot_trace(trace)
fig.savefig('trace.eps')
plt.close(fig)

# print(y)

# h_std = Normal(var_name='h_std', loc=np.array([0.]*N), scale=np.array([1.]*N))
# h_aux = Exp(var_name='h_aux', var=h_std)
# x = Normal(var_name='x', loc=np.array([0.]*N), scale='h_aux')
#
# data = Data(var_name='x', var_values=y)
#
# model = Model([h_std, h_aux, x], var_list=['h_std'],
#               data=data)
# start_x = np.array([0.]*N)
# print(model.log_posterior(start_x))
#
# sampler = HamiltonianMC(log_pdf=model.log_posterior,
#                         start_x=start_x)
#
# sampler.sample(N_sample, warmup=W)
#
