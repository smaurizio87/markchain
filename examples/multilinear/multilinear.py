"""
https://www.kaggle.com/code/billbasener/bayesian-linear-regression-in-pymc3
"""
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from markchain.distributions import HalfNormal, Normal, Model, Data, Linear, SumList, Pow
from markchain.classes import HamiltonianMC, StochasticDualAverage
from markchain.utils import trace_summary, plot_trace

alpha, sigma = 1, 1
beta = [1, 2.5]

# Size of dataset
size = 1000

# Predictor variable
X1 = np.random.randn(size)
X2 = np.random.randn(size) * 0.2
X0 = np.ones(shape=size)

# Simulate outcome variable
Y = Data(var_name='Y_obs', var_values=alpha + beta[0]*X1
         + beta[1]*X2 + np.random.randn(size)*sigma)

alpha = Normal('alpha', loc=0, scale=10)
beta_0 = Normal('beta_0', loc=0, scale=10)
beta_1 = Normal('beta_1', loc=0, scale=1)
sigma_0 = Normal('sigma', loc=0, scale=2)
sigma = Pow(var_name='sigma', var=sigma_0, n=2)

# Expected value of outcome
b0 = Linear(var=beta_0, var_name='b0', coeffs=X1)
b1 = Linear(var=beta_1, var_name='b1', coeffs=X2)
a = Linear(var=alpha, var_name='a', coeffs=X0)
mu = SumList(var_name='mu', var_list=[a, b0, b1])

# Likelihood (sampling distribution) of observations
Y_obs = Normal('Y_obs', mu='mu', sigma='sigma')

model = Model([alpha, beta_0, beta_1, sigma_0, sigma, b0, b1, a, mu, Y_obs],
              var_list=['alpha', 'beta_0', 'beta_1', 'sigma'],
              data=Y)
sampler = StochasticDualAverage(log_pdf=model.log_posterior,
                        start_x=np.array([1, 1, 2, 1]),
                        renormalize_mass=True,
                        find_start=True,
                        integrator='leapfrog',
                        alpha=1,
                        decorrelate=True,
                        var_names=model.var_list)
sampler.delta=0.02
sampler.L = 3

sampler.sample(20000, warmup=20000)
trace = sampler.points_dict()

print(pd.DataFrame(trace_summary(trace)))
fig = plot_trace(trace)
fig.savefig('trace.eps')
plt.close(fig)
