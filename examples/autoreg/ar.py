import numpy as nn
import pandas as pd
from jax import numpy as np
from matplotlib import pyplot as plt

from markchain.distributions import Normal, Gamma, Model, Data, SumList, Pow
from markchain.classes import HamiltonianMC, SeriesMC
from markchain.utils import trace_summary


size = 20
n = 9

dt = [nn.random.normal(loc=1, scale=1, size=size)]

for i in range(n):
    x0 = nn.random.normal(loc=0, scale=.05)
    rs = dt[-1] + nn.random.normal(loc=x0, scale=.01, size=size)
    dt += [rs]

print(len(dt))

ls = [Normal(var_name=f"p_{i}", loc=0, scale=.3) for i in range(n)]

z = [SumList(var_name=f'z_{i}', var_list=ls[:i]) for i in range(1, n)]
log_sig = Normal(var_name='log_sig', loc=1, scale=3)
sig = Pow(var_name='sig', var=log_sig, n=2)
w = [Normal(var_name=f"w_{i}", loc=f"z_{i}", scale='sig') for i in range(1, n)]

data = [Data(var_name=f'w_{i}', var_values=dt[i]) for i in range(1, n)]

distrs = ls + z + [log_sig, sig] + w

model = Model(distribs=distrs, var_list=[f"p_{i}"
                                         for i in range(n)]+['log_sig'],
              data=data
              )

print(len(model.var_list))
# z0 = np.array([0]*n + [1])
z0 = np.array([0]*n + [.1])

sampler = HamiltonianMC(log_pdf=model.log_posterior, start_x=z0,
                        var_names=model.var_list,
                        renormalize_mass=True,
                        alpha=0.85
                        )

warmup = 50000
smpl = 50000

sampler.delta = 1e-4
sampler.L = 5
sampler.n_step = 3
sampler.sample(smpl, warmup=warmup)
pts = sampler.points_dict()

recap = pd.DataFrame.from_dict(trace_summary(pts))

print(recap)

print(pts.keys())
s = np.arange(len(pts['log_sig']))
fig, ax = plt.subplots(ncols=2, nrows=5)
for k, elem in enumerate(pts):
    i = k % 5
    j = k // 5
    print(i, j)
    ax[i, j].plot(s, pts[elem])
    # ax[k, 1].hist(pts[elem], bins=50)
fig.tight_layout()
fig.savefig('trace.eps')
plt.close(fig)

fig, ax = plt.subplots(ncols=2, nrows=5)
for k, elem in enumerate(pts):
    i = k % 5
    j = k // 5
    ax[i, j].hist(pts[elem], bins=50)
    # ax[k, 1].hist(pts[elem], bins=50)
fig.tight_layout()
fig.savefig('hist.eps')
plt.close(fig)
