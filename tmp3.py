from jax import numpy as jnp
import numpy as np
from numpy import random
from scipy.stats import norm
from markchain.distributions import Normal, Data, Exp, Model, Linear, SumList, Pow

tol = 1e-2
eps = 1e-5

loc_mu = 0
loc_sig = 0
scale_mu = 4
scale_sig = 0.5
w = jnp.array(random.normal(loc=2, scale=1, size=20))

z = jnp.linspace(-2, 2, num=20)
z = jnp.linspace(-2, 2, num=20)

data = z*w

def ff(x):
    y = (np.sum(norm.logpdf(data, loc=x[1], scale=np.exp(x[2])))
            + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
            + np.sum(norm.logpdf(x[1], loc=z*x[0], scale=scale_mu))
            + norm.logpdf(x[2], loc=loc_sig, scale=scale_sig)
            )
    print('X', x[0])
    print('F', z*x[0])
    print('G', norm.logpdf(x[1], loc=z*x[0], scale=scale_mu))
    print('D', norm.logpdf(data, loc=x[1], scale=np.exp(x[2])))
    return y


mu = Normal(var_name='mu', loc=loc_mu, scale=scale_mu)

x = Linear(var_name='x', var=mu, coeffs=z)

mu1 = Normal(var_name='mu1', loc='x', scale=scale_mu)

log_sig = Normal(var_name='log_sig', loc=loc_sig, scale=scale_sig)

sig = Exp(var_name='sig', var=log_sig)

tt = Normal(var_name='y', loc='mu1', scale='sig')

model1 = Model([mu, mu1, log_sig, sig, x, tt],
                var_list=['mu', 'mu1', 'log_sig'],
                extra_vars=['sig', 'x'],
                data=Data(var_name='y',
                            var_values=data))
x = np.array([4., 2., 3.])
r = model1.log_posterior(x)

dff = r - ff(x)
print(ff(x))
print(r)
