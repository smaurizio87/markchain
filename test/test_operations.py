from jax import numpy as jnp
import numpy as np
from numpy import random
from scipy.stats import norm
from markchain.distributions import Normal, Data, Exp, Model, Linear, SumList, Pow


def test_linear():

    tol = 1e-2
    eps = 1e-5

    loc_mu = 0
    loc_sig = 0
    scale_mu = 4
    scale_sig = 0.5
    w = jnp.array(random.normal(loc=2, scale=1, size=20))

    z = jnp.linspace(-2, 2, num=20)
    z = jnp.linspace(-2, 2, num=20)

    data = z*w

    def ff(x):
        y = (np.sum(norm.logpdf(data, loc=z*x[0], scale=np.exp(x[1])))
             + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
             + norm.logpdf(x[1], loc=loc_sig, scale=scale_sig)
             )
        return y

    tt = Normal(var_name='y', loc='x', scale='sig')


    mu = Normal(var_name='mu', loc=loc_mu, scale=scale_mu)

    x = Linear(var_name='x', var=mu, coeffs=z)

    log_sig = Normal(var_name='log_sig', loc=loc_sig, scale=scale_sig)

    sig = Exp(var_name='sig', var=log_sig)

    model1 = Model([mu, log_sig, sig, x, tt],
                   var_list=['mu', 'log_sig'],
                   extra_vars=['sig', 'x'],
                   data=Data(var_name='y',
                             var_values=data))

    for _ in range(10):
        x = np.random.normal(loc=np.array([2, 3]))

        dff = model1.log_posterior(x) - ff(x)
        assert np.max(np.abs(dff)) < tol


def test_linear_composition():
    tol = 1e-2
    eps = 1e-5

    loc_mu = 0
    loc_sig = 0
    scale_mu = 4
    scale_sig = 0.5
    w = jnp.array(random.normal(loc=2, scale=1, size=20))

    z = jnp.linspace(-2, 2, num=20)
    z = jnp.linspace(-2, 2, num=20)

    data = z*w

    def ff(x):
        y = (np.sum(norm.logpdf(data, loc=z*np.exp(x[0]), scale=np.exp(x[1])))
             + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
             + norm.logpdf(x[1], loc=loc_sig, scale=scale_sig)
             )
        return y

    tt = Normal(var_name='y', loc='x', scale='sig')

    mu0 = Normal(var_name='mu0', loc=loc_mu, scale=scale_mu)

    mu = Exp(var_name='mu', var=mu0)

    x = Linear(var_name='x', var=mu, coeffs=z)

    log_sig = Normal(var_name='log_sig', loc=loc_sig, scale=scale_sig)

    sig = Exp(var_name='sig', var=log_sig)

    model1 = Model([mu0, mu, log_sig, sig, x, tt],
                   var_list=['mu0', 'log_sig'],
                   extra_vars=['sig', 'mu', 'x'],
                   data=Data(var_name='y',
                             var_values=data))
    x = np.array([1., 2.])
    x1 = np.array([1+eps, 2])
    x2 = np.array([1, 2+eps])

    d1 = (ff(x1)-ff(x))/eps
    d2 = (ff(x2)-ff(x))/eps

    dff = model1.log_posterior(x) - ff(x)
    assert np.max(np.abs(dff)) < tol

    dff1 = model1.d_log_posterior(x) - np.array([d1, d2])
    assert np.max(np.abs(dff1)) < tol


def test_linear_sum():

    tol = 1e-2
    eps = 1e-5

    loc_mu = 0
    scale_mu = 4

    loc_mu1 = 0
    scale_mu1 = 4

    loc_sig = 0
    scale_sig = 0.5
    w = jnp.array(random.normal(loc=2, scale=1, size=20))
    w1 = jnp.array(random.normal(loc=-4, scale=1, size=20))

    z = jnp.linspace(-2, 2, num=20)
    z1 = jnp.ones(20)

    data = z*w + z1*w1

    def ff(x):
        y = (np.sum(norm.logpdf(data, loc=x[0]+x[1], scale=np.exp(x[2])))
             + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
             + norm.logpdf(x[1], loc=loc_mu1, scale=scale_mu1)
             + norm.logpdf(x[2], loc=loc_sig, scale=scale_sig)
             )
        return y

    mu = Normal(var_name='mu', loc=loc_mu, scale=scale_mu)
    c = Linear(var_name='c', var=mu, coeffs=z)

    mu1 = Normal(var_name='mu1', loc=loc_mu1, scale=scale_mu1)
    c1 = Linear(var_name='c1', var=mu1, coeffs=z1)

    x = SumList(var_name='x', var_list=[mu, mu1])

    log_sig = Normal(var_name='log_sig', loc=loc_sig, scale=scale_sig)
    sig = Exp(var_name='sig', var=log_sig)

    tt = Normal(var_name='y', loc='x', scale='sig')

    model1 = Model([mu, mu1, c, c1, x, log_sig, sig, tt],
                   var_list=['mu', 'mu1', 'log_sig'],
                   extra_vars=['sig', 'c', 'c1', 'x'],
                   data=Data(var_name='y',
                             var_values=data))

    x = np.array([1., 2., 3.])
    x1 = np.array([1+eps, 2, 3])
    x2 = np.array([1, 2+eps, 3])
    x3 = np.array([1, 2, 3+eps])

    d1 = (ff(x1)-ff(x))/eps
    d2 = (ff(x2)-ff(x))/eps
    d3 = (ff(x3)-ff(x))/eps

    dff = model1.log_posterior(x) - ff(x)
    assert np.max(np.abs(dff)) < tol

    # dff1 = model1.d_log_posterior(x) - np.array([d1, d2, d3])
    # assert np.max(np.abs(dff1)) < tol

def test_linear_sum_sick():
    tol = 1e-2
    eps = 1e-5

    loc_mu = 0
    scale_mu = 4

    loc_mu1 = 0
    scale_mu1 = 4

    loc_sig = 0
    scale_sig = 0.5
    w = jnp.array(random.normal(loc=2, scale=1, size=2))
    w1 = jnp.array(random.normal(loc=-4, scale=1, size=2))

    z = jnp.linspace(-2, 2, num=2)
    z1 = jnp.ones(2)

    data = z*w + z1*w1

    def ff(x):
        y = (np.sum(norm.logpdf(data, loc=(z*x[0]**2+z1*x[1])**3, scale=np.exp(x[2])**2))
                + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
                + norm.logpdf(x[1], loc=loc_mu1, scale=scale_mu1)
                + norm.logpdf(x[2], loc=loc_sig, scale=scale_sig)
                )
        return y

    mu0 = Normal(var_name='mu0', loc=loc_mu, scale=scale_mu)
    mu = Pow(var_name='mu', var=mu0, n=2)
    c = Linear(var_name='c', var=mu, coeffs=z)

    mu1 = Normal(var_name='mu1', loc=loc_mu1, scale=scale_mu1)
    c1 = Linear(var_name='c1', var=mu1, coeffs=z1)

    x0 = SumList(var_name='x0', var_list=[c, c1])

    log_sig = Normal(var_name='log_sig', loc=loc_sig, scale=scale_sig)
    sig0 = Exp(var_name='sig0', var=log_sig)
    sig = Pow(var_name='sig', var=sig0, n=2)

    x = Pow(var_name='x', var=x0, n=3)

    tt = Normal(var_name='y', loc='x', scale='sig')

    model1 = Model([mu0, mu, mu1, c, c1, x0, x, log_sig, sig0, sig, tt],
                    var_list=['mu0', 'mu1', 'log_sig'],
                    extra_vars=['mu', 'sig0',  'sig', 'c', 'c1', 'x0', 'x'],
                    data=Data(var_name='y',
                                var_values=data))

    x = np.array([1., 4., 9.])
    x1 = np.array([1+eps, 2, 3])
    x2 = np.array([1, 2+eps, 3])
    x3 = np.array([1, 2, 3+eps])

    d1 = (ff(x1)-ff(x))/eps
    d2 = (ff(x2)-ff(x))/eps
    d3 = (ff(x3)-ff(x))/eps

    dff = model1.log_posterior(x) - ff(x)
    assert np.max(np.abs(dff))<tol


def test_linear_1():

    tol = 1e-2
    eps = 1e-5

    loc_mu = 0
    loc_sig = 0
    scale_mu = 4
    scale_sig = 0.5
    w = jnp.array(random.normal(loc=2, scale=1, size=20))

    z = jnp.linspace(-2, 2, num=20)
    z = jnp.linspace(-2, 2, num=20)

    data = z*w

    def ff(x):
        y = (np.sum(norm.logpdf(data, loc=np.exp(z*x[0]), scale=np.exp(x[1])))
             + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
             + norm.logpdf(x[1], loc=loc_sig, scale=scale_sig)
             )
        return y

    tt = Normal(var_name='y', loc='xa', scale='sig')


    mu = Normal(var_name='mu', loc=loc_mu, scale=scale_mu)

    x = Linear(var_name='x', var=mu, coeffs=z)

    xa = Exp(var_name='xa', var=x)

    log_sig = Normal(var_name='log_sig', loc=loc_sig, scale=scale_sig)

    sig = Exp(var_name='sig', var=log_sig)

    model1 = Model([mu, log_sig, sig, x, xa, tt],
                   var_list=['mu', 'log_sig'],
                   extra_vars=['sig', 'x', 'xa'],
                   data=Data(var_name='y',
                             var_values=data))
    x = np.array([1., 2.])
    x1 = np.array([1+eps, 2])
    x2 = np.array([1, 2+eps])

    d1 = (ff(x1)-ff(x))/eps
    d2 = (ff(x2)-ff(x))/eps

    dff = model1.log_posterior(x) - ff(x)
    assert np.max(np.abs(dff)) < tol



def test_linear_sum_sick_1():
    tol = 1e-3
    eps = 1e-5

    loc_mu = 0
    scale_mu = 4

    loc_mu1 = 0
    scale_mu1 = 4

    loc_sig = 0
    scale_sig = 0.5
    w = jnp.array(random.normal(loc=2, scale=1, size=2))
    w1 = jnp.array(random.normal(loc=-4, scale=1, size=2))

    z = jnp.linspace(-2, 2, num=2)
    z1 = jnp.ones(2)

    data = z*w + z1*w1
    data1 = z*w - z1*w1

    def ff(x):
        y = (np.sum(norm.logpdf(data, loc=(z*x[0]**2+z1*x[1])**2, scale=np.exp(x[2])**2))
                +np.sum(norm.logpdf(data1, loc=(z*x[0]**2-z1*x[1])**3, scale=np.exp(x[2])**2))
                + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
                + norm.logpdf(x[1], loc=loc_mu1, scale=scale_mu1)
                + norm.logpdf(x[2], loc=loc_sig, scale=scale_sig)
                )
        return y

    mu0 = Normal(var_name='mu0', loc=loc_mu, scale=scale_mu)
    mu = Pow(var_name='mu', var=mu0, n=2)
    c = Linear(var_name='c', var=mu, coeffs=z)

    mu1 = Normal(var_name='mu1', loc=loc_mu1, scale=scale_mu1)
    c1 = Linear(var_name='c1', var=mu1, coeffs=z1)
    c2 = Linear(var_name='c1', var=mu1, coeffs=-z1)

    x0 = SumList(var_name='x0', var_list=[c, c1])
    x0a = SumList(var_name='x0', var_list=[c, c2])

    log_sig = Normal(var_name='log_sig', loc=loc_sig, scale=scale_sig)
    sig0 = Exp(var_name='sig0', var=log_sig)
    sig = Pow(var_name='sig', var=sig0, n=2)

    x = Pow(var_name='x', var=x0, n=2)
    xa = Pow(var_name='xa', var=x0a, n=3)

    tt = Normal(var_name='y', loc='x', scale='sig')
    tt1 = Normal(var_name='y1', loc='xa', scale='sig')

    model1 = Model([mu0, mu, mu1, c, c1,c2, x0, x0a, x, xa, log_sig, sig0, sig, tt, tt1],
                    var_list=['mu0', 'mu1', 'log_sig'],
                    extra_vars=['mu', 'sig0',  'sig', 'c', 'c1', 'x0','x0a', 'x', 'xa'],
                    data=[Data(var_name='y',
                                var_values=data), Data(var_name='y1', var_values=data1)])

    for _ in range(200):
        z0 = np.random.normal(loc=np.array([5, 8, 12]), scale=4)

        dff = model1.log_posterior(z0) - ff(z0)
        assert np.max(np.abs(dff/ff(z0)))<tol


def test_h1():

    tol = 1e-2
    eps = 1e-5

    loc_mu = 0
    loc_sig = 0
    scale_mu = 4
    scale_sig = 0.5
    w = jnp.array(random.normal(loc=2, scale=1, size=20))

    z = jnp.linspace(-2, 2, num=20)
    z = jnp.linspace(-2, 2, num=20)

    data = z*w

    def ff(x):
        y = (np.sum(norm.logpdf(data, loc=z*x[1], scale=np.exp(x[2])))
             + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
             + norm.logpdf(x[1], loc=x[0], scale=scale_mu)
             + norm.logpdf(x[2], loc=loc_sig, scale=scale_sig)
             )
        return y


    mu = Normal(var_name='mu', loc=loc_mu, scale=scale_mu)
    mu1 = Normal(var_name='mu1', loc='mu', scale=scale_mu)

    x = Linear(var_name='x', var=mu1, coeffs=z)

    log_sig = Normal(var_name='log_sig', loc=loc_sig, scale=scale_sig)

    sig = Exp(var_name='sig', var=log_sig)

    tt = Normal(var_name='y', loc='x', scale='sig')

    model1 = Model([mu, mu1, log_sig, sig, x, tt],
                   var_list=['mu', 'mu1','log_sig'],
                   extra_vars=['sig', 'x'],
                   data=Data(var_name='y',
                             var_values=data))
    x = np.array([1., 2., 3.])

    dff = model1.log_posterior(x) - ff(x)
    assert np.max(np.abs(dff)) < tol

def test_h2():

    tol = 1e-2
    eps = 1e-5

    loc_mu = 0
    loc_sig = 0
    scale_mu = 4
    scale_sig = 0.5
    w = jnp.array(random.normal(loc=2, scale=1, size=20))

    z = jnp.linspace(-2, 2, num=20)
    z = jnp.linspace(-2, 2, num=20)

    data = z*w

    def ff(x):
        y = (np.sum(norm.logpdf(data, loc=x[1], scale=np.exp(x[2])))
             + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
             + np.sum(norm.logpdf(x[1], loc=z*x[0], scale=scale_mu))
             + norm.logpdf(x[2], loc=loc_sig, scale=scale_sig)
             )
        return y


    mu = Normal(var_name='mu', loc=loc_mu, scale=scale_mu)

    x = Linear(var_name='x', var=mu, coeffs=z)

    mu1 = Normal(var_name='mu1', loc='x', scale=scale_mu)

    log_sig = Normal(var_name='log_sig', loc=loc_sig, scale=scale_sig)

    sig = Exp(var_name='sig', var=log_sig)

    tt = Normal(var_name='y', loc='mu1', scale='sig')

    model1 = Model([mu, mu1, log_sig, sig, x, tt],
                   var_list=['mu', 'mu1', 'log_sig'],
                   extra_vars=['sig', 'x'],
                   data=Data(var_name='y',
                             var_values=data))
    x = np.array([1., 2., 3.])

    dff = model1.log_posterior(x) - ff(x)
    assert np.max(np.abs(dff)) < tol
