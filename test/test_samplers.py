from jax import numpy as np
from jax import jit
from markchain.classes import HamiltonianMC, MonolithicGibbs


def test_hmc():
    n = 100000
    warmup = 10000
    tol = 5e-2

    @jit
    def logpdf(x):
        return -x@x/2

    @jit
    def dlogpdf(x):
        return -x

    @jit
    def pdf(x):
        return np.exp(logpdf(x))

    start_x = np.array([0.5, -0.5])
    delta = 0.2
    masses = np.diag(np.array([1., 1.]))

    sampler = HamiltonianMC(log_pdf=logpdf, start_x=start_x, masses=masses,
                            integrator='leapfrog', J=dlogpdf,
                            w_plus=1, alpha=1)
    sampler.L = 4

    sampler.sample(n, warmup=warmup)

    trace = sampler.points()
    assert np.abs(np.mean(trace[0])) < tol

def test_gibbs():
    n = 5000
    warmup = 5000
    tol = 1e-1

    @jit
    def logpdf(x):
        return -x@x/2

    @jit
    def dlogpdf(x):
        return -x

    @jit
    def pdf(x):
        return np.exp(logpdf(x))

    start_x = np.array([0.5, -0.5])

    sampler = MonolithicGibbs(log_pdf=logpdf, start_x=start_x,
                              distrib_type=['Real', 'Real'])

    sampler.sample(n, warmup=warmup)

    trace = sampler.points()
    assert np.abs(np.mean(trace[0])) < tol
