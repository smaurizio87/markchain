from jax import numpy as jnp
import pytest
import numpy as np
from numpy import random
from scipy.stats import multivariate_normal, uniform, multivariate_t
from scipy.stats import norm, t, cauchy, beta, binom, gamma, poisson
from markchain.distributions import (Model, Data, Normal, Exp, StudentT,
                                 Cauchy, Beta, Binom, Gamma, Poisson,
                                     MultivariateNormal, ProdList, Uniform,
                                     Pow, MultivariateT, MVNormal
                                     )


def test_beta_binom():
    tol = 1e-3
    tol1 = 1e-2

    dt = jnp.array([1, 0, 0, 1, 1, 0, 0, 0])

    n = len(dt)
    a = 0.2
    b = 0.8
    x = Beta(var_name='x', a=a, b=b)
    k = Binom(var_name='k', n=n, p='x')


    def fp(p):
        x = beta.logpdf(p, a=a, b=b)
        return x

    def f(p):
        x = beta.logpdf(p, a=a, b=b) + np.sum(binom.logpmf(dt, n=len(dt), p=p))
        return x

    model = Model([x, k], var_list=['x'], data=Data(var_name='k',
                                                    var_values=dt))

    for p0 in np.random.uniform(0, 1, 20):

        rs = model.log_posterior(np.array([p0]))
        rs1 = f(p0)
        assert np.abs((rs1 - rs)/rs) < tol

        rs = model.log_prior(np.array([p0]))
        rs1 = fp(p0)
        assert np.abs((rs1 - rs)/rs1) < tol

def test_gamma_poisson():
    a = 2
    b = 3
    tol = 1e-2

    x = Gamma(var_name='x', a=a, b=b)
    k = Poisson(var_name='k', x='x')
    data = jnp.array([1,2,3,2,3,4,2,1,1,2,1])

    model = Model([x, k], var_list=['x'], data=Data(var_name='k', var_values=data))

    def ff(x):
        w = gamma.logpdf(x=x[0], a=a, scale=1/b)
        ww = np.sum(poisson.logpmf(k=data, mu=x[0]))
        return w + ww

    def fp(x):
        w = gamma.logpdf(x=x[0], a=a, scale=1/b)
        return w


    for pt0 in np.random.normal(0, 50, 100):

        pt = jnp.array([pt0])**2
        y = model.log_posterior(pt)
        assert np.abs((y - ff(pt))/y) < tol

        y1 = model.log_prior(pt)
        assert np.abs((y1 - fp(pt))/y) < tol



def test_normal():

    tol = 1e-2
    eps = 1e-5

    loc_mu = 0
    scale_mu = 4
    scale_sig = 0.5
    data = jnp.array(random.normal(loc=2, scale=1, size=20))

    def ff(x):
        y = (np.sum(norm.logpdf(data, loc=x[0], scale=np.exp(x[1])))
             + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
             + norm.logpdf(x[1], loc=x[0], scale=scale_sig)
             )
        return y

    def fp(x):
        y = (norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
             + norm.logpdf(x[1], loc=x[0], scale=scale_sig)
             )
        return y

    tt = Normal(var_name='x', loc='log_mu', scale='sig')

    log_mu = Normal(var_name='log_mu', loc=loc_mu, scale=scale_mu)

    log_sig = Normal(var_name='log_sig', loc='log_mu', scale=scale_sig)

    sig = Exp(var_name='sig', var=log_sig)

    model1 = Model([log_mu, log_sig, sig, tt],
                   var_list=['log_mu', 'log_sig'],
                   extra_vars=['sig'],
                   data=Data(var_name='x',
                             var_values=data))
    w = np.random.normal(loc=np.array([0, 0]), scale=3, size=(100, 2))
    for x in w:
        dff = model1.log_posterior(x) - ff(x)
        assert np.max(np.abs(dff/ff(x))) < tol

        dff1 = model1.log_prior(x) - fp(x)
        assert np.max(np.abs(dff1/fp(x))) < tol



def test_t():
    tol = 1e-2
    eps = 1e-5

    loc_mu = 0
    scale_mu = 4
    loc_sig = 4
    scale_sig = 0.5
    loc_df = 3.
    scale_df = 1.

    scale_df = 5.0
    data = jnp.array(random.normal(loc=2, scale=1, size=5))

    def ff(x):
        y = (np.sum(t.logpdf(data, loc=x[0], scale=np.exp(x[1]), df=x[2]))
             + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
             + norm.logpdf(x[1], loc=loc_sig, scale=scale_sig)
             + t.logpdf(x[2], loc=loc_df, scale=scale_df, df=scale_df)
             )
        return y

    tt = StudentT(var_name='x', loc='log_mu', scale='sig', df='log_df')

    log_mu = Normal(var_name='log_mu', loc=loc_mu, scale=scale_mu)

    log_sig = Normal(var_name='log_sig', loc=loc_sig, scale=scale_sig)
    log_df = StudentT(var_name='log_df', loc=loc_df, scale=scale_df,
                      df=scale_df)

    sig = Exp(var_name='sig', var=log_sig)

    model1 = Model([log_mu, log_sig, sig, log_df, tt],
                   var_list=['log_mu', 'log_sig', 'log_df'],
                   extra_vars=['sig'],
                   data=Data(var_name='x',
                             var_values=data))

    w = np.random.normal(loc=np.array([0, 0, 0]), scale=3, size=(100, 3))
    for x0 in w:
        x = np.array([x0[0], x0[1], x0[2]**2])
        dff = model1.log_posterior(x) - ff(x)
        assert np.max(np.abs(dff)/ff(x)) < tol



def test_cauchy():

    tol = 1e-2
    eps = 1e-5

    loc_mu = 0
    scale_mu = 4
    scale_sig = 0.5
    data = jnp.array(np.random.normal(loc=2, scale=1, size=10))

    def ff(x):
        y = (np.sum(cauchy.logpdf(data, loc=x[0], scale=np.exp(x[1])))
             + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
             + norm.logpdf(x[1], loc=x[0], scale=scale_sig)
             )
        return y

    tt = Cauchy(var_name='x', loc='log_mu', scale='sig')

    log_mu = Normal(var_name='log_mu', loc=loc_mu, scale=scale_mu)

    log_sig = Normal(var_name='log_sig', loc='log_mu', scale=scale_sig)

    sig = Exp(var_name='sig', var=log_sig)

    model1 = Model([log_mu, log_sig, sig, tt],
                   var_list=['log_mu', 'log_sig'],
                   extra_vars=['sig'],
                   data=Data(var_name='x',
                             var_values=data))

    w = np.random.normal(loc=np.array([0, 0]), scale=3, size=(100, 2))
    for x in w:
        dff = model1.log_posterior(x) - ff(x)
        assert np.max(np.abs(dff/ff(x))) < tol


def test_hierarchical():
    loc_mu = 0
    scale_mu = 4
    scale_sig = 0.5

    data = random.normal(loc=2, scale=1, size=10)
    data1 = random.normal(loc=2.5, scale=1, size=10)

    x = Normal(var_name='x', loc='log_mu', scale='sig')
    y = Normal(var_name='y', loc='log_mu', scale='sig')

    log_mu = Normal(var_name='log_mu', loc=loc_mu, scale=scale_mu)

    log_sig = Normal(var_name='log_sig', loc='log_mu', scale=scale_sig)

    sig = Exp(var_name='sig', var=log_sig)

    model = Model([log_mu, log_sig, sig, x, y],
                var_list=['log_mu', 'log_sig'],
                extra_vars=['sig'],
                data=[Data(var_name='x', var_values=data),
                        Data(var_name='y', var_values=data1)
                        ])


    def ff(x):
        y = (
            np.sum(norm.logpdf(data, loc=x[0], scale=np.exp(x[1])))
            + np.sum(norm.logpdf(data1, loc=x[0], scale=np.exp(x[1])))
            + norm.logpdf(x[0], loc=loc_mu, scale=scale_mu)
            + norm.logpdf(x[1], loc=x[0], scale=scale_sig)
        )
        return y

    tol = 1e-4

    w = np.random.normal(loc=np.array([0, 0]), scale=3, size=(100, 2))
    for x in w:
        assert np.abs((ff(x) - model.log_posterior(x))/ff(x)) < tol

# @pytest.mark.xfail(reason='currently not supported')
def test_multivariate_normal():
    tol = 1e-3
    mu = Normal(var_name='mu', loc=0, scale=2, shape=2)


    x = MVNormal(var_name='x',
                           loc='mu',cov = np.diag(np.array([1, 1])), shape=2)

    for _ in range(5):
        data = np.random.multivariate_normal(mean=np.array([0.5, -0.5]),
                                            cov = np.diag(np.array([1, 1])),
                                            size=5)

        model = Model([mu, x],
                    var_list=['mu'],
                    data=Data(var_name='x', var_values=data))

        def ff(x):
            rs = (np.sum(multivariate_normal.logpdf(
                data,
                mean=np.array([x[0], x[1]]),
                cov = np.diag(np.array([1. , 1.]))))
                + norm.logpdf(x[0], loc=0, scale=2)
                + norm.logpdf(x[1], loc=0, scale=2)
                )
            return rs
        x0 = np.array([-2, 4])
        assert np.abs(model.log_posterior(x0) - ff(x0)) < tol

        x1 = x0**2
        assert np.abs(model.log_posterior(x1) - ff(x1)) < tol

        x2 = x0/2+0.3
        assert np.abs(model.log_posterior(x2) - ff(x2)) < tol

        x0 = np.array([6, 1, 8, 1, -0.2])
        assert np.abs(model.log_posterior(x0) - ff(x0)) < tol

        x1 = x0**2
        assert np.abs(model.log_posterior(x1) - ff(x1)) < tol

        x2 = x0/2+0.3
        assert np.abs(model.log_posterior(x2) - ff(x2)) < tol

@pytest.mark.xfail(reason='currently not supported')
def test_multivariate_t():
    tol = 1e-4
    mu_0 = Normal(var_name='mu_0', loc=0, scale=2)
    mu_1 = Normal(var_name='mu_1', loc=0, scale=2)

    ls_0 = Normal(var_name='ls_0', loc=0, scale=1)
    ls_1 = Normal(var_name='ls_1', loc=0, scale=1)
    corr = Uniform(var_name='corr', low=-1, high=1)

    sig_0 = Pow(var=ls_0, var_name='sig_0', n=2)
    sig_1 = Pow(var=ls_1, var_name='sig_1', n=2)
    sig_diag = ProdList(var_list=[ls_0, ls_1, corr], var_name='sig_diag')

    x = MultivariateT(var_name='x',
                           loc=['mu_0', 'mu_1'],
                           shape=[['sig_0', 'sig_diag'],
                                ['sig_diag', 'sig_1']], df=4)

    for _ in range(10):
        data = np.random.multivariate_normal(mean=np.array([0.5, -0.5]),
                                            cov = np.array([[2, 1], [1, 3]]),
                                             size=3)

        model = Model([mu_0, mu_1, ls_0, corr, ls_1, sig_0, sig_1, sig_diag, x],
                    var_list=['mu_0', 'mu_1', 'ls_0', 'ls_1', 'corr'],
                    data=Data(var_name='x', var_values=data))

        def ff(x):
            rs = (np.sum(multivariate_t.logpdf(
                data,
                loc=np.array([x[0], x[1]]),
                shape = np.array([[x[2]**2, x[2]*x[3]*x[4]],
                                [x[2]*x[3]*x[4], x[3]**2]]),
                df=4
                                                    ))
                + norm.logpdf(x[0], loc=0, scale=2)
                + norm.logpdf(x[1], loc=0, scale=2)
                + norm.logpdf(x[2], loc=0, scale=1)
                + norm.logpdf(x[3], loc=0, scale=1)
                + uniform.logpdf(x[4], loc=-1, scale=2)
                )
            return rs
        x0 = np.array([-2, 4, 2, 4, 0.8])
        assert np.abs(model.log_posterior(x0)/ff(x0)-1) < tol

        x1 = x0**2
        assert np.abs(model.log_posterior(x1)/ff(x1)-1) < tol

        x2 = x0/2+0.3
        assert np.abs(model.log_posterior(x2)/ff(x2)-1) < tol

        x0 = np.array([6, 1, 8, 1, -0.2])
        assert np.abs(model.log_posterior(x0)/ff(x0)-1) < tol

        x1 = x0**2
        assert np.abs(model.log_posterior(x1)/ff(x1)-1) < tol

        x2 = x0/2+0.3
        assert np.abs(model.log_posterior(x2)/ff(x2)-1) < tol
