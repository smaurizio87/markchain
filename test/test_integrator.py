from jax import numpy as np

from markchain.classes import Leapfrog, ThirdOrderSymplecticIntegrator, FourthOrderSymplecticIntegrator


def f(q):
    return q**2


def df(q):
    return 2.0*q


def test_leapfrog():
    q = np.array([1])
    p = np.array([1])
    invm = np.diag(np.array([1]))
    delta = 1e-2
    L = 4
    i1 = Leapfrog(q=q, p=p, invm=invm, U=f, delta=delta, eps=0, J=df)
    h0 = f(q) + p @ invm @ p/2.0
    i1.integrate(L)
    h1 = f(i1.q) + i1.p @ invm @ i1.p/2.0
    tol = 1e-2
    print(i1.q, i1.p)
    assert np.abs(h1-h0) < tol

def test_third_order():
    q = np.array([1])
    p = np.array([1])
    invm = np.diag(np.array([1]))
    delta = 1e-2
    L = 4
    i1 = ThirdOrderSymplecticIntegrator(q=q, p=p, invm=invm, U=f,
                                        delta=delta, eps=0, J=df)
    h0 = f(q) + p @ invm @ p/2.0
    i1.integrate(L)
    h1 = f(i1.q) + i1.p @ invm @ i1.p/2.0
    tol = 1e-2
    print(i1.q, i1.p)
    assert np.abs(h1-h0) < tol

def test_fourth_order():
    q = np.array([1])
    p = np.array([1])
    invm = np.diag(np.array([1]))
    delta = 1e-2
    L = 4
    i1 = FourthOrderSymplecticIntegrator(q=q, p=p, invm=invm, U=f,
                                        delta=delta, eps=0, J=df)
    h0 = f(q) + p @ invm @ p/2.0
    i1.integrate(L)
    h1 = f(i1.q) + i1.p @ invm @ i1.p/2.0
    tol = 1e-2
    print(i1.q, i1.p)
    assert np.abs(h1-h0) < tol


test_third_order()
test_leapfrog()
