from collections import defaultdict
from typing import Callable
from abc import ABC, abstractmethod
from jax import numpy as np
import numpy as nn
from .distributions import Distribution, Deterministic1D

# Find an intelligent and reliable way to implement this

class GPKernel:
    def __init__(self, kernel):
        self.kernel = kernel

    def set_params(self, **kwarks):
        self.params = {**kwarks}

    def __add__(self, k):

        rs = GPKernel()


class RBF(GPKernel):
    def __init__(self):
        pass
