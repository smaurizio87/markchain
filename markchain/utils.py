import pandas as pd
import numpy as np
from jax import jit
from matplotlib import pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf


def trace_summary(trace: dict):
    out = {key: {} for key in trace.keys()}
    for key, value in trace.items():
        mean = np.mean(value)
        std = np.std(value)
        out[key]['mean'] = mean
        out[key]['median'] = np.median(value)
        out[key]['std'] = std
        out[key]['skew'] = np.mean(((value-mean)/std)**3)
        out[key]['kurt'] = np.mean(((value-mean)/std)**4)
        out[key]['qq10'] = np.quantile(value, 0.1)
        out[key]['qq90'] = np.quantile(value, 0.9)
    return out

def plot_trace(trace: dict | list, color=None, palette=None,
               bins: int|tuple=30, alpha=1):
    """
    https://www.nature.com/articles/s41562-021-01177-7
    """
    if type(trace) == dict:
        if len(list(trace.keys())) > 1:
            fig, ax = plt.subplots(nrows=len(list(trace.keys())), ncols=3)
            n = len(trace[list(trace.keys())[0]])
            x = np.arange(n)
            for l, key in enumerate(trace):
                y = trace[key]
                ax[l][0].plot(x, y, color=color)
                ax[l][0].set_ylabel(key)
                ax[l][1].hist(y, density=True, color=color, bins=50)
                plot_acf(y, ax=ax[l][2])
            fig.tight_layout()
            return fig
        else:
            fig, ax = plt.subplots(nrows=3)
            n = len(trace[list(trace.keys())[0]])
            x = np.arange(n)
            for l, key in enumerate(trace):
                y = trace[key]
                ax[0].plot(x, y, color=color)
                ax[0].set_ylabel(key)
                ax[1].hist(y, density=True, color=color, bins=50)
                plot_acf(y, ax=ax[2])
            fig.tight_layout()
            return fig
    elif type(trace) == list:
        if len(list(trace[0].keys())) > 1:
            n = len(list(trace[0].keys()))
            fig, ax = plt.subplots(nrows=n, ncols=4,
                                   figsize=(18, 3*n))
            n = len(trace[0][list(trace[0].keys())[0]])
            x = np.arange(n)
            for l, key in enumerate(trace[0]):
                y0 = [r[key] for r in trace]
                for w, y in enumerate(y0):
                    ax[l][0].plot(x, y, color=palette[w],
                                alpha=alpha)
                    ax[l][0].set_ylabel(key)
                    ax[l][1].hist(y, density=True, color=palette[w], bins=bins,
                                  alpha=alpha)
                    plot_acf(y, ax=ax[l][2], color=palette[w], alpha=alpha)

            step = 50
            q = psrf(trace, step=step)
            r_hat = q[0]
            mean = q[1]

            for l, key in enumerate(r_hat):
                y = r_hat[key]
                y_mean = mean[key]
                x = step*np.arange(len(y))
                ax[l][3].plot(x, y, color='k',
                            alpha=alpha)
                ax[l][3].set_ylim([0, 2])

                ax[l][0].plot(x, y_mean, color='k',
                            alpha=alpha)
            fig.tight_layout()
            return fig
        else:
            pass


def psrf_single(traces: list, step: int=10):
    m = len(traces)
    n = len(traces[0])
    rs = []
    rs_mean = []
    for l in range(step, n, step):
        y1 = [elem[: l] for elem in traces]
        w = np.mean([np.var(elem, ddof=1) for elem in y1])
        b_n = np.var([np.mean(elem) - np.mean(y1) for elem in y1], ddof=1)
        sig_plus = (n-1)/n*w + b_n
        r_hat = (m+1)/m*sig_plus/w - (n-1)/(m*n)
        rs.append(r_hat)
        rs_mean.append(np.mean(y1))
    return (rs, rs_mean)

def psrf(traces: dict, step: int=10):
    rs = {}
    rs_mean = {}
    for k, key in enumerate(traces[0].keys()):
        n = len(traces[0][key])
        trc = [elem[key] for elem in traces]
        w = psrf_single(trc, step=step)
        rs[key] = w[0]
        rs_mean[key] = w[1]
    return (rs, rs_mean)
