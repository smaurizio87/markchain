import numpy as nn
from scipy.stats import multivariate_normal, multivariate_t, norm, uniform
from distributions import MultivariateT, Data, Pow, Model, Normal, ProdList, Uniform

mu_0 = Normal(var_name='mu_0', loc=0, scale=2)
mu_1 = Normal(var_name='mu_1', loc=0, scale=2)

ls_0 = Normal(var_name='ls_0', loc=0, scale=1)
ls_1 = Normal(var_name='ls_1', loc=0, scale=1)
corr = Uniform(var_name='corr', low=0, high=1)

sig_0 = Pow(var=ls_0, var_name='sig_0', n=2)
sig_1 = Pow(var=ls_1, var_name='sig_1', n=2)
sig_diag = ProdList(var_list=[ls_0, ls_1, corr], var_name='sig_diag')

x = MultivariateT(var_name='x',
                       loc=['mu_0', 'mu_1'],
                       shape=[['sig_0', 'sig_diag'],
                            ['sig_diag', 'sig_1']], df=4)

data = nn.random.multivariate_normal(mean=nn.array([0.5, -0.5]),
                                     cov = nn.diag(nn.array([1, 1])))

model = Model([mu_0, mu_1, ls_0, corr, ls_1, sig_0, sig_1, sig_diag, x],
              var_list=['mu_0', 'mu_1', 'ls_0', 'ls_1', 'corr'],
              data=Data(var_name='x', var_values=data))


def ff(x):
    rs = (nn.sum(multivariate_t.logpdf(data, loc=nn.array([x[0], x[1]]),
                                    shape = nn.array([[x[2]**2, x[2]*x[3]*x[4]],
                                                    [x[2]*x[3]*x[4], x[3]**2]]),
                                       df=4
                                           ))
          + norm.logpdf(x[0], loc=0, scale=2)
          + norm.logpdf(x[1], loc=0, scale=2)
          + norm.logpdf(x[2], loc=0, scale=1)
          + norm.logpdf(x[3], loc=0, scale=1)
          + uniform.logpdf(x[4], loc=0, scale=1)
          )
    return rs
x0 = nn.array([-2, 4, 2, 4, 0.5])
print(x0)
print(ff(x0))
print(model.log_posterior(x0))

x1 = x0**2
print(x1)
print(model.log_posterior(x1))
print(ff(x1))

x2 = x0/2+0.3
print(x2)
print(model.log_posterior(x2))
print(ff(x2))
