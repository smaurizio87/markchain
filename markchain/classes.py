from multiprocess import Process, Queue
import numpy as np
from jax import numpy as jnp
from jax import grad, hessian
from abc import ABC, abstractmethod
from typing import Callable
from jax import jit
from scipy.stats import norm, kstest
from scipy.optimize import minimize
from tqdm import tqdm
from deprecated import deprecated

# Currently implementing different sampling strategies, one for
# each possible domain
# TODO: add the different updating strategies (random walk etc)
# to each sampling strategy
# Add Likelihood class


@jit
def kin_en(p, invm):
    return jnp.dot(p, jnp.dot(invm, p))/2


class MHSamplingDomain(ABC):
    """The abstract class which provides the updating strategies
    for the different sampling domains:
    Continuous -> Real_Axis, Positive, Bound [0, 1]
    Discrete   -> Integers, Integers Positive and Finite
    """
    def __init__(self):
        self.x = 0

    @abstractmethod
    def kernel(self, x: float, y: float) -> float:
        pass

    def set_kernel(self, k: Callable) -> None:
        pass

    @abstractmethod
    def default_generator(self):
        pass

    @abstractmethod
    def generator(self):
        pass

    def set_x(self, x) -> None:
        self.x = x

    @abstractmethod
    def set_strategy(self, name: str) -> None:
        pass


class FiniteSampler(MHSamplingDomain):
    def __init__(self, minval: int = 0, maxval: int = 1):
        self.minval = minval
        self.maxval = maxval

        def k0(x, y) -> float:
            return 1

        self.k = k0

    def kernel(self, x, y) -> float:
        return self.k(x, y)

    def set_kernel(self, k: Callable) -> None:
        self.k = k

    def default_generator(self):
        return np.random.default_rng().integers(low=self.minval,
                                                high=self.maxval,
                                                endpoint=True)

    def generator(self):
        return self.default_generator()

    def set_strategy(self, name: str) -> None:
        pass

    def set_step(self, step: float) -> None:
        self.step = step


class NaturalSampler(MHSamplingDomain):
    def __init__(self):
        self.step = 1

        def k0(x, y) -> float:
            return 1

        self.k = k0

    def kernel(self, x, y) -> float:
        return self.k(x, y)

    def set_kernel(self, k: Callable) -> None:
        self.k = k

    def default_generator(self):
        return (self.x + np.random.default_rng().poisson(lam=self.step)
                - np.random.default_rng().poisson(lam=self.step)
                )

    def generator(self):
        return self.default_generator()

    def set_strategy(self, name: str) -> None:
        pass

    def set_step(self, step: float) -> None:
        self.step = step


class BoundSamplier(MHSamplingDomain):
    def __init__(self, minval: float = 0, maxval: float = 1):
        self.minval = minval
        self.maxval = maxval

        def k0(x, y):
            return 1

        self.k = k0

    @jit
    def kernel(self, x, y) -> float:
        return self.k(x, y)

    def set_kernel(self, k: Callable):
        self.k = k

    def default_generator(self):
        return np.random.default_rng().uniform(self.minval, self.maxval)

    def generator(self):
        return self.default_generator()

    def set_strategy(self, name: str) -> None:
        pass


class PositiveSamplier(MHSamplingDomain):
    def __init__(self, minval: float = 0, step: float = 1) -> None:
        self.step = step

        def k0(x: float, y: float) -> float:
            return 1

        self.k = k0

    def kernel(self, x: float, y: float) -> float:
        return self.k(x, y)

    def set_kernel(self, k: Callable) -> None:
        self.k = k

    def default_generator(self) -> float:
        return np.random.default_rng().exponential(scale=self.step)

    def generator(self) -> Callable:
        return self.default_generator()

    def set_step(self, step: float) -> None:
        self.step = step

    def set_strategy(self, name: str) -> None:
        pass


class RealSamplier(MHSamplingDomain):
    def __init__(self, step: float = 1) -> None:
        self.step = step

        def k0(x: float, y: float):
            return 1  # norm.pdf(y, scale=self.step)

        self.k = jit(k0)

    def kernel(self, x: float, y: float) -> float:
        return self.k(x, y)

    def set_kernel(self, k: Callable) -> None:
        self.k = jit(k)

    def default_generator(self) -> float:
        return self.x + np.random.default_rng().normal(scale=self.step)
        # return np.random.default_rng().normal(scale=self.step)

    def generator(self):
        return self.default_generator()

    def set_step(self, step: float) -> None:
        self.step = step

    def set_strategy(self, name: str) -> None:
        pass


class MetropolisHastingsReal:
    """The 1d Metropolis-Hastings sampler for real functions"""

    def __init__(self, log_pdf: Callable,
                 start_x: float = np.random.default_rng().normal(),
                 sample_range: str = 'real', strategy: str = 'MH') -> None:

        self.sampler = {
            'bound': BoundSamplier(),
            'positive': PositiveSamplier(),
            'real': RealSamplier(),
            'finite': FiniteSampler(),
            'natural': NaturalSampler()
        }

        self.sample_range = sample_range
        self.sample_strategy = self.sampler[self.sample_range]

        self.log_pdf = log_pdf
        self.aux = jit(norm.pdf)
        self.seed = 0
        self.x = jnp.array([start_x])
        self.x_temp = start_x
        self.sample_strategy.set_x(start_x)
        self.y = jnp.array([self.seed])
        self.step = 0.1
        self.accepted = 0
        self.rejected = 0
        self.alpha_opt = 0.23

        self.strategies = {
            'MH': 0,
            'Brownian': 1,
            'Adaptive': 2,
            'Custom': 3
        }

        self.strategy = self.strategies[strategy]

    def update_pdf(self, log_pdf: Callable) -> None:
        """Change the sampled distribution. Used in the Gibbs sampler"""
        self.log_pdf = log_pdf

    def __iter__(self):
        return self

    def __next__(self) -> None:
        x_old = self.x_temp
        y_old = jnp.exp(self.log_pdf(x_old))

        # We cannot use y[-1] in the Gibbs sampler

        x_temp = self.sample_strategy.default_generator()
        y_temp = jnp.exp(self.log_pdf(x_temp))
        z_temp = self.sample_strategy.kernel(x_old, x_temp)
        z_old = self.sample_strategy.kernel(x_temp, x_old)

        alpha = jnp.minimum(1., (y_temp*z_old)/(y_old*z_temp + 1e-8))

        w = np.random.default_rng().random()

        if w < alpha:
            self.x = jnp.append(self.x, x_temp)
            self.x_temp = x_temp
            self.sample_strategy.set_x(x_temp)
            self.y = jnp.append(self.y, y_temp)
            self.accepted += 1
        else:
            self.x = jnp.append(self.x, x_old)
            self.x_temp = x_old
            self.y = jnp.append(self.y, y_old)
            self.sample_strategy.set_x(x_old)
            self.rejected += 1

        if self.strategy == 2:
            acc = self.accepted/(self.accepted + self.rejected + 1e-5)
            step = self.step
            self.step = step*((1.0 + (acc - self.alpha_opt)/(
                (jnp.size(self.x)))))
            self.sample_strategy.set_step(self.step)

    def set_step(self, step: float) -> None:
        self.step = step
        self.sample_strategy.set_step(step)

    def set_strategy(self, name: str) -> None:
        self.strategy = self.strategies[name]
        self.sample_strategy.set_strategy(name)

    def kernel(self, kernel: Callable) -> None:
        self.kernel = kernel

    def sample(self, N: int) -> None:
        for i in range(N-1):
            self.__next__()

    def get_last(self) -> float:
        return self.x_temp


class Sampler(ABC):

    @abstractmethod
    def __iter__(self):
        pass

    @abstractmethod
    def __next__(self):
        pass

    @abstractmethod
    def points(self) -> jnp.array:
        pass

    @abstractmethod
    def current_point(self) -> jnp.array:
        pass

    @abstractmethod
    def sample(self, N: int, warmup: int = 0) -> None:
        pass


@deprecated(version='0.0.1', reason="Use MonolithicGibbs")
class Gibbs(Sampler):
    """Gibbs sampler, each marginal distribution is sampled with a
    Metropolis-Hastings algorithm"""

    def __init__(self, log_pdf: Callable, start_x: jnp.array) -> None:
        self.log_pdf = jit(log_pdf)
        self.x = Trace([jnp.array(start_x)])
        self.current_x = start_x
        self.N = jnp.size(start_x)
        self.samplers = []
        self.given_conditional = False
        self.cond = None
        for k in range(self.N):
            f = self.conditional(k, self.current_x)
            sampler = MetropolisHastingsReal(f, start_x[k])
            self.samplers += [sampler]

    def conditional(self, i: int, y: jnp.array) -> Callable:
        """return the conditional probability for x_i"""
        if self.given_conditional:
            def f(x):
                x_temp = jnp.concatenate((y[: i], jnp.array([x]), y[i+1:]))
                return self.log_pdf(x_temp, i)
            return f
        else:
            def f(x):
                x_temp = jnp.concatenate((y[: i], jnp.array([x]), y[i+1:]))
                return self.log_pdf(x_temp)

            return f

    def update_x(self, i: int, y: float) -> None:
        x_temp = jnp.concatenate((self.current_x[: i], jnp.array([y]),
                                 self.current_x[i+1:]))
        self.current_x = x_temp

    def update(self, i: int) -> None:
        """Generates a new point for x_i using the MH algorithm"""
        log_pdf = lambda x: self.log_pdf(jnp.concatenate((self.current_x[: i],
                                                          jnp.array([x]),
                                                          self.current_x[i+1:]
                                                          )))
        # log_pdf = self.conditional(i, self.current_x)
        self.samplers[i].update_pdf(log_pdf)
        self.samplers[i].__next__()
        xn = self.samplers[i].get_last()
        self.update_x(i, xn)

    def __iter__(self):
        return self

    def __next__(self):
        for i in range(self.N):
            self.update(i)
        self.x.append(self.current_x)

    def sample(self, N: int, warmup: int = 0) -> None:
        self.warmup = warmup
        self.x.warmup = warmup
        for i in tqdm(range(N + self.warmup - 1)):
            # print(i)
            # print(self.x.trace[-1])
            self.__next__()

    def points(self):
        """Returns the sampled points"""
        return jnp.array(self.x[self.warmup:]).transpose()

    def current_point(self):
        return self.x[-1]

    def set_strategy(self, name: str, i: int = -1) -> None:
        if i > 0:
            self.samplers[i].set_strategy(name)
        else:
            for k in range(self.N):
                self.samplers[i].set_strategy(name)


class MonolithicGibbs:
    def __init__(self, log_pdf: Callable, start_x: jnp.array,
                 distrib_type: list, var_names: list = []):

        sampler = {
            'Bound': BoundSamplier,
            'Positive': PositiveSamplier,
            'Real': RealSamplier,
            'Finite': FiniteSampler,
            'Natural': NaturalSampler,
            'Other': None
        }
        self.x_tmp = start_x
        self.log_pdf = jit(log_pdf)
        self.samplers = [sampler[var]() for var in distrib_type]
        self.x = []
        self.var_names = var_names

    def set_log_pdf(self, log_pdf: Callable):
        self.log_pdf = log_pdf

    def __next__(self):
        x_o = self.x_tmp
        x_n = x_o
        x_aux = x_o
        for i, x_i in enumerate(self.x_tmp):
            y_old = self.log_pdf(x_n)
            self.samplers[i].x = x_i
            x_i_n = self.samplers[i].generator()
            z_tmp = self.samplers[i].kernel(x_i_n, x_i)
            z_old = self.samplers[i].kernel(x_i, x_i_n)
            x_aux = x_aux.at[i].set(x_i_n)
            y_tmp = self.log_pdf(x_aux)
            w = np.random.uniform()
            alpha = jnp.minimum(1, np.exp(y_tmp-y_old)/np.exp(z_tmp - z_old))
            if w < alpha:
                x_n = x_aux
            else:
                x_n = x_o
            x_o = x_n
            x_aux = x_n
        self.x_tmp = x_n

    def sample(self, N: int, warmup: int = 0):
        for _ in tqdm(range(warmup)):
            self.__next__()

        for _ in tqdm(range(N)):
            self.__next__()
            self.x.append(self.x_tmp)

    def points(self):
        return jnp.array(self.x).transpose()

    def points_dict(self):
        w = self.points()
        out = {name: elem for name, elem in zip(self.var_names, w)}
        return out

    def reinitialize(self):
        pass


class HamiltonianMC(Sampler):
    """Hamiltonian Monte Carlo sampler. We assume
    K = p**2/(2m) and U = -log(P)
    """
    def __init__(self, log_pdf: Callable, start_x: jnp.array,
                 masses: jnp.array = jnp.array([]),
                 integrator: str = 'leapfrog',
                 J: Callable = None, w_plus: float = 1, alpha: float = 1,
                 var_names: list = [], renormalize_mass: bool = True,
                 find_start: bool = True,
                 decorrelate: bool = False,
                 group_masses: list = None,
                 precompile: bool = True
                 ) -> None:
        self.km = 0.5
        self.km0 = 0.9
        self.kma = 0.5
        self.km0a = 0.9
        self.kmb = 0.1
        self.km0b = 0.95
        self.L_mean = 1
        self.alpha_arr = []
        self.hm = 0.0
        if precompile:
            self.log_pdf = jit(log_pdf)
        else:
            self.log_pdf = log_pdf
        self.var_names = var_names
        if find_start:
            z = minimize(lambda x: -log_pdf(x), start_x)
            start_x = z.x
        self.x_start = start_x
        self.hessian = hessian(log_pdf)(start_x)
        if np.min(np.abs(np.diag(self.hessian))) > 1e-5:
            self.scale = np.sqrt(np.abs(np.diag(self.hessian)))
        else:
            self.scale = np.ones(len(start_x))

        if J is not None:
            ff = lambda x: J(self.x_start + self.scale*x)
        else:
            ff = lambda x: grad(log_pdf)(self.x_start + self.scale*x)
        self.J = jit(ff)

        # print('SCALE', self.hessian, self.scale)
        self.x = Trace([jnp.array(start_x)])
        self.current_x = start_x
        self.x_temp = self.current_x
        self.x_new = self.x_temp
        self.N = jnp.size(start_x)
        self.integrator = integrator
        if not len(masses):
            self.m = np.diag(self.scale)
        else:
            self.m = masses
        self.renormalize_mass = renormalize_mass
        if self.renormalize_mass:
            self.m /= np.max(self.m)
        self.eps_val = 3e-2
        self.delta = 1e-1
        self.L = 1
        self.accepted = 0
        self.rejected = 0
        self.eps = self.eps_val
        # self.ratio_max = 0.6
        # self.ratio_min = 0.15
        # New versions
        self.ratio_max = 0.8
        self.ratio_min = 0.4
        self.tol = 0.2
        self.counter = 0
        self.max_count = 50
        self.cnt = 100
        self._skip = 0
        self.is_warmup = False
        self.h = 0.1
        zero = [0]*self.N
        # self.p = np.random.multivariate_normal(mean=zero, cov=self.m)
        self.p = np.random.default_rng().normal(size=self.N)
        self.p_old = self.p
        self.alpha = alpha
        self.w_sign = jnp.array([w_plus, 1.0 - w_plus])
        self.p_sign = jnp.array([1, -1])
        self.force_convergence = True
        self.pvals = [self.p]
        self.energy = []

        self.invm = jnp.linalg.inv(self.m)
        self.initialized = False

        self.integr_method = {'leapfrog': Leapfrog,
                              'third_order': ThirdOrderSymplecticIntegrator,
                              'fourth_order': FourthOrderSymplecticIntegrator,
                              'adaptive': AdaptiveLeapfrog}
        self.accept_list = []
        self.decorrelate = decorrelate
        self.thr_ks = 0.2
        self.m_scale = 1
        self.n_step = 1
        self.L_mean = 0.1
        self.n_step_mean = 0.1
        self.kmopt = 0.45

        if group_masses is None:
            self.group_masses = list(range(len(self.x_temp)))
        else:
            self.group_masses = group_masses
        self.group_masses.append(len(self.x_temp))

    def set_log_pdf(self, log_pdf: Callable, J: Callable = None):
        self.log_pdf = log_pdf
        if J is not None:
            self.J = jit(J)
        else:
            self.J = jit(grad(log_pdf))

    def U(self, x: jnp.array) -> float:
        """The potential is taken to be -log(pdf)"""
        return -self.log_pdf(x) + 1e-18

    def set_m(self, m: jnp.array):
        self.m = m
        self.invm = jnp.linalg.inv(self.m)

    def set_eps(self, eps: float) -> None:
        """Changes the step in the leapfrog algorithm"""
        self.eps_val = eps
        self.eps = eps

    def set_delta(self, delta: float) -> None:
        """Changes the step in the numerical evaluation of the
        Jacobian"""
        self.delta = delta

    def set_L(self, L: int) -> None:
        """Changes the number of leapfrog steps before updating the trace"""
        self.L = L

    def __iter__(self):
        return self

    def reinitialize(self):
        r = np.cov(np.transpose(self.x[-self.cnt:]))
        s = self.x_temp
        self.x_new = 3*np.random.default_rng().multivariate_normal(
            mean=s, cov=r
        )

    def __next__(self):
        """Performs a single step, which means L leapfrog steps, then
        uses a MH algorithm in order to select between the old and
        the new point"""
        self.n_step = 1 + np.random.poisson(lam=self.n_step_mean)
        for _ in range(self.n_step):
            self.L = 1 + np.random.poisson(lam=self.L_mean)

            if np.isnan(self.p).any():
                self.p = np.random.default_rng().normal(size=self.N)

            self.p = jnp.sqrt(
                1-self.alpha**2
            )*self.p + self.alpha*np.random.multivariate_normal(
                mean=[0]*self.N, cov=np.diag([1]*self.N))

            self.p_old = self.p
            sgn = np.random.choice(self.p_sign, p=self.w_sign)
            self.p = sgn*self.p

            self.E = kin_en(self.p, self.invm) + self.U(
                self.current_x)
            self.x_temp = self.x_new

            integrator = self.integr_method[self.integrator](self.x_temp,
                                                             self.p, self.invm,
                                                             self.U,
                                                             self.delta,
                                                             self.eps,
                                                             self.J,
                                                             x0=self.x_start,
                                                             scale=self.scale)

            integrator.integrate(self.L)

            if self.is_warmup and self.counter % self.cnt == 0 and self.counter:
                acc = np.mean(np.array(self.accept_list[-self.cnt:]))
                print(acc, self.delta, self.L)
                if acc < self.ratio_min:
                    z = (self.ratio_min - acc)/(1-self.ratio_min)
                    self.delta /= np.sqrt(1+5*z)
                    # self.m_scale *= np.sqrt(z)
                    # if self.L > 1:
                    #     self.L -= 1
                    self.L_mean *= 0.2
                elif acc > self.ratio_max:
                    z = (acc - self.ratio_max)/(1-self.ratio_max)
                    self.delta *= (1+2*z)
                    # self.L += 1
                    self.L_mean *= 1.2
                else:
                    x = np.transpose(self.x[-self.cnt:])
                    x1 = np.transpose(self.x[-int(np.floor(self.cnt/2)):])
                    x2 = np.transpose(self.x[-self.cnt:])
                    w0 = np.array([np.mean(elem) for elem in x])
                    w1 = np.array([np.mean(elem) for elem in x1])
                    std = np.array([np.std(elem) for elem in x1])
                    tmp = ((w0-w1)/std)**2
                    rr = np.array([(elem - np.mean(elem))/np.std(elem)
                                   for elem in x1])
                    # print(x1)
                    tmp2 = [kstest(x1[i], x2[i]).statistic
                            for i in range(len(x1))]
                    x_corr = np.array([np.mean(
                        [elem[k:]@elem[:-k]/len(elem) for k in range(1, 20)])
                        for elem in rr])
                    l_corr = np.max(np.abs(x_corr))
                    if self.decorrelate and l_corr > 0.5 and np.max(tmp2
                                                                    ) < 0.2:
                        self.n_step_mean *= 1.2
                    print(np.max(tmp2), self.L, self.delta, l_corr,
                          self.n_step_mean)
                    if np.max(tmp2) > self.thr_ks:
                        zz = np.sqrt(1+tmp)
                        if not np.isnan(zz).any():
                            m_tmp = np.diag(np.diag(self.m)/zz)
                            rs = np.concatenate([[np.mean(
                                [x for k, x in enumerate(m_tmp)
                                 if k >= self.group_masses[i]
                                 and k < self.group_masses[i+1]])
                            ]*(self.group_masses[i+1]-self.group_masses[i])
                                for i, elem in enumerate(self.group_masses)
                                if i < len(self.group_masses)-1]).ravel()
                            self.set_m(np.diag(rs))
                            if self.renormalize_mass:
                                self.set_m(self.m/np.max(self.m)*self.m_scale)
                    else:
                        self.cnt = int(2*self.cnt)
                        self.thr_ks = np.min(np.array([self.thr_ks,
                                                       1.5*np.max(tmp2)]))
            self.x_temp = integrator.coord()
            self.p = integrator.moment()

            w = np.random.default_rng().random()
            E_temp = kin_en(self.p, self.invm) + self.U(
                self.x_temp)
            alpha = jnp.minimum(1.0, jnp.exp(self.E - E_temp))
            if w < alpha:
                self.x_new = self.x_temp
                self.current_x = self.x_temp
                self.accepted += 1
                self.accept_list.append(1)
                self.p_old = self.p
            else:
                self.x_new = self.current_x
                self.rejected += 1
                self.accept_list.append(0)

            self.p = sgn*self.p
            # ratio = self.accepted/(self.accepted + self.rejected)
            self.counter += 1
        self.pvals.append(self.p)
        self.x.append(self.x_new)

    def sample(self, N: int, warmup: int = 0, show_progress: bool = True):
        """Performs burnout + N steps"""
        self.warmup = warmup
        self.is_warmup = bool(warmup)
        self.is_warmup = True
        if not show_progress:
            if self.is_warmup:
                for i in range(self.warmup):
                    self.__next__()
            self.is_warmup = False
            for i in range((self._skip+1)*(N-1)):
                self.__next__()
        else:
            if self.is_warmup:
                for i in tqdm(range(self.warmup)):
                    self.__next__()
            self.is_warmup = False
            for i in tqdm(range(N-1)):
                self.__next__()

    def points(self) -> np.array:
        """Returns the sampled points"""
        return jnp.array(self.x[self.warmup:]).transpose()

    def current_point(self):
        return self.x[-1]

    def points_dict(self):
        w = self.points()
        out = {name: elem for name, elem in zip(self.var_names, w)}
        return out


class SeriesMC(HamiltonianMC):
    def __next__(self):
        """Performs a single step, which means L leapfrog steps, then
        uses a MH algorithm in order to select between the old and
        the new point"""
        self.n_step = 1 + np.random.poisson(lam=self.n_step_mean)
        for _ in range(self.n_step):
            self.L = 1 + np.random.poisson(lam=self.L_mean)

            if np.isnan(self.p).any():
                self.p = np.random.default_rng().normal(size=self.N)

            self.p = jnp.sqrt(
                1-self.alpha**2
            )*self.p + self.alpha*np.random.multivariate_normal(
                mean=[0]*self.N, cov=np.diag([1]*self.N))

            self.p_old = self.p
            sgn = np.random.choice(self.p_sign, p=self.w_sign)
            self.p = sgn*self.p

            self.E = kin_en(self.p, self.invm) + self.U(
                self.current_x)
            self.x_temp = self.x_new

            integrator = self.integr_method[self.integrator](
                self.x_temp,
                self.p, self.invm,
                self.U, self.delta,
                self.eps,
                self.J,
                x0=self.x_start,
                scale=self.scale)

            integrator.integrate(self.L)

            self.x_temp = integrator.coord()
            self.p = integrator.moment()

            w = np.random.default_rng().random()
            E_temp = kin_en(self.p, self.invm) + self.U(
                self.x_temp)
            alpha = jnp.minimum(1, jnp.exp(self.E - E_temp))
            if w < alpha:
                self.x_new = self.x_temp
                self.current_x = self.x_temp
                self.accepted += 1
                self.accept_list.append(1)
                self.p_old = self.p
            else:
                self.x_new = self.current_x
                self.rejected += 1
                self.accept_list.append(0)

            self.p = sgn*self.p
            # ratio = self.accepted/(self.accepted + self.rejected)
            self.counter += 1

            if self.is_warmup and self.counter % self.cnt == 0 and self.counter:
                acc = np.mean(np.array(self.accept_list[-self.cnt:]))
                x1 = np.transpose(self.x[-int(np.floor(self.cnt/2)):])
                x2 = np.transpose(self.x[-self.cnt:])
                rr = np.array([(elem - np.mean(elem))/np.std(elem)
                               for elem in x1])
                tmp2 = [kstest(x1[i], x2[i]).statistic for i in range(len(x1))]
                x_corr = np.array([np.mean(
                    [elem[k:]@elem[:-k]/len(elem) for k in range(1, 20)])
                    for elem in rr])
                l_corr = np.max(np.abs(x_corr))
                # if l_corr > 8:
                #     self.n_step += 1
                print(self.delta, self.L_mean, self.n_step_mean)
                print(acc, np.max(tmp2), l_corr)
                if acc < self.ratio_min:
                    self.delta /= 3
                    self.L_mean /= 1.2
                elif acc > self.ratio_max:
                    self.delta *= 1.1
                else:

                    if l_corr > 0.95:
                        self.L_mean *= 1.5
                        self.n_step_mean *= 1.5
                    else:
                        self.cnt *= 2
        self.pvals.append(self.p)
        self.x.append(self.x_new)


class StochasticDualAverage(HamiltonianMC):
    def __next__(self):
        """Performs a single step, which means L leapfrog steps, then
        uses a MH algorithm in order to select between the old and
        the new point"""

        self.L = 1 + np.random.poisson(lam=self.L_mean)

        if np.isnan(self.p).any():
            self.p = np.random.default_rng().normal(size=self.N)

        self.p = jnp.sqrt(
            1-self.alpha**2
        )*self.p + self.alpha*np.random.multivariate_normal(
            mean=[0]*self.N, cov=np.diag([1]*self.N))

        self.p_old = self.p
        sgn = np.random.choice(self.p_sign, p=self.w_sign)
        self.p = sgn*self.p

        self.E = kin_en(self.p, self.invm) + self.U(
            self.current_x)
        self.x_temp = self.x_new

        integrator = self.integr_method[self.integrator](
            self.x_temp,
            self.p, self.invm,
            self.U, self.delta,
            self.eps,
            self.J,
            x0=self.x_start,
            scale=self.scale)

        integrator.integrate(self.L)

        if self.is_warmup and self.counter % self.cnt == 0 and self.counter > self.cnt:
            acc = np.mean(np.array(self.accept_list[-self.cnt:]))

            nn = self.cnt
            xa = np.transpose(self.x[-nn:])

            x1 = np.transpose(self.x[nn//2:])
            x2 = np.transpose(self.x[-nn:])
            tmp2 = [kstest(x1[i], x2[i]).statistic for i in range(len(xa))]


            w0 = np.array([np.mean(elem) for elem in xa])
            w1 = np.array([np.mean(elem) for elem in x1])
            std = np.array([np.std(elem) for elem in x1]) + 1e-6
            tmp = ((w0-w1)/std)**2
            # print(x1)
            tmp2 = [kstest(x1[i], x2[i]).statistic for i in range(len(x1))]

            if self.decorrelate:
                self.kmb = self.km0a*self.kmb + (1.0-self.km0b)*np.max(tmp)

                self.kma = self.km0a*self.kma + (1.0-self.km0a)*np.max(tmp2)
                self.L_mean = np.minimum(1/np.exp(-np.log(self.L_mean) +
                                    1/np.sqrt(self.counter+1.0)*(self.kma)-
                                    1/np.sqrt(self.counter+1.0)*(self.kmb)),
                                                    5)
            else:
                self.kma = self.km0a*self.kma + (1.0-self.km0a)*np.max(tmp2)
                self.L_mean = np.minimum(1/np.exp(-np.log(self.L_mean) +
                                    1/np.sqrt(self.counter+1.0)*(self.kma)), 5)
            if self.renormalize_mass:
                zz = np.sqrt(1+np.array(tmp2))
                zz /= np.max(zz)
                # self.set_m(np.diag(np.diag(self.m)/zz))
                # self.set_m(self.m/np.max(self.m))


                if not np.isnan(zz).any():
                    m_tmp = np.diag(np.diag(self.m)/zz)
                    rs = np.concatenate([[np.mean(
                        [x for k, x in enumerate(m_tmp)
                        if k>=self.group_masses[i]
                        and k<self.group_masses[i+1]])
                    ]*(self.group_masses[i+1]-self.group_masses[i])
                        for i, elem in enumerate(self.group_masses)
                        if i<len(self.group_masses)-1]).ravel()
                    self.set_m(np.diag(rs)/np.max(rs))
            self.cnt = int(self.cnt*1.5)
            # if np.max(tmp2) > 0.2:
            #     self.L_mean += 1
            print(nn, np.max(tmp2), np.max(tmp))
            print(self.L_mean)

            print(acc, self.delta, self.alpha)
            if np.max(tmp) > 10:
                self.x_temp = self.x_start
        #     is_ok = 1
        #     acc = np.mean(np.array(self.accept_list[-self.cnt:]))
        #     print(acc, self.delta, self.L, self.alpha)
        #     if  acc < self.ratio_min:
        #         z = (self.ratio_min - acc)/(1-self.ratio_min)
        #         self.delta /= np.sqrt(1+5*z)
        #         # self.m_scale *= np.sqrt(z)
        #         is_ok = 0
        #         # if self.L > 1:
        #         #     self.L -= 1
        #         self.L_mean *= 0.2
        #     elif acc > self.ratio_max:
        #         z = (acc - self.ratio_max)/(1-self.ratio_max)
        #         self.delta *= (1+2*z)
        #         is_ok = 0
        #         # self.L += 1
        #         self.L_mean *= 1.2
        #         print(self.L, self.delta,  self.n_step_mean)
        #     else:
        #         self.cnt = int(1.5*self.cnt)
        #         # self.alpha *= 0.96
        self.x_temp = integrator.coord()
        self.p = integrator.moment()

        w = np.random.default_rng().random()
        E_temp = kin_en(self.p, self.invm) + self.U(
            self.x_temp)
        alpha = jnp.minimum(1.0, jnp.exp(self.E - E_temp))
        if w < alpha:
            self.x_new = self.x_temp
            self.current_x = self.x_temp
            self.accepted += 1
            self.accept_list.append(1)
            self.p_old = self.p
            energy = E_temp
        else:
            self.x_new = self.current_x
            self.rejected += 1
            self.accept_list.append(0)
            energy = self.E

        self.p = sgn*self.p
        # ratio = self.accepted/(self.accepted + self.rejected)
        self.counter += 1
        self.pvals.append(self.p)
        self.x.append(self.x_new)
        if not self.is_warmup:
            self.energy.append(energy)
        # assuming alpha nondecreasing function of delta
        if self.is_warmup:
            self.km = self.km0*self.km + (1.0-self.km0)*self.accept_list[-1]
            self.delta = np.exp(np.log(self.delta) +
                                1/np.sqrt(self.counter+1.0)*(self.km-self.kmopt))




class DualAverageMC(HamiltonianMC):

    def __init__(self, log_pdf: Callable, start_x: jnp.array,
                 masses: jnp.array = jnp.array([]),
                 integrator: str = 'leapfrog',
                 J: Callable = None, w_plus: float = 1, alpha: float = 1,
                 var_names: list = [], renormalize_mass: bool = False,
                 find_start: bool = True,
                 decorrelate: bool = True,
                 group_masses: list = None
                 ) -> None:
        super().__init__(log_pdf, start_x, masses, integrator, J, w_plus,
                         alpha, var_names, find_start=find_start)
        self.acc_ratio = 0.
        self.delta = 1e-1
        self._lambda = 4
        self.log_deltabar = 0
        self.t0 = 10
        self.counter = 1

    def __next__(self):
        """
        https://arxiv.org/pdf/1111.4246.pdf
        """
        Hopt = 0.65
        t0 = 10
        gamma = 0.05
        kappa = 0.75
        self.L = 1
        if np.isnan(self.p).any():
            self.p = np.random.default_rng().normal(size=self.N)

        self.p = jnp.sqrt(1-self.alpha**2
                        )*self.p + self.alpha*np.random.multivariate_normal(
                            mean=[0]*self.N, cov=np.diag([1]*self.N))

        self.p_old = self.p
        sgn = np.random.choice(self.p_sign, p=self.w_sign)
        self.p = sgn*self.p

        self.E = kin_en(self.p, self.invm) + self.U(
            self.current_x)
        self.x_temp = self.x_new

        integrator = self.integr_method[self.integrator](self.x_temp,
                                                         self.p, self.invm,
                                                         self.U, self.delta,
                                                         self.eps,
                                                         self.J,
                                                         x0=self.x_start,
                                                         scale=self.scale)

        integrator.integrate(self.L)

        self.x_temp = integrator.coord()
        self.p = integrator.moment()

        w = np.random.default_rng().random()
        E_temp = kin_en(self.p, self.invm) + self.U(
            self.x_temp)
        alpha = jnp.minimum(1, jnp.exp(self.E - E_temp))
        if w < alpha:
            self.x_new = self.x_temp
            self.current_x = self.x_temp
            self.accepted += 1
            self.accept_list.append(1)
            self.p_old = self.p
            dd = 1.
        else:
            self.x_new = self.current_x
            self.rejected += 1
            self.accept_list.append(0)
            dd = 0.

        self.p = sgn*self.p
        self.counter += 1

        if self.is_warmup:
            self.acc_ratio = ((1. - 1/(self.counter + self.t0))*self.acc_ratio
                              + 1/(self.counter + self.t0)*dd)
            log_delta = Hopt - np.sqrt(self.counter)/gamma*self.acc_ratio
            self.log_deltabar = (self.counter)**(-kappa)*log_delta+(1.-self.counter)**(-kappa)*self.log_deltabar
            self.delta = np.exp(log_delta)
            print(log_delta, self.delta, self.acc_ratio, self.accept_list[-1])
        else:
            self.delta = np.exp(self.log_deltabar)

        self.pvals.append(self.p)
        self.x.append(self.x_new)



class Trace:
    """The trace of a Sampler"""

    def __init__(self, trace: list = []):
        self.trace = trace
        self.warpup = 0

    def append(self, x: jnp.array) -> None:
        self.trace.append(x)

    def get_variable(self, i: int) -> jnp.array([]):
        z = jnp.array([elem[i] for k, elem in enumerate(self.trace)
                      if k >= self.warmup])
        return z

    def __getitem__(self, i: int) -> jnp.array:
        return self.trace[i]

    def __setitem__(self, i: int, x: jnp.array) -> None:
        self.trace[i] = x

    def __len__(self):
        return len(self.trace)

    def random(self, n: int = 1):
        return np.random.default_rng().choice(self.trace, n)


class Integrator(ABC):
    @abstractmethod
    def __init__(self, q: jnp.array, p: jnp.array,
                 invm: jnp.array, U: Callable, delta: float, eps: float,
                 J: Callable = None, x0: jnp.array = None,
                 scale: jnp.array = None):

        if x0 is None:
            self.x0 = jnp.zeros(len(q))
        else:
            self.x0 = x0

        if scale is None:
            self.scale = jnp.ones(len(q))
        else:
            self.scale = scale

        self.q = (q-self.x0)/self.scale
        self.p = p
        self.invm = invm
        self.U = U # lambda x: U(x-self.x0)
        self.delta = delta
        self.eps = eps
        self.J = J # lambda x: J(self.scale*x+self.x0)

        # self.m = np.linalg.inv(self.invm)

    @abstractmethod
    def initialize(self):
        pass

    @abstractmethod
    def step(self):
        pass

    @abstractmethod
    def integrate(self, L):
        self.initialize()
        for k in range(L):
            self.step()

    def coord(self):
        return self.scale*self.q+self.x0

    def moment(self):
        return self.p


class Leapfrog(Integrator):
    def __init__(self, q: jnp.array, p: jnp.array,
                 invm: jnp.array, U: Callable, delta: float, eps: float,
                 J: Callable = None,
                 x0: jnp.array = None,
                 scale: jnp.array = None
                 ):
        super().__init__(q, p, invm, U, delta, eps, J, x0, scale)

    def initialize(self):
        pass

    def step(self):
        """Numerically integrates the Hamilton equations by using the leapfrog
        algorithm"""

        if not self.J:
            dp0 = -J_fwd(self.U, self.q, self.eps)
        else:
            dp0 = -self.J(self.q)

        p0 = self.p + dp0*self.delta/2

        dq = jnp.dot(self.invm, p0)
        self.q = self.q + dq*self.delta

        if not self.J:
            dp1 = -J_fwd(self.U, self.q, self.eps)
        else:
            dp1 = -self.J(self.q)

        self.p = p0 + dp1*self.delta/2

        return self.q, self.p

    def integrate(self, L):
        super().integrate(L)


class ThirdOrderSymplecticIntegrator(Integrator):
    def __init__(self, q: jnp.array, p: jnp.array,
                 invm: jnp.array, U: Callable, delta: float, eps: float,
                 J: Callable = None,
                 x0: jnp.array = None,
                 scale: jnp.array = None):
        super().__init__(q, p, invm, U, delta, eps, J, x0, scale)

    def initialize(self):
        pass

    def step(self):
        """https://en.wikipedia.org/wiki/Symplectic_integrator """
        p = self.p
        q = self.q

        a1 = 2/3
        a2 = -2/3
        a3 = 1
        b1 = 7/24
        b2 = 3/4
        b3 = -1/24

        if not self.J:
            dp = -J(self.U, q, self.eps)
        else:
            dp = -self.J(q)

        p = p + b1*self.delta*dp
        dq = jnp.dot(self.invm, p)
        q = q + a1*self.delta*dq

        if not self.J:
            dp = -J(self.U, q, self.eps)
        else:
            dp = -self.J(q)

        p = p + b2*self.delta*dp
        dq = jnp.dot(self.invm, p)
        q = q + a2*self.delta*dq

        if not self.J:
            dp = -J(self.U, q, self.eps)
        else:
            dp = -self.J(q)

        p = p + b3*self.delta*dp
        dq = jnp.dot(self.invm, p)
        q = q + a3*self.delta*dq

        self.q = q
        self.p = p

        return q, p

    def integrate(self, L):
        super().integrate(L)


class FourthOrderSymplecticIntegrator(Integrator):
    def __init__(self, q: np.array, p: np.array,
                 invm: np.array, U: Callable, delta: float, eps: float,
                 J: Callable = None,
                 x0: jnp.array = None,
                 scale: jnp.array = None):
        super().__init__(q, p, invm, U, delta, eps, J, x0, scale)

    def initialize(self):
        pass

    def step(self):
        """https://www.slac.stanford.edu/pubs/slacpubs/5000/slac-pub-5071.pdf
        (4.9)"""
        p = self.p
        q = self.q
        b0 = jnp.power(2, 1/3)
        x0 = (b0 + 1/b0 - 1)/6

        c1 = x0 + 1/2
        c4 = c1
        c2 = -x0
        c3 = c2

        d1 = 2*x0 + 1
        d2 = -4*x0 - 1
        d3 = 2*x0 + 1
        d4 = 0

        if not self.J:
            dp = -J(self.U, q, self.eps)
        else:
            dp = -self.J(q)

        p = p + c1*self.delta*dp
        dq = jnp.dot(self.invm, p)
        q = q + d1*self.delta*dq

        if not self.J:
            dp = -J(self.U, q, self.eps)
        else:
            dp = -self.J(q)

        p = p + c2*self.delta*dp
        dq = jnp.dot(self.invm, p)
        q = q + d2*self.delta*dq

        if not self.J:
            dp = -J(self.U, q, self.eps)
        else:
            dp = -self.J(q)
        p = p + c3*self.delta*dp
        dq = jnp.dot(self.invm, p)
        q = q + d3*self.delta*dq

        if not self.J:
            dp = -J(self.U, q, self.eps)
        else:
            dp = -self.J(q)
        p = p + c4*self.delta*dp
        dq = jnp.dot(self.invm, p)
        q = q + d4*self.delta*dq

        self.q = q
        self.p = p

        return q, p

    def integrate(self, L):
        super().integrate(L)


class AdaptiveLeapfrog(Integrator):
    def __init__(self, q: np.array, p: np.array,
                 invm: np.array, U: Callable, delta: float, eps: float,
                 J: Callable = None):
        """https://arxiv.org/pdf/1311.6602.pdf pag 25
        where w->dq, a->dv, tau->dt."""
        super().__init__(q, p, invm, U, delta, eps, J)
        self.m = jnp.linalg.inv(self.invm)

    def a(self, q):
        if self.J is None:
            y = -J(self.U, q, self.eps)
        else:
            y = -self.J(self.q)
        return jnp.dot(self.invm, y)

    def initialize(self):
        self.v = jnp.dot(self.invm, self.p)
        self.dq = self.v
        self.dv = self.a(self.q)

    def step(self):
        dt = self.delta
        self.q += dt/2*self.dq
        self.v += dt/2*self.dv

        self.dq += 2*(self.v-self.dq)
        self.dv += 2*(self.a(self.q) - self.dv)

        dq1 = self.dq
        dv1 = self.dv

        self.q += dt*self.dq
        self.v += dt*self.dv

        self.dq += 2*(self.v - self.dq)
        self.dv += 2*(self.a(self.q) - self.dv)

        self.q += dt/2*self.dq
        self.v += dt/2*self.dv

        self.p = jnp.dot(self.m, self.v)

        self.dq = (self.dq + dq1)/2
        self.dv = (self.dv + dv1)/2

        return self.q, self.p


def J2(f: Callable, x: np.array, eps: float) -> np.array:
    """Numerically computes the Jacobian with a symmetric
    difference algorithm"""
    N = np.size(x)
    rs = []
    for i in range(N):
        zp = np.array([x[i] + eps])
        zm = np.array([x[i] - eps])
        xp = np.concatenate((x[: i], zp, x[i+1:]))
        xm = np.concatenate((x[: i], zm, x[i+1:]))
        ypl = 2*f(xp)/3
        ymi = 2*f(xm)/3

        zp1 = np.array([x[i] + 2*eps])
        zm1 = np.array([x[i] - 2*eps])
        xp1 = np.concatenate((x[: i], zp1, x[i+1:]))
        xm1 = np.concatenate((x[: i], zm1, x[i+1:]))
        ypl1 = f(xp1)/12
        ymi1 = f(xm1)/12

        rs += [(ymi1 - ymi + ypl - ypl1)/(eps)]
    return np.array(rs)


def J1(f: Callable, x: np.array, eps: float) -> np.array:
    """Numerically computes the Jacobian with a symmetric
    difference algorithm"""
    N = np.size(x)
    rs = []
    for i in range(N):
        zp = np.array([x[i] + eps])
        zm = np.array([x[i] - eps])
        xp = np.concatenate((x[: i], zp, x[i+1:]))
        xm = np.concatenate((x[: i], zm, x[i+1:]))
        ypl = f(xp)
        ymi = f(xm)
        rs += [(ypl - ymi)/(2*eps)]
    return np.array(rs)


def J_fwd(f: Callable, x: np.array, eps: float,
          f0: np.array = None) -> np.array:
    """Numerically computes the Jacobian with a forward
    difference algorithm."""
    N = np.size(x)
    rs = []
    if f0:
        y0 = f0
    else:
        y0 = f(x)
    for i in range(N):
        zp = np.array([x[i] + eps])
        xp = np.concatenate((x[: i], zp, x[i+1:]))
        ypl = f(xp)
        rs += [(ypl - y0)/(eps)]
    return np.array(rs)


def J(f: Callable, x: np.array, eps: float) -> np.array:
    rs = J_fwd(f, x, eps)
    return np.array(rs)


class MultiTrace:
    def __init__(self, sampler, num_traces):
        self.sampler_list = [sampler for _ in range(num_traces)]
        self.num_traces = num_traces
        self.rs = []

    def sample(self, n_traces, n_sample, warmup: int = 0):

        def calc_trace(sampler):
            sampler.sample(0, warmup=warmup)
            sampler.reinitialize()
            sampler.sample(n_sample)
            w = sampler.points_dict()
            return {key: val[-n_sample:] for key, val in w.items()}

        def do_calc_trace(q, sampler):
            q.put(calc_trace(sampler))

        q = Queue()

        rs = []
        for sampler in self.sampler_list:
            p = Process(target=do_calc_trace, args=(q, sampler))
            p.start()

        for sampler in self.sampler_list:
            r = q.get()
            rs.append(r)
        self.rs = rs
        return rs
