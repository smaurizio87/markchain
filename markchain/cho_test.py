from timeit import default_timer as timer
import numpy as np
from jax import numpy as jnp
from scipy import linalg
from markchain.linalg import cho_solve, cholesky

n = 2000

for _ in range(5):
    # Collection of Gaussian vectors
    A = np.random.randn(n, n)

    # Ensure symmetry one of two ways:
    cv = (A @ A.T)

    cv = cv + np.eye(n)

    ta = timer()
    w = linalg.cholesky(cv, lower=True)
    tb = timer()
    ww = cholesky(cv)
    tc = timer()
    print('X')
    print(tb-ta)
    print(tc-tb)

    # print(w)

    u, s, vt = np.linalg.svd(cv)

    sut = np.diag(np.sqrt(s))@u.T

    q, r = np.linalg.qr(sut)

    z = np.array([r.T[:, k]*np.sign(r[k, k]) for k in range(n)]).T
    # print(z)

    y = np.random.normal(size=n)

    t0 = timer()
    p = np.linalg.solve(z.T, np.linalg.solve(z, y))
    t1 = timer()
    p1a = cho_solve(z, y)
    t2 = timer()
    print('Y')
    print(t1-t0)
    # print(p)
    print(t2-t1)
    # print(p1)
