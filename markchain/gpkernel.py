from abc import ABC, abstractmethod
from collections import defaultdict
from jax import numpy as np
from .special import Cp


class GaussianKernel(ABC):
    def __init__(self, **kwargs):
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.store = True
        self.is_white_noise = False
        self.w = None
        self.wx = None
        self.wxx = None

    def eval(self, var_values):
        x = np.array([var_values[name] for name in self.var_names
                      if name not in self.kwargs.values()])

        for arg in self.extra_args:
            if arg in self.kwargs.keys() and type(self.kwargs[arg]) == str:
                self.__dict__['_'+arg] = var_values[self.kwargs[arg]]
        return x

    def update(self, x):
        return x

    def get_extra_args(self, kwargs):
        for arg in self.extra_args:
            if arg in kwargs.keys():
                if type(kwargs[arg]) == str:
                    self.var_names += [kwargs[arg]]
                    self.__dict__['_'+arg] = None
                else:
                    self.__dict__['_'+arg] = kwargs[arg]
            else:
                self.__dict__['_'+arg] = self.default_args[arg]

    def __add__(self, x):
        y = KernelSum(self, x)
        return y

    def __mul__(self, x):
        y = KernelProd(self, x)
        return y

    def __pow__(self, n):
        y = KernelPow(self, n)
        return y

    def _no_white_noise(self, x):
        if not self.is_white_noise:
            return self.k(x)
        else:
            return 0.0

    def _get_white_noise(self, x):
        if self.is_white_noise:
            return self.k(x)
        else:
            return 0.0

    def k(self, x):
        self._x = x
        z = self.kernel(x)
        return z

    def kx(self, x):
        z = self.kernel_x(x)
        return z

    def kxx(self, x):
        z = self.kernel_xx(x)
        return z


class RBF(GaussianKernel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'theta', 'X']
        self.default_args = defaultdict(float, {'sigma': 1, 'theta': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)

        def kernel(x):
            theta = self._theta
            sigma = self._sigma
            if self.w is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.w = w
            else:
                w = self.w
            z = sigma**2*np.exp(-w/theta)
            return z

        self.kernel = kernel

        def kernel_x(x):
            theta = self._theta
            sigma = self._sigma
            if self.wx is None:
                w = ((self._x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wx = w
            else:
                w = self.wx
            z = sigma**2*np.exp(-w/theta)
            return z

        self.kernel_x = kernel_x

        def kernel_xx(x):
            theta = self._theta
            sigma = self._sigma
            if self.wxx is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wxx = w
            else:
                w = self.wxx
            z = sigma**2*np.exp(-w/theta)
            return z

        self.kernel_xx = kernel_xx




class Matern(GaussianKernel):
    def __init__(self, p, **kwargs):
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'rho', 'p', 'X']
        self.default_args = defaultdict(float, {'sigma': 1, 'rho': 1, 'p': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)
        self.w = None
        self.p = p

        def kernel(x):
            sigma = self._sigma
            rho = self._rho
            p = self.p
            print('p', p)
            w = np.array([[Cp(p, np.sqrt((x[a] - x[b])@(x[a]-x[b]))/rho)
                           for a in range(len(x))] for b in range(len(x))])
            z = sigma**2*w
            return z

        self.kernel = kernel


class Matern0(GaussianKernel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'rho', 'X']
        self.default_args = defaultdict(float, {'sigma': 1, 'rho': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)
        self.w = None

        def kernel(x):
            sigma = self._sigma
            rho = self._rho
            if self.w is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.w = w
            else:
                w = self.w
            z = sigma**2*np.exp(-np.sqrt(w)/rho)
            return z
        self.kernel = kernel

        def kernel_x(x):
            sigma = self._sigma
            rho = self._rho
            if self.wx is None:
                w = ((self._x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wx = w
            else:
                w = self.wx
            z = sigma**2*np.exp(-np.sqrt(w)/rho)
            return z
        self.kernel_x = kernel_x

        def kernel_xx(x):
            sigma = self._sigma
            rho = self._rho
            if self.wxx is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wxx = w
            else:
                w = self.wxx
            z = sigma**2*np.exp(-np.sqrt(w)/rho)
            return z
        self.kernel_xx = kernel_xx


class Matern1(GaussianKernel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'rho', 'X']
        self.default_args = defaultdict(float, {'sigma': 1, 'rho': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)
        self.w = None

        def kernel(x):
            sigma = self._sigma
            rho = self._rho
            if self.w is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.w = w
            else:
                w = self.w
            z = sigma**2*np.exp(-np.sqrt(3*w)/rho)*(1+np.sqrt(3*w)/rho)
            return z
        self.kernel = kernel

        def kernel_x(x):
            sigma = self._sigma
            rho = self._rho
            if self.wx is None:
                w = ((self._x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wx = w
            else:
                w = self.wx
            z = sigma**2*np.exp(-np.sqrt(3*w)/rho)*(1+np.sqrt(3*w)/rho)
            return z
        self.kernel_x = kernel_x

        def kernel_xx(x):
            sigma = self._sigma
            rho = self._rho
            if self.wxx is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wxx = w
            else:
                w = self.wxx
            z = sigma**2*np.exp(-np.sqrt(3*w)/rho)*(1+np.sqrt(3*w)/rho)
            return z
        self.kernel_xx = kernel_xx


class Matern2(GaussianKernel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'rho', 'X']
        self.default_args = defaultdict(float, {'sigma': 1, 'rho': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)
        self.w = None

        def kernel(x):
            sigma = self._sigma
            rho = self._rho
            if self.w is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.w = w
            else:
                w = self.w
            z = sigma**2*np.exp(-np.sqrt(5*w)/rho)*(1+np.sqrt(5*w)/rho
                                                    + 5*w**2/(3*rho**2))
            return z
        self.kernel = kernel

        def kernel_x(x):
            sigma = self._sigma
            rho = self._rho
            if self.wx is None:
                w = ((self._x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wx = w
            else:
                w = self.wx
            z = sigma**2*np.exp(-np.sqrt(5*w)/rho)*(1+np.sqrt(5*w)/rho
                                                    + 5*w**2/(3*rho**2))
            return z
        self.kernel_x = kernel_x

        def kernel_xx(x):
            sigma = self._sigma
            rho = self._rho
            if self.wxx is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wxx = w
            else:
                w = self.wxx
            z = sigma**2*np.exp(-np.sqrt(5*w)/rho)*(1+np.sqrt(5*w)/rho
                                                    + 5*w**2/(3*rho**2))
            return z
        self.kernel_xx = kernel_xx


class OrnsteinUhlenbeck(GaussianKernel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'theta', 'X']
        self.default_args = defaultdict(float, {'sigma': 1, 'theta': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)
        self.w = None

        def kernel(x):
            theta = self._theta
            sigma = self._sigma
            if self.w is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.w = w
            else:
                w = self.w
            z = sigma**2*np.exp(-np.sqrt(w)/theta)
            return z
        self.kernel = kernel

        def kernel_x(x):
            theta = self._theta
            sigma = self._sigma
            if self.wx is None:
                w = ((self._x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wx = w
            else:
                w = self.wx
            z = sigma**2*np.exp(-np.sqrt(w)/theta)
            return z
        self.kernel_x = kernel_x

        def kernel_xx(x):
            theta = self._theta
            sigma = self._sigma
            if self.wxx is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wxx = w
            else:
                w = self.wxx
            z = sigma**2*np.exp(-np.sqrt(w)/theta)
            return z
        self.kernel_xx = kernel_xx


class RationalQuadratic(GaussianKernel):
    def __init__(self, **kwargs):
        # Should be ok
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'theta',  'alpha', 'X']
        self.default_args = defaultdict(float, {'sigma': 1, 'theta': 1,
                                                'alpha': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)
        self.w = None

        def kernel(x):
            theta = self._theta
            sigma = self._sigma
            alpha = self._alpha
            if self.w is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)

                self.w = w
            else:
                w = self.w
            z = sigma**2/(1+w/(2*theta**2*alpha))**(alpha)
            return z
        self.kernel = kernel

        def kernel_x(x):
            theta = self._theta
            sigma = self._sigma
            alpha = self._alpha
            if self.wx is None:
                w = ((self._x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)
                self.wx = w
            else:
                w = self.wx
            z = sigma**2/(1+w/(2*theta**2*alpha))**(alpha)
            return z
        self.kernel_x = kernel_x

        def kernel_xx(x):
            theta = self._theta
            sigma = self._sigma
            alpha = self._alpha
            if self.wxx is None:
                w = ((x[:, np.newaxis, :] - x[np.newaxis, :, :])**2).sum(axis=2)

                self.wxx = w
            else:
                w = self.wxx
            z = sigma**2/(1+w/(2*theta**2*alpha))**(alpha)
            return z
        self.kernel_xx = kernel_xx


class LinearKernel(GaussianKernel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['theta', 'c', 'X']
        self.default_args = defaultdict(float, {'theta': 1, 'c': 0,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)

        def kernel(x):
            theta = self._theta
            c = self._c
            if self.w is None:
                w = np.dot(x-c, (x-c).T)
                self.w = w
            else:
                w = self.w
            z = w*theta
            return z
        self.kernel = kernel

        def kernel_x(x):
            theta = self._theta
            c = self._c
            if self.wx is None:
                w = np.dot(self._x-c, (x-c).T)
                self.wx = w
            else:
                w = self.wx
            z = w*theta
            return z
        self.kernel_x = kernel_x

        def kernel_xx(x):
            theta = self._theta
            c = self._c
            if self.wxx is None:
                w = np.dot(x-c, (x-c).T)
                self.wxx = w
            else:
                w = self.wxx
            z = w*theta
            return z
        self.kernel_xx = kernel_xx


class ConstantKernel(GaussianKernel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'X']
        self.default_args = defaultdict(float, {'sigma': 0,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)
        self.w = None

        def kernel(x):
            sigma = self._sigma
            if self.w is None:
                w = np.ones(shape=(len(x), len(x)))
                self.w = w
            else:
                w = self.w
            z = sigma**2*w
            return z
        self.kernel = kernel

        def kernel_x(x):
            sigma = self._sigma
            if self.wx is None:
                w = np.ones(shape=(len(self._x), len(x)))
                self.wx = w
            else:
                w = self.wx
            z = sigma**2*w
            return z
        self.kernel_x = kernel_x

        def kernel_xx(x):
            sigma = self._sigma
            if self.wxx is None:
                w = np.ones(shape=(len(x), len(x)))
                self.wxx = w
            else:
                w = self.wxx
            z = sigma**2*w
            return z
        self.kernel_xx = kernel_xx


class Minimum(GaussianKernel):
    def __init__(self, **kwargs):
        # OK
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'X']
        self.default_args = defaultdict(float, {'sigma': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)

        def kernel(x):
            sigma = self._sigma
            w = np.array([[np.minimum(x[a][0], x[b][0]) for a in range(len(x))]
                          for b in range(len(x))])
            z = sigma*w
            return z
        self.kernel = kernel


class PeriodicKernel(GaussianKernel):
    def __init__(self, **kwargs):
        # OK
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'length', 'period', 'X']
        self.default_args = defaultdict(float, {'sigma': 1, 'length': 1,
                                                'pediod': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)
        self.w = None

        def kernel(x):
            sigma = self._sigma
            length = self._length
            period = self._period
            if self.w is None:
                w = (np.sin(
                    (x[:, np.newaxis, :] - x[np.newaxis, :, :])/period)**2
                ).sum(axis=2)
                self.w = w
            else:
                w = self.w
            z = sigma**2*np.exp(-2/length**2*w)
            return z
        self.kernel = kernel

        def kernel_x(x):
            sigma = self._sigma
            length = self._length
            period = self._period
            if self.wx is None:
                w = (np.sin(
                    (self._x[:, np.newaxis, :] - x[np.newaxis, :, :])/period)**2
                ).sum(axis=2)
                self.wx = w
            else:
                w = self.wx
            z = sigma**2*np.exp(-2/length**2*w)
            return z
        self.kernel_x = kernel_x

        def kernel_xx(x):
            sigma = self._sigma
            length = self._length
            period = self._period
            if self.wxx is None:
                w = (np.sin(
                    (x[:, np.newaxis, :] - x[np.newaxis, :, :])/period)**2
                ).sum(axis=2)
                self.wxx = w
            else:
                w = self.wxx
            z = sigma**2*np.exp(-2/length**2*w)
            return z
        self.kernel_xx = kernel_xx


class CosineKernel(GaussianKernel):
    def __init__(self, **kwargs):
        # OK
        super().__init__(**kwargs)
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'period', 'X']
        self.default_args = defaultdict(float, {'sigma': 1,
                                                'pediod': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)
        self.w = None

        def kernel(x):
            sigma = self._sigma
            period = self._period
            if self.w is None:
                w = ((
                    (x[:, np.newaxis, :] - x[np.newaxis, :, :]))**2
                ).sum(axis=2)
                self.w = w
            else:
                w = self.w
            z = sigma**2*np.cos(np.sqrt(w)/period)
            return z
        self.kernel = kernel

        def kernel_x(x):
            sigma = self._sigma
            period = self._period
            if self.wx is None:
                w = ((
                    (self._x[:, np.newaxis, :] - x[np.newaxis, :, :]))**2
                ).sum(axis=2)
                self.wx = w
            else:
                w = self.wx
            z = sigma**2*np.cos(np.sqrt(w)/period)
            return z
        self.kernel_x = kernel_x

        def kernel_xx(x):
            sigma = self._sigma
            length = self._length
            period = self._period
            if self.wxx is None:
                w = (np.sin(
                    (x[:, np.newaxis, :] - x[np.newaxis, :, :]))**2
                ).sum(axis=2)
                self.wxx = w
            else:
                w = self.wxx
            z = sigma**2*np.cos(np.sqrt(w)/period)
            return z
        self.kernel_xx = kernel_xx


class WhiteNoise(GaussianKernel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # OK
        self.var_names = []
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = ['sigma', 'X']
        self.default_args = defaultdict(float, {'sigma': 1,
                                                'X': np.array([])})
        self.get_extra_args(kwargs)
        self.is_white_noise = True

        def kernel(x):
            sigma = self._sigma
            z = sigma**2*np.eye(len(x))
            return z

        def kernel_x(x):
            return 0.0

        def kernel_xx(x):
            return 0.0

        self.kernel = kernel
        self.kernel_x = kernel_x
        self.kernel_xx = kernel_xx


class KernelSum(GaussianKernel):
    def __init__(self, k1, k2):
        self.k1 = k1
        self.k2 = k2
        self.var_names = []
        self.kwargs = defaultdict(str, k1.kwargs)
        self.kwargs.update(k2.kwargs)
        self.default_args = k1.default_args
        self.default_args.update(k2.default_args)
        self.extra_args = list(self.default_args.keys())
        self.get_extra_args(self.kwargs)

    def get_extra_args(self, kwargs):
        for arg in self.extra_args:
            if arg in kwargs.keys():
                if type(kwargs[arg]) == str:
                    self.var_names += [kwargs[arg]]
                    self.__dict__['_'+arg] = None
                else:
                    self.__dict__['_'+arg] = kwargs[arg]
            else:
                self.__dict__['_'+arg] = self.default_args[arg]
        self.k1.get_extra_args(kwargs)
        self.k2.get_extra_args(kwargs)

    def k(self, x):
        self._x = x
        return self.k1.k(x) + self.k2.k(x)

    def kx(self, x):
        return self.k1.kx(x) + self.k2.kx(x)

    def kxx(self, x):
        return self.k1.kxx(x) + self.k2.kxx(x)

    def eval(self, var_values):
        x = np.array([var_values[name] for name in self.var_names
                      if name not in self.kwargs.values()])

        for arg in self.extra_args:
            if arg in self.kwargs.keys() and type(self.kwargs[arg]) == str:
                self.__dict__['_'+arg] = var_values[self.kwargs[arg]]
        self.k1.eval(var_values)
        self.k2.eval(var_values)
        return x

    def _no_white_noise(self, x):
        return self.k1._no_white_noise(x) + self.k2._no_white_noise(x)

    def _get_white_noise(self, x):
        return self.k1._get_white_noise(x) + self.k2._get_white_noise(x)


class KernelProd(GaussianKernel):
    def __init__(self, k1, k2):
        self.k1 = k1
        self.k2 = k2
        self.var_names = []
        self.kwargs = defaultdict(str, k1.kwargs)
        self.kwargs.update(k2.kwargs)
        self.default_args = k1.default_args
        self.default_args.update(k2.default_args)
        self.extra_args = list(self.default_args.keys())
        self.get_extra_args(self.kwargs)

    def get_extra_args(self, kwargs):
        for arg in self.extra_args:
            if arg in kwargs.keys():
                if type(kwargs[arg]) == str:
                    self.var_names += [kwargs[arg]]
                    self.__dict__['_'+arg] = None
                else:
                    self.__dict__['_'+arg] = kwargs[arg]
            else:
                self.__dict__['_'+arg] = self.default_args[arg]
        self.k1.get_extra_args(kwargs)
        self.k2.get_extra_args(kwargs)

    def k(self, x):
        self._x = x
        return self.k1.k(x) * self.k2.k(x)

    def kx(self, x):
        return self.k1.kx(x) * self.k2.kx(x)

    def kxx(self, x):
        return self.k1.kxx(x) * self.k2.kxx(x)

    def eval(self, var_values):
        x = np.array([var_values[name] for name in self.var_names
                      if name not in self.kwargs.values()])

        for arg in self.extra_args:
            if arg in self.kwargs.keys() and type(self.kwargs[arg]) == str:
                self.__dict__['_'+arg] = var_values[self.kwargs[arg]]
        self.k1.eval(var_values)
        self.k2.eval(var_values)
        return x

    def _no_white_noise(self, x):
        return self.k1._no_white_noise(x) * self.k2._no_white_noise(x)

    def _get_white_noise(self, x):
        return self.k1._get_white_noise(x) + self.k2._get_white_noise(x)


class KernelPow(GaussianKernel):
    def __init__(self, k1, n: float):
        self.k1 = k1
        self.n = n
        self.var_names = []
        self.kwargs = defaultdict(str, k1.kwargs)
        self.default_args = k1.default_args
        self.extra_args = list(self.default_args.keys())
        self.get_extra_args(self.kwargs)

    def get_extra_args(self, kwargs):
        for arg in self.extra_args:
            if arg in kwargs.keys():
                if type(kwargs[arg]) == str:
                    self.var_names += [kwargs[arg]]
                    self.__dict__['_'+arg] = None
                else:
                    self.__dict__['_'+arg] = kwargs[arg]
            else:
                self.__dict__['_'+arg] = self.default_args[arg]
        self.k1.get_extra_args(kwargs)

    def k(self, x):
        self._x = x
        return self.k1.k(x)**self.n

    def kx(self, x):
        return self.k1.kx(x)**self.n

    def kxx(self, x):
        return self.k1.kxx(x)**self.n

    def eval(self, var_values):
        x = np.array([var_values[name] for name in self.var_names
                      if name not in self.kwargs.values()])

        for arg in self.extra_args:
            if arg in self.kwargs.keys() and type(self.kwargs[arg]) == str:
                self.__dict__['_'+arg] = var_values[self.kwargs[arg]]
        self.k1.eval(var_values)
        return x


class KernelApply(GaussianKernel):
    def __init__(self, k1, f: callable):
        self.k1 = k1
        self.f = f
        self.var_names = []
        self.kwargs = defaultdict(str, k1.kwargs)
        self.default_args = k1.default_args
        self.extra_args = list(self.default_args.keys())
        self.get_extra_args(self.kwargs)

    def get_extra_args(self, kwargs):
        for arg in self.extra_args:
            if arg in kwargs.keys():
                if type(kwargs[arg]) == str:
                    self.var_names += [kwargs[arg]]
                    self.__dict__['_'+arg] = None
                else:
                    self.__dict__['_'+arg] = kwargs[arg]
            else:
                self.__dict__['_'+arg] = self.default_args[arg]
        self.k1.get_extra_args(kwargs)

    def k(self, x):
        self._x = x
        return self.f(self.k1.k(x))

    def kx(self, x):
        return self.f(self.k1.kx(x))

    def kxx(self, x):
        return self.f(self.k1.kxx(x))

    def eval(self, var_values):
        x = np.array([var_values[name] for name in self.var_names
                      if name not in self.kwargs.values()])

        for arg in self.extra_args:
            if arg in self.kwargs.keys() and type(self.kwargs[arg]) == str:
                self.__dict__['_'+arg] = var_values[self.kwargs[arg]]
        self.k1.eval(var_values)
        return x

