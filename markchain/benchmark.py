import numpy as np
from numba import jit
import seaborn as sns
from matplotlib import pyplot as plt
from classes import Gibbs, HamiltonianMC

N = 70000
burnout = 10000


@jit(target_backend='cuda')
def f(x):
    y = -100*(x[1]-x[0])**2 - (1-x[0])**2
    return np.exp(y/20)/(2*np.pi)

@jit(target_backend='cuda')
def df0(x):
    y = 200*(x[1]-x[0]) + 2*(1-x[0])
    return -y/20

@jit(target_backend='cuda')
def df1(x):
    y = -200*(x[1]-x[0])
    return -y/20


xstart = np.random.default_rng().normal(loc=0, scale=1, size=2)

# model = Gibbs(f, start_x=xstart)
# # model.set_strategy('Adaptive')
# model.sample(N)
#
# z = range(N+1)
#
# mean = np.array([1, 1])
# cov = np.array([[10, 11], [11, 101/10]])
#
# x = model.points()
# print(np.mean(x), np.mean(x[1]), mean)
# print(np.cov(x), cov)
#
# fig = plt.figure()
# ax0 = fig.add_subplot(211)
# ax0.plot(z, x[0])
#
# ax1 = fig.add_subplot(212)
# ax1.plot(z, x[1])
# fig.savefig('./images/trace_Rosenbrock_Gibbs.eps')
# plt.close(fig)
#
# g = sns.jointplot(x[0], x[1], kind='kde',
#                   color='b')
# g.set_axis_labels(r'$x$', r'$y$')
# fig = plt.gcf()
# fig.savefig('images/kde_Rosenbrock_Gibbs.eps')
# plt.close(fig)
def df(x):
    return np.array([df0(x), df1(x)])

model = HamiltonianMC(f, start_x=xstart,
                      J=df)
model.sample(N, warmup=1000)

z = range(N+1)

mean = np.array([1, 1])
cov = np.array([[10, 11], [11, 101/10]])

x = model.points()
print(np.mean(x), np.mean(x[1]), mean)
print(np.cov(x), cov)

fig = plt.figure()
ax0 = fig.add_subplot(211)
ax0.plot(z, x[0])

ax1 = fig.add_subplot(212)
ax1.plot(z, x[1])
fig.savefig('./images/trace_Rosenbrock_HMC.eps')
plt.close(fig)

g = sns.jointplot(x[0], x[1], kind='kde',
                  color='b')
g.set_axis_labels(r'$x$', r'$y$')
fig = plt.gcf()
fig.savefig('images/kde_Rosenbrock_HMC.eps')
plt.close(fig)

print(xstart)
