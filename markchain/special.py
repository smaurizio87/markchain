from jax import numpy as np
from jax.scipy.special import gammaln
from jax import vmap
from math import factorial


def faux(p, i, x):
    # rs = (gammaln(p+1)-gammaln(2*p+1)
    #       + gammaln(p+i+1) - gammaln(p-i+1) - gammaln(i+1))
    rsn = factorial(p)/factorial(2*p)*factorial(p+i)/factorial(p-i)/factorial(i)
    return rsn*(2*x)**(p-i)


def Cp(p, x):
    r = np.sqrt(2.0*p+1.0)*x
    # i = np.array(list(range(0, p+1)))
    w = np.array([faux(p, elem, x) for elem in range(p+1)])
    rs = np.sum(w)*np.exp(-r)
    return rs
