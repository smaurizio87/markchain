from collections import defaultdict
from typing import Callable
from abc import ABC, abstractmethod
from jax import numpy as np
import numpy as nn
from jax import grad
from jax import nn as jax_nn
from jax.scipy.special import gammaln, gammainc
from scipy.stats import multivariate_t, genextreme, multinomial
from scipy.stats import betabinom as bbinom
from jax.scipy.stats import multivariate_normal, dirichlet, betabinom
from jax.scipy import linalg
from .linalg import cholesky, cho_solve


def identity(x):
    return x


def didentity(x):
    return 1


def logcomb(n, k):
    z = gammaln(n+1) - gammaln(k+1) - gammaln(n-k+1)
    return z

def logmultim(n, k):
    z = gammaln(n+1) - np.sum(np.array([gammaln(elem+1) for elem in k]))
    return z


def resize(x):
    try:
        if len(x) == 1:
            return resize(x[0])
        elif len(x) > 1:
            return [resize(elem) for elem in x]
    except TypeError:
        return x

def cum_sum(v):
    rs = []
    for k, elem in enumerate(v):
        z = 0
        for w in v[:k]:
            z += w
        rs.append(z)
    return rs

class Distribution(ABC):
    def __init__(self, var_name: str, shape: int=1, **kwargs):
        self.var_name = var_name
        self.var_names = [var_name]
        self.kwargs = defaultdict(str, kwargs)
        self.extra_args = []
        self.default_args = defaultdict(str, {})
        self.is_deterministic = False
        self.is_multivariate = False
        self.distr_type = None
        self.shape = shape

    def get_extra_args(self, kwargs):
        for arg in self.extra_args:
            if arg in kwargs.keys():
                if type(kwargs[arg]) == str:
                    self.var_names += [kwargs[arg]]
                    self.__dict__['_'+arg] = None
                else:
                    self.__dict__['_'+arg] = kwargs[arg]
            else:
                self.__dict__['_'+arg] = self.default_args[arg]

    def sample_from_trace(self, trace: dict):
        n = len(list(trace.values())[0])
        tmp = defaultdict(np.array, {})
        aux = self.kwargs.copy()
        rs = []
        for arg_key, arg_val in aux.items():
            if type(arg_val) == str and arg_val in trace.keys():
                tmp['_'+arg_key] = trace[arg_val]
        for i in range(n):
            for key, val in tmp.items():
                self.__dict__[key] = val[i]
                z = self.random()
            rs.append(z)
        return np.array(rs)

    @abstractmethod
    def logpdf(self, x):
        pass

    def random(self):
        pass

    def eval(self, var_values):
        x = np.array([var_values[name] for name in self.var_names
                      if name not in self.kwargs.values()])

        for arg in self.extra_args:
            if arg in self.kwargs.keys() and type(self.kwargs[arg]) == str:
                self.__dict__['_'+arg] = var_values[self.kwargs[arg]]
        return x

    def get_value(self, var_values):
        x = np.array([var_values[name] for name in self.var_names
                      if name not in self.kwargs.values()])
        return resize(x)

    def update(self, x):
        return x

    def get_start_point(self):
        return 1


class Normal(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'scale']
        self.default_args = defaultdict(float, {'loc': 0, 'scale': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Real'

    def logpdf(self, x):
        y = (x - self._loc)/self._scale
        # print(f"NAME {self.var_name}")
        # print('PDF')
        # print(f"x: {x}")
        # print(f"loc: {self._loc}")
        # print(f"scale: {self._scale}")
        # print(f"y: {y}")
        rs = -y**2/2 - 0.5*np.log(2.0*np.pi) - np.log(self._scale)
        # print(f"RS: {rs}")
        return rs

    def random(self, args):
        loc = args[0]
        scale = args[1]
        z2 = nn.random.normal(loc=loc, scale=scale)
        return z2


class GaussianMixture(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'scale', 'c']
        self.default_args = defaultdict(np.array, {'loc': np.array([0]), 'scale': np.array([1]),
                                                   'c': np.array([1])})
        self.get_extra_args(kwargs)
        self.distr_type = 'MultidimReal'

    def logpdf(self, x):
        y = np.array([(x - self._loc[k])/self._scale[k]
                      for k in range(len(self._c))])
        rs = np.array([-y[i]**2/2 - 0.5*np.log(2.0*np.pi) - np.log(self._scale[i])
                       for i in range(len(self._scale))])
        w = np.sum(np.array([self._c[i]*np.exp(rs[i]) for i in range(len(self._c))]))
        ws = np.sum(self._c)
        return np.log(w/ws)


class MVNormal(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'cov']
        self.default_args = defaultdict(float, {'loc': 0, 'cov': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'MultidimReal'

    def logpdf(self, x):
        print('MV')
        # print(x)
        print(self._loc)
        print(self._cov)
        rs = multivariate_normal.logpdf(x,
                                        mean=np.array(self._loc).reshape(
                                            -1, self.shape),
                                        cov=np.array(self._cov).reshape(
                                            -1, self.shape, self.shape))
        print(np.shape(rs))
        return rs

    def random(self, args):
        loc = args[0]
        scale = args[1]
        z2 = nn.random.default_rng().multivariate_normal(loc=loc, cov=scale)
        return z2

class GaussianProcessOld(Distribution):
    def __init__(self, var_name, kernel, m: Callable=None, k: Callable=None, **kwargs):
        """Here f is integrated out"""
        self.kernel = kernel
        super().__init__(var_name, **kwargs)
        self.extra_args = ['sigma_0', 'sigma_1', 'theta', 'X']
        self.default_args = defaultdict(float, {
            'sigma_0': 1, 'sigma_1': 1, 'theta': 1, 'X': np.array([])})
        self.get_extra_args(kwargs)
        self.distr_type = 'MultidimReal'

        def m0(x, theta):
            return 0*x

        def k0(x, sigma_0, sigma_1, theta):
            w = np.array([[(x[a] - x[b])@(x[a]-x[b]) for a in range(len(x))]
                           for b in range(len(x))])
            z = sigma_1**2*np.exp(-w/theta) + sigma_0**2*np.diag(np.array([1.0]*len(x)))
            return z

        if m is None:
            self.m = m0

        if k is None:
            self.k = k0

    def logpdf(self, x):
        mean = self.m(self._X, self._theta).reshape(-1, self.shape)
        cov = self.k(self._X, self._sigma_0, self._sigma_1, self._theta).reshape(-1, self.shape, self.shape)
        L = cholesky(cov)
        alpha = cho_solve(L[0],  x[0])
        rsn = -alpha@x[0]/2 - np.log(np.diag(L[0])).sum() - len(x[0])/2*np.log(2.0*np.pi)

        return rsn

    def random(self, args):
        # TODO: implement  http://gaussianprocess.org/gpml/chapters/RW.pdf!
        # already done in examples/gprocess.py
        pass
        # sigma_0 = args[0]
        # sigma_1 = args[1]
        # theta = args[2]
        # mean = self.m(self._X, theta).reshape(-1, self.shape)
        # cov = self.k(self._X, sigma_0, sigma_1, theta).reshape(-1, self.shape, self.shape)
        # L = linalg.cholesky(cov)
        # alpha = linalg.cho_solve((L, True), x)
        # z2 = nn.random.default_rng().multivariate_normal(loc=loc, cov=scale)
        # return z2

    def sample_fstar(self, x, y, xstar):
        pass


class GaussianProcess(Distribution):
    def __init__(self, var_name, kernel, **kwargs):
        """Here f is integrated out"""
        self.kernel = kernel
        self.k = self.kernel.k
        self.kernel.get_extra_args(kwargs)
        super().__init__(var_name, **kwargs)
        self.extra_args =  self.kernel.extra_args# []# ['sigma_0', 'sigma_1', 'theta', 'X']
        self.default_args = self.kernel.default_args #defaultdict(float, {})
        self.get_extra_args(kwargs)
        self.distr_type = 'MultidimReal'

    def get_extra_args(self, kwargs):
        for arg in self.extra_args:
            if arg in kwargs.keys():
                if type(kwargs[arg]) == str:
                    self.var_names += [kwargs[arg]]
                    self.__dict__['_'+arg] = None
                else:
                    self.__dict__['_'+arg] = kwargs[arg]
            else:
                self.__dict__['_'+arg] = self.default_args[arg]
        self.kernel.get_extra_args(kwargs)


    def eval(self, var_values):
        self.kernel.eval(var_values)
        x = np.array([var_values[name] for name in self.var_names
                      if name not in self.kwargs.values()])

        for arg in self.extra_args:
            if arg in self.kwargs.keys() and type(self.kwargs[arg]) == str:
                self.__dict__['_'+arg] = var_values[self.kwargs[arg]]
        return x


    def logpdf(self, x):
        mean = 0.*self.kernel._X # self.m(self._X, self._theta).reshape(-1, self.shape)
        cov = self.kernel.k(self.kernel._X) # .reshape(-1, self.shape, self.shape)
        L = cholesky(cov)
        alpha = cho_solve(L,  x[0])
        rsn = -alpha@x[0]/2 - np.log(np.diag(L)).sum() - len(x[0])/2*np.log(2.0*np.pi)

        return rsn

    def random(self, args):
        z = nn.random.multivariate_normal(mean=args[0],
                                          cov=args[1])
        return z

    def sample_fstar(self, X, Y, x, var_values):
        tmp = self.get_pars(X, Y, x, var_values)
        out = np.array([self.random(elem) for elem in tmp])
        return out

    def _get_pars(self, X, Y, x, var_values):
        self.kernel.eval(var_values)
        K0 = self.kernel.k(X)
        Kx = self.kernel.kx(x)
        Kxx = self.kernel.kxx(x)
        Lcho = cholesky(K0)
        alpha = cho_solve(Lcho, Y)
        mn = np.dot(Kx.T, alpha)
        v = cho_solve(Lcho, Kx)
        covn = Kxx - np.dot(v.T, Kx)
        return (mn, covn)

    def get_pars(self, X, Y, x, v):
        out = []
        ks = list(v.keys())
        n = len(v[ks[0]])

        for i in range(n):
            dd = {k: v[k][i] for k in ks}
            m, c = self._get_pars(X, Y, x, dd)
            out.append((m, c))
        return out


class ARMA(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['mu', 'phi', 'sigma']
        self.default_args = defaultdict(float, {'mu': 0, 'phi': 0,  'sigma': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'MultidimReal'

    def logpdf(self, x):
        # z = self._mu + self._phi*((x[2:] + x[:-2])/2 - self._mu)
        # y = (x[1:-1] - z)/self._sigma
        # s0 = self._sigma/np.sqrt(1-self._phi**2)
        # y0 = (x[0] - self._mu)/s0
        # yf = (x[-1] - (self._mu + self._phi*(x[-2]-self._mu)))/self._sigma
        # y = np.insert(y, 0, y0)
        # y = np.insert(y, -1, yf)
        # rr = np.array([s0]+[self._sigma]*(len(x)-1))
        # rs = -y**2/2 - 0.5*np.log(2.0*np.pi) - np.log(rr)

        z = self._mu + self._phi*(x[:-1] - self._mu)
        y = (x[1:]-z)/self._sigma
        s0 = self._sigma/np.sqrt(1-self._phi**2)
        y0 = (x[0] - self._mu)/s0
        y = np.insert(y, 0, y0)
        rr = np.array([s0]+[self._sigma]*(len(x)-1))
        rs = -y**2/2 - 0.5*np.log(2.0*np.pi) - np.log(rr)
        return rs

    def random(self, args):
        pass



class HalfNormal(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'scale']
        self.default_args = defaultdict(float, {'loc': 0, 'scale': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Positive'

    def logpdf(self, x):
        y = (x - self._loc)/self._scale
        rs = -y**2/2 - 0.5*np.log(2.0*np.pi) - np.log(self._scale)
        rs = rs*np.heaviside(y-1e-15, 0) - 1e5*np.heaviside(-y+1e-15, 0)
        return rs

    def random(self, args):
        return nn.random.normal(loc=args[0], scale=args[1])


class StudentT(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'scale', 'df']
        self.default_args = defaultdict(float, {'loc': 0, 'scale': 1, 'df': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Real'

    def logpdf(self, x):
        y = (x-self._loc)/self._scale
        z = (-(self._df+1)/2.0*np.log(1+y**2/self._df)
             + gammaln((self._df+1)/2.0) - 0.5*np.log(self._df*np.pi)
             - gammaln(self._df/2.0) - np.log(self._scale))
        return z

    def random(self, args):
        loc = args[0]
        scale = args[1]
        df = args[2]
        return loc + scale*nn.random.standard_t(df=df)


class Cauchy(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'scale']
        self.default_args = defaultdict(float, {'loc': 0., 'scale': 1.})
        self.get_extra_args(kwargs)
        self.distr_type = 'Real'

    def logpdf(self, x):
        y = (x - self._loc)/self._scale
        return -np.log(1.0+y**2)-np.log(np.pi)-np.log(self._scale)

    def random(self, args):
        loc = args[0]
        scale = args[1]
        return loc + scale*nn.random.standard_cauchy()


class HalfCauchy(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'scale']
        self.default_args = defaultdict(float, {'loc': 0., 'scale': 1.})
        self.get_extra_args(kwargs)
        self.distr_type = 'Real'

    def logpdf(self, x):
        y = (x - self._loc)/self._scale
        rs = -np.log(1.0+y**2)-np.log(np.pi)-np.log(self._scale)
        rs = rs*np.heaviside(x-self._loc, 0) -1e5*np.heaviside(self._loc-x, 0)
        return rs

    def random(self, args):
        loc = args[0]
        scale = args[1]
        return loc + scale*nn.random.standard_cauchy()


class Expon(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['scale']
        self.default_args = defaultdict(float, {'scale': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Positive'

    def logpdf(self, x):
        y = x/self._scale
        y *= np.heaviside(y, 0) - 1e5*np.heaviside(-y, 0)
        return -y-np.log(self._scale)

    def logsurvivor(self, x):
        y = x/self._scale
        y *= np.heaviside(y, 0) - 1e5*np.heaviside(-y, 0)
        return -y

    def random(self, args):
        scale = args[0]
        return nn.random.exponential(scale=scale)


class Weibull(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['scale', 'k']
        self.default_args = defaultdict(float, {'scale': 1, 'k': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Positive'

    def logpdf(self, x):
        y = x/self._scale
        y *= np.heaviside(y, 0) - 1e5*np.heaviside(-y, 0)
        rs = -self._k*y + (self._k-1)*np.log(y)+np.log(self._k/self._scale)
        return rs

    def logsurvivor(self, x):
        y = x/self._scale
        y *= np.heaviside(y, 0) - 1e5*np.heaviside(-y, 0)
        rs = -self._k*y
        return rs

    def random(self, args):
        scale = args[0]
        k = args[1]
        return nn.random.weibull(scale=scale, a=k)


class Pareto(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['scale', 'k']
        self.default_args = defaultdict(float, {'scale': 1, 'k': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Positive'

    def logpdf(self, x):
        rs = -(self._k+1)*np.log(x)+np.log(self._k*self._scale)
        rs = (rs*np.heaviside(x-self._scale, 0.)
              - 1e5*np.heaviside(self._scale-x, 0))
        return rs


class Gamma(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['a', 'b']
        self.default_args = defaultdict(float, {'a': 1, 'b': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Positive'

    def logpdf(self, x):
        rs = (-gammaln(self._a) + self._a*np.log(self._b)
              + (self._a-1)*np.log(x)-self._b*x)
        return rs

    def logsurvivor(self, x):
        rs = np.log(1 - gammainc(self._a, self._b*x)/np.exp(gammaln(self._a)))
        return rs*np.heaviside(x, 0) - 1e5*np.heaviside(-x, 0)

    def random(self, args):
        a = args[0]
        b = args[1]
        return nn.random.gamma(shape=a, scale=1/b)


class GammaPoisson(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['a', 'b']
        self.default_args = defaultdict(float, {'a': 1, 'b': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Integers'

    def logpdf(self, x):
        # TODO: check
        rs = (gammaln(x+self._b) - gammaln(self._b)
              + x*np.log(self._a) - (x+self._b)*np.log(self._a+1)
              - gammaln(x+1))
        return rs


class Gev(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['mu', 'sigma', 'xi']
        self.default_args = defaultdict(float, {'mu': 0, 'sigma': 1, 'xi': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Positive'

    def logpdf(self, x):
        z = (x-self._mu)/self._sigma
        w = (1 + self._xi*z)
        t = w**(-1/self._xi)
        rs = - np.log(self._sigma) - t + (self._xi+1)*np.log(t)
        return rs*np.heaviside(w, 0) - 1e18*np.heaviside(-w, 0)

    def random(self, args):
        mu = args[0]
        sigma = args[1]
        xi = args[2]
        return genextreme.rvs(xi, loc=mu, scale=sigma)


class Beta(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['a', 'b']
        self.default_args = defaultdict(float, {'a': 1, 'b': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Bound'

    def logpdf(self, x):
        th = np.heaviside(x, 0.0) + np.heaviside(1-x, 0.0) - 1
        rs = ((self._a-1)*np.log(x) + (self._b-1)*np.log(1-x)
              - gammaln(self._a)-gammaln(self._b)
              + gammaln(self._a+self._b))
        return (rs*th - (1.0-th)*1e18)

    def random(self, args):
        a = args[0]
        b = args[1]
        return nn.random.beta(a=a, b=b)


class Binom(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['p', 'n']
        self.default_args = defaultdict(float, {'p': 0.5, 'n': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Finite'

    def logpdf(self, karr):

        def faux(k):
            rs = (k*np.log(self._p) + (self._n-k)*np.log(1-self._p)
                  + logcomb(self._n, k))
            return rs
        return faux(karr)

    def random(self, args):
        p = args[0]
        n = args[1]
        return nn.random.binomial(p=p, n=n)


class BetaBinom(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['n' ,'a', 'b']
        self.default_args = defaultdict(float, {'n': 1, 'a': 1, 'b': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Bound'

    def logpdf(self, x):
        th = np.heaviside(x, 0.0) + np.heaviside(1-x, 0.0) - 1

        rs = (gammaln(self._a + self._b)
              - gammaln(self._a)
              - gammaln(self._b)
              + gammaln(self._a + x)
              + gammaln(self._b + self._n - x)
              - gammaln(self._a + self._b + self._n)
              + gammaln(self._n + 1) - gammaln(x+1) - gammaln(self._n -x + 1))
        return rs

    def random(self, args):
        n = args[0]
        a = args[1]
        b = args[2]
        return betabinom.rvs(n, a, b)


class NegBinom(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['n', 'p']
        self.default_args = defaultdict(float, {'n': 1, 'p': 0.5})
        self.get_extra_args(kwargs)
        self.distr_type = 'Finite'

    def logpdf(self, karr):

        def faux(k):
            rs = (self._n*np.log(self._p) + (k)*np.log(1-self._p)
                  + logcomb(self._n+k-1, k))
            return rs
        return faux(karr)

    def random(self, args):
        n = args[0]
        p = args[1]
        return nn.random.negative_binomial(n=n, p=p)


class Poisson(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['x']
        self.default_args = defaultdict(float, {'x': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Natural'

    def logpdf(self, karr):

        def faux(k):
            rs = (k*np.log(self._x) - self._x - gammaln(k+1))
            return rs

        rs = faux(karr)
        return rs

    def random(self, args):
        x = args[0]
        return nn.random.poisson(lam=x)


class ZeroInflatedPoisson(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['x', 'p']
        self.default_args = defaultdict(float, {'x': 1, 'p': 0})
        self.get_extra_args(kwargs)
        self.distr_type = 'Natural'

    def logpdf(self, karr):
        def faux(k):
            rsfin = (k*np.log(self._x) - self._x - gammaln(k+1) +
                   np.log(1. - self._p)) * np.heaviside(k - 0.5, 0.0)
            rs0 = np.log((self._p + (1.-self._p)*np.exp(
                - self._p)))*np.heaviside(0.5 - k, 0.0)
            return (rsfin+rs0)

        rs = faux(karr)
        return rs

    def random(self, args):
        #  TODO: check me!
        x = args[0]
        p = args[1]
        rs = nn.random.binomial(n=1, p=p)*nn.random.poisson(lam=x)
        return rs


class Uniform(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['low', 'high']
        self.default_args = defaultdict(float, {'low': 0, 'high': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Bound'

    def logpdf(self, x):
        y = -np.log(self._high - self._low)
        y *= np.heaviside(x-self._low, 0)*np.heaviside(self._high-x, 0)
        return y - 1e18*(np.heaviside(x-self._high, 0)
                         + np.heaviside(self._low-x, 0))

    def random(self, args):
        low = args[0]
        high = args[1]
        return nn.random.uniform(low=low, high=high)


class Dirichlet(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['alpha']
        self.default_args = defaultdict(np.array, {'alpha': np.array([1.0])})
        self.get_extra_args(kwargs)
        self.distr_type = 'Positive'

    def logpdf(self, x):
        z = np.array([dirichlet.logpdf(elem, self._alpha) for elem in x])
        return z

    def random(self, args):
        alpha = args[0]
        return nn.random.default_rng().dirichlet(alpha=alpha)


class DirichletNew(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['alpha']
        self.default_args = defaultdict(np.array, {'alpha': np.array([1.0])})
        self.get_extra_args(kwargs)
        self.distr_type = 'Positive'

    def logpdf(self, x):
        # z = np.array([dirichlet.logpdf(elem, self._alpha) for elem in x])
        y = np.append(x[:len(self._alpha)-1],
                       1.0-np.sum(x[:len(self._alpha)-1]))
        z = (self._alpha-1)*np.log(y)+gammaln(np.sum(self._alpha))-np.sum(gammaln(self._alpha))
        return z

    def random(self, args):
        alpha = args[0]
        return nn.random.default_rng().dirichlet(alpha=alpha)


class Multinomial(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['n', 'p']
        self.default_args = defaultdict(float, {'n': 1, 'p': 0.5})
        self.get_extra_args(kwargs)
        self.distr_type = 'MultidimInteger'

    def logpdf(self, karr):
        def faux(k):
            rs = (np.sum(k*np.log(self._p)) + logmultim(self._n, k))
            return rs
        z = np.array([faux(elem) for elem in karr[0]])
        return z

    def random(self, args):
        n = args[0]
        p = args[1]
        return nn.random.default_rng().multinomial(n=n, pvals=p)


class ChiSquared(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['k']
        self.default_args = defaultdict(float, {'k': 1, 'scale': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Positive'

    def logpdf(self, x):
        y = np.heaviside(x, 0)*x/self._scale
        rs = (-self._k/2*np.log(2.0)-gammaln(self._k/2)
              - x/2-(self._k/2-1)*np.log(y) - np.log(self._scale))
        return rs - 1e18*np.heaviside(x, 0)

    def random(self, args):
        df = args[0]
        return self._scale*nn.random.chisquare(df=df)


class PowerLaw(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['k', 'loc', 'scale']
        self.default_args = defaultdict(float, {'k': 1,
                                                'loc': 0, 'scale': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Bound'

    def logpdf(self, x):
        y = np.heaviside(x-self._loc, 0)*(x-self._loc)/self._scale
        rs = ((self._k-1)*np.log(y) - self._k - np.log(self._scale))
        return rs - 1e18*np.heaviside(x-self._loc, 0)


class Multivariate(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'cov']
        self.is_multivariate = True

    def eval_recursive(self, arg, var_values):
        if type(arg) == list:
            return [self.eval_recursive(elem, var_values) for elem in arg]
        elif type(arg) == str:
            return var_values[arg]
        else:
            return arg

    def eval(self, var_values):
        x = np.array([var_values[name] for name in self.var_names
                      if name not in self.kwargs.values()])

        for arg in self.extra_args:
            if arg in self.kwargs.keys() and type(self.kwargs[arg]) == str:
                self.__dict__['_'+arg] = var_values[self.kwargs[arg]]
            elif arg in self.kwargs.keys() and type(self.kwargs[arg]) == list:
                w = [self.eval_recursive(elem, var_values)
                     for elem in self.kwargs[arg]
                     ]
                self.__dict__['_'+arg] = w
        return x


class MultivariateNormal(Multivariate):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'cov']
        self.default_args = defaultdict(float, {'loc': 0, 'cov': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'MultidimReal'

    def logpdf(self, x):
        rs = multivariate_normal.logpdf(x, mean=np.array(self._loc),
                                        cov=np.array(self._cov))
        return rs

    def random(self, args):
        # TODO: test
        mean = args[0]
        cov = args[1]
        z2 = nn.random.multivariate_normal(mean=mean, cov=cov)
        return z2


class MVDiagonalNormal(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'cov']
        self.default_args = defaultdict(float, {'loc': 0, 'cov': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'MultidimReal'

    def logpdf(self, x):
        rs = multivariate_normal.logpdf(x, mean=np.array(self._loc),
                                        cov=np.diag(np.array(self._cov)))
        return rs

    def random(self, args):
        # TODO: test
        mean = args[0]
        cov = args[1]
        z2 = nn.random.multivariate_normal(mean=mean, cov=np.diag(cov))
        return z2


class MultivariateT(Multivariate):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['loc', 'shape', 'df']
        self.default_args = defaultdict(float, {'loc': 0, 'cov': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'MultidimReal'

    def logpdf(self, x):
        loc = np.array(self._loc)
        p = len(loc)
        shape = np.array(self._shape)
        df = self._df
        _, s, vt = np.linalg.svd(shape, hermitian=True)
        _, ri = np.linalg.qr(np.sqrt(np.diag(1/s))@vt)

        def f_pdf(x):
            if np.shape(x) == np.shape(loc):

                y = x - loc

                yv1 = ri@y

                rs =(-(df + p)/2*np.log(1 + yv1@yv1/df) +
                    gammaln((df + p)/2) - gammaln(df/2)-p/2*np.log(df) -p/2*np.log(np.pi)-np.sum(np.log(s))/2)
                return rs
            else:
                return np.array([f_pdf(elem) for elem in x])

        return f_pdf(x)

    def random(self, args):
        # TODO: test
        loc = args[0]
        shape = args[1]
        df = args[2]
        rs = multivariate_t(loc=loc, shape=shape, df=df).rvs()
        return rs


class Data:
    def __init__(self, var_name, var_values):
        self.var_name = var_name
        self.var_values = var_values


class Model:
    def __init__(self, distribs, var_list: list = None,
                 data: Data = None,
                 extra_vars: list = None):
        self.distribs = distribs

        self.shapes = [elem.shape for elem in self.distribs
                       if not elem.is_deterministic]
        self.shapes_sum = cum_sum(self.shapes)

        if type(data) == list:
            self.data_names = [elem.var_name for elem in data]
        else:
            self.data_names = [data.var_name]

        if var_list is not None:
            self.var_list = var_list
        else:
            self.var_list = [distr.var_name for distr in self.distribs
                             if not distr.is_deterministic and distr.var_name
                             not in self.data_names]

        self.data = data
        self.extra_vars = [var.var_name for var in distribs
                           if not var.is_deterministic]

        self.random_list = [distr for distr in self.distribs if not
                            distr.is_deterministic]

        self.deterministic_list = [distr for distr in self.distribs if
                                   distr.is_deterministic]

        distrib_type = {distr.var_name: distr.distr_type for distr
                        in self.random_list}

        self.complete_vars = {distr.var_name: k
                              for k, distr in enumerate(self.random_list)
                              if distr.var_name not in self.data_names}
        tmp_vars = {distr.var_name: k + len(self.var_list)
                    for k, distr in enumerate(self.deterministic_list)}
        self.complete_vars.update(tmp_vars)

        self.distrib_type = [distrib_type[var] for var in self.var_list]

    def _prepare(self, x):
        if type(self.data) == list:
            var_values = defaultdict(float, {elem.var_name:
                                             elem.var_values
                                             for elem in self.data})
        else:
            var_values = defaultdict(float, {self.data.var_name:
                                             self.data.var_values})

        var_values.update(defaultdict(float, {self.var_list[k]:
                                              x[self.shapes_sum[k]:
                                                self.shapes_sum[k]+self.shapes[k]]
                                              for k in range(len(self.var_list))
                                              }))
        for distr in self.distribs:
            var_values = distr.update(var_values)

        return var_values

    def log_posterior(self, x):
        var_values = self._prepare(x)
        p0 = np.array([np.sum(distr.logpdf(distr.eval(var_values)))
                       for distr in self.distribs])
        return np.sum(p0)

    def posterior(self, x):
        return np.exp(self.log_posterior(x))

    def d_log_posterior(self, x):
        xx = np.array(x, dtype=float)
        y = grad(lambda x: self.log_posterior(x))
        return y(xx)

    def log_prior(self, x):
        var_values = self._prepare(x)
        p0 = np.array([np.sum(distr.logpdf(distr.eval(var_values)))
                       for distr in self.distribs if distr.var_name
                       in self.var_list])
        return np.sum(p0)

    def fill_trace(self, trace):
        out = defaultdict(list,
                          {var.var_name: []
                           for var in self.deterministic_list})
        for x in np.transpose(trace):
            var_values = self._prepare(x)
            for var in self.deterministic_list:
                out[var.var_name].append(var_values[var.var_name])
        return out

    def fill_trace_new(self, trace):
        tmp = defaultdict(np.array,
                          {self.var_list[k]: trace[k]
                           for k in range(len(self.var_list))})
        for distr in self.deterministic_list:
            x = np.array([tmp[elem.var_name]
                          for elem in distr.var_list]).transpose()
            tmp[distr.var_name] = distr.f_list(x)
        return tmp

    def f_deterministic(self, x):
        z = list(x)

        # var_values.update(defaultdict(float, {self.var_list[k]:
        #                                       x[self.shapes_sum[k]:
        #                                         self.shapes_sum[k]+self.shapes[k]]
        #                                       for k in range(len(self.var_list))
        #                                       }))
        for distr in self.deterministic_list:
            rs = distr.f(np.array([z[self.complete_vars[elem.var_name]]
                                   for elem in distr.var_list]))
            z.append(rs)
        return z

    def predictive(self, x, size: int=1):
        rs = []
        for k in range(size):
            for distr in self.distribs:
                if distr.var_name in self.data_names and not distr.is_multivariate:
                    args = [x[self.complete_vars[distr.kwargs[elem]]]
                            if type(distr.kwargs[elem]) == str
                            else distr.kwargs[elem] for elem in distr.kwargs]
                    rs.append(distr.random(args))
                elif distr.var_name in self.data_names and distr.is_multivariate:
                    def f_rec(y):
                        if type(y) == list:
                            return [f_rec(elem) for elem in y]
                        elif type(y) == str:
                            return x[self.complete_vars[y]]
                        else:
                            return y
                    args = [f_rec(distr.kwargs[elem]) for elem in distr.kwargs]
                    rs.append(distr.random(args))
        return rs


class Deterministic1D:
    def __init__(self, var, var_name, **kwargs):
        self.is_deterministic = True
        self.is_one_dim = True
        self.var_name = var_name
        self.var_names = [var_name]
        self.var = var
        self.var_list = [var]
        self.f = identity
        self.df = didentity
        self.f_list = self.f
        self.kwargs = kwargs

    def update(self, var_values):
        v = var_values
        w = self.var.get_value(var_values)
        v.update({self.var_name: self.f(w)})
        return v

    def eval(self, var_values):
        w = self.var.eval(var_values)
        for var_name in self.var.var_names:
            self.__dict__['_'+var_name] = var_values[var_name]
        return w

    def get_value(self, var_values):
        w = self.var.get_value(var_values)
        return self.f(w)

    def logpdf(self, x):
        return 0

    def d_log_p(self, x, upper, var_values):
        return defaultdict(float, {})

    def df_rec(self, var_values):
        if not self.var.is_deterministic:
            w = self.eval(var_values)[0]
            return self.df(w)
        else:
            w = self.eval(var_values)[0]
            return self.df(self.var.f(w))*self.var.df_rec(var_values)


class Exp(Deterministic1D):
    def __init__(self, var, var_name):
        super().__init__(var, var_name)
        def f(x):
            y = np.array(x)
            return np.exp(y)
        self.f = f
        self.f_list = self.f
        self.df = np.exp


class Pow(Deterministic1D):
    def __init__(self, var, var_name, n):
        super().__init__(var, var_name)

        self.n = n

        def f(x):
            return x**self.n

        def df(x):
            return self.n*x**(self.n-1)

        self.f = f
        self.f_list = self.f
        self.df = df

class UMinus(Deterministic1D):
    def __init__(self, var, var_name, arg=None):
        super().__init__(var, var_name)

        def f(x):
            return -x

        self.f = f


class Sum(Deterministic1D):
    def __init__(self, var, var_name, arg, shape: int=1):
        super().__init__(var, var_name)

        self.arg = arg

        def f(x):
            if shape == 1:
                return x + self.arg
            else:
                return np.array([x]*shape) + arg

        self.f = f


class Prod(Deterministic1D):
    def __init__(self, var, var_name, arg, shape: int=1):
        super().__init__(var, var_name)

        self.arg = arg

        def f(x):
            return np.array([x]*shape) * self.arg

        self.f = f


class Log(Deterministic1D):
    def __init__(self, var, var_name):
        super().__init__(var, var_name)
        self.f = np.log
        self.f_list = self.f

        def df(x):
            return 1/x
        self.df = df


class Linear(Deterministic1D):
    def __init__(self, var, var_name, coeffs: np.array):
        super().__init__(var, var_name)

        self.coeffs = coeffs

        def f(x):
            return self.coeffs*x

        def df(x):
            return self.coeffs

        def f_list(x):
            return np.array([f(elem) for elem in x])

        self.f = f
        self.f_list = f_list
        self.df = df


class Logistic(Deterministic1D):
    def __init__(self, var, var_name):
        super().__init__(var, var_name)

        def f(x):
            y = 1.0/(1+np.exp(-x))
            return y
        self.f = f
        self.f_list = f




class MultidimDeterministic:
    def __init__(self, var_name: str, var_list: list):
        self.is_deterministic = True
        self.var_name = var_name
        self.var_list = var_list
        self.var_names = [var_name]
        self.is_one_dim = False

    def update(self, var_values):
        v = var_values
        w = [var.get_value(var_values) for var in self.var_list]
        v.update({self.var_name: self.f(w)})
        return v

    def eval(self, var_values):
        w = [var.eval(var_values) for var in self.var_list]
        for var in self.var_list:
            for var_name in var.var_names:
                self.__dict__['_'+var_name] = var_values[var_name]
        return w

    def get_value(self, var_values):
        w = [var.get_value(var_values) for var in self.var_list]
        return self.f(w)

    def logpdf(self, x):
        return 0


class SumList(MultidimDeterministic):
    def __init__(self, var_name, var_list):
        super().__init__(var_name, var_list)

        def f(x):
            rs = x[0]*0.0
            for w in x:
                rs += w
            return rs

        def f_list(x):
            return np.array([f(elem) for elem in x])

        self.f = f
        self.f_list = f_list

    def logpdf(self, x):
        return 0

class CumSum(MultidimDeterministic):
    def __init__(self, var_name, var_list):
        super().__init__(var_name, var_list)
        self.f = np.cumsum
        self.f_list = self.f

    def logpdf(self, x):
        return 0

class ProdList(MultidimDeterministic):
    def __init__(self, var_name, var_list):
        super().__init__(var_name, var_list)

        def f(x):
            rs = 1
            for w in x:
                rs *= w
            return rs

        def f_list(x):
            return np.array([f(elem) for elem in x])

        self.f = f
        self.f_list = f_list

    def logpdf(self, x):
        return 0


class SoftMax(Deterministic1D):
    def __init__(self, var_name, var):
        super().__init__(var, var_name)

        def f(x):
            rs = jax_nn.softmax(x)
            return rs

        def f_list(x):
            return np.array([f(elem) for elem in x])

        self.f = f
        self.f_list = f_list


class StickBreaking(Deterministic1D):
    def __init__(self, var, var_name):
        super().__init__(var, var_name)
        def f(x):
            y = np.insert(np.cumprod(1-np.array(x)), 0, 1)
            z = np.array(x)*y[:len(x)]
            return z

        self.f = f
        self.f_list = f


class Minus(MultidimDeterministic):
    def __init__(self, var_name, var_list):
        super().__init__(var_name, var_list)

        def f(x):
            rs = x[0]-x[1]
            return rs

        def f_list(x):
            return np.array([f(elem) for elem in x])

        self.f = f
        self.f_list = f_list

    def logpdf(self, x):
        return 0

class IfElse(MultidimDeterministic):
    def __init__(self, var_name, var_list, shape: int=1):
        super().__init__(var_name, var_list)

        def f(x):
            z = (x[1]*np.heaviside(
                x[0], 0)
                 +x[2]*np.heaviside(
                     1-x[0], 0))
            return np.array(z).reshape(-1, shape)

        self.f = f
        def f_list(x):
            z = np.array([f(elem) for elem in x])
            return z

        self.f_list = f


class WeibullHazard(Distribution):
    def __init__(self, var_name, delta, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['scale', 'k']
        self.default_args = defaultdict(float, {'scale': 1, 'k': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Positive'
        self.delta = delta

    def logpdf0(self, x):
        y = x/self._scale
        y *= np.heaviside(y, 0) - 1e5*np.heaviside(-y, 0)
        rs = -self._k*y + (self._k-1)*np.log(y)+np.log(self._k/self._scale)
        return rs

    def logsurvivor0(self, x):
        y = x/self._scale
        y *= np.heaviside(y, 0) - 1e5*np.heaviside(-y, 0)
        rs = -self._k*y
        return rs

    def logpdf(self, x):
        z = self.delta*self.logpdf0(x) + (1-self.delta)*self.logsurvivor0(x)
        return z

    def random(self, args):
        scale = args[0]
        k = args[1]
        return nn.random.weibull(a=k)*scale


class SVM(Distribution):
    def __init__(self, var_name, **kwargs):
        super().__init__(var_name, **kwargs)
        self.extra_args = ['h0', 'phi', 'scale', 'mu']
        self.default_args = defaultdict(float, {'h0': 0, 'phi': 0, 'scale': 1,
                                                'mu': 1})
        self.get_extra_args(kwargs)
        self.distr_type = 'Real'

    def logpdf(self, x):
        loc = self._mu + self._phi*(x[0][:-1] - self._mu)

        z = np.insert(loc, 0, self._h0)
        y = (x - z)/self._scale
        rs = -y**2/2 - 0.5*np.log(2.0*np.pi) - np.log(self._scale)
        return rs
