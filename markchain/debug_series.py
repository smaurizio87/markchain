import pandas as pd
from jax import numpy as np
import numpy as nn
from markchain.distributions import MVDiagonalNormal, Data, Model, Normal,\
    Exp, Expon, Distribution, Uniform, Pow, SVM
from markchain.classes import SeriesMC, HamiltonianMC
from markchain.utils import plot_trace, trace_summary

N = 20000
W = 20000

sz = 100


Y = nn.random.multivariate_normal(mean=nn.array([0.0]*sz),
                                     cov=nn.diag(nn.array([1.]*sz)),
                                     size=1)

data = Data(var_name='y',
            var_values=Y)

h0 = Normal(var_name='h0', loc=0., scale=1.)
log_sig = Normal(var_name='log_sig', scale=1.)
sig = Exp(var_name='sig', var=log_sig)
phi = Uniform(var_name='phi', low=-1., high=1.)
mu = Normal(var_name='mu', loc=0., scale=1.)
h = SVM(var_name='h', h0='h0', phi='phi', mu='mu', scale='sig', shape=sz)

y = Normal(var_name='y', loc='h', scale=1)

model = Model([h0, log_sig, sig, phi, mu, h],
              var_list=['h0', 'log_sig', 'phi', 'mu', 'h'],
              data=Data(var_name='y', var_values=np.log(Y**2)))
z0 = np.array([0., 0., 0., 0.]+[0.0]*sz)
print(model.log_posterior(z0))

sampler = HamiltonianMC(log_pdf=model.log_posterior,
                        renormalize_mass=False,
                        start_x=z0)

sampler.delta = 1e-2
sampler.L_mean = 0.1
sampler.L = 1
sampler.sample(N, warmup=W)

# mu = Normal(var_name='mu', loc=0, scale=1)
# x = Normal(var_name='x',
#            loc='mu', scale=2, shape=sz)
#
# y = Normal(var_name='y',
#            loc='mu', scale=2, shape=sz)
#
# model = Model([mu, x, y],var_list=['mu', 'x'], data=data)
#
# z0 = np.random.normal(size=sz)
# print(model.log_posterior(z0))
#
# z0[-1] = 2
#
# sampler = SeriesMC(log_pdf=model.log_posterior,
#                         start_x=z0, find_start=True,
#                    var_names=model.var_list)
#
# sampler.delta = 1e-2
# sampler.sample(N, warmup=W)
# trace = sampler.points_dict()
# print(pd.DataFrame(trace_summary(trace)))
