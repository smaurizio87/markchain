import pandas as pd
import numpy as np
import seaborn as sns
from numba import jit
from matplotlib import pyplot as plt
from markchain.classes import HamiltonianMC

N = 10000
WARMUP = 0

PLOT_FIGS = 1

N_TRACES = 4

palette = 'crest'


@jit(target_backend='cuda')
def t_ev(x, xi):
    if xi != 0:
        z = (1.0 + xi*x)
        return z**(-1/xi)
    elif xi == 0:
        return np.exp(-x)


@jit(target_backend='cuda')
def logt_ev(x, xi):
    # xi = 0
    if xi != 0:
        z = (1.0 + xi*x)
        return (-1/xi*np.log(z))
    elif xi == 0:
        return -x
    else:
        return 0


@jit(target_backend='cuda')
def pdf_ev(x, xi, sig, mu):
    z = (x-mu)/sig
    if xi == 0:
        xpn = np.exp(-z) + z
        return np.exp(-xpn)

    else:
        w = (1 + xi*z)
        if w > 0:
            res = np.exp(-w**(-1/xi))*w**(-1-1/xi)/sig
            return res
        else:
            return 0.0


@jit(target_backend='cuda')
def logpdf_ev(x, xi, logsig, mu):
    sig = np.exp(logsig)

    z = (x-mu)/sig
    if xi == 0:
        # xpn = np.exp(-z) + z
        # return -xpn
        res = -logsig - z - np.exp(-z)
        return res

    else:
        w = (1 + xi*z)
        if w > 0:
            # res = -w**(-1/xi) + (-1-1/xi)*np.log(w) - logsig
            res = - logsig - (1+1/xi)*np.log(w) - w**(-1/xi)
            return res
        else:
            return -1e18


def logpdf_ev_vec(x, xi, logsig, mu):
    sig = np.exp(logsig)

    z = (x-mu)/sig
    w = (1 + xi*z)
    res = - logsig - (1+1/xi)*np.log(w) - w**(-1/xi)
    return np.sum(np.heaviside(w, 0.0)*res - 1e18*np.heaviside(-w, res))


def dlogpdf(x, xi, logsig, mu):
    sig = np.exp(logsig)
    z = (x-mu)/sig
    w = (1 + xi*z)
    w *= np.heaviside(w, 0.0)
    w1 = z/w
    log_w = np.log(w)
    rs_xi = -w1*(1/xi + 1) + (w1/(xi) - log_w/xi**2)/w**(1/xi) + log_w/xi**2

    rs0 = - (1 + xi) / w + w**(-1-1/xi)

    dz_mu = -1/sig
    dz_logsig = -z
    v1 = np.sum(rs_xi)
    v2 = np.sum(rs0*dz_mu)
    v3 = np.sum((rs0*dz_logsig-1))
    return np.array([v1, v2, v3])


@jit(target_backend='cuda')
def dlogpdf_ev_vec_xi(x, xi, logsig, mu):
    sig = np.exp(logsig)

    z = (x-mu)/sig
    w = (1 + xi*z)
    # res = - logsig - (1+1/xi)*np.log(w) - w**(-1/xi)
    rs = (-z*(1/xi + 1)/w + (z/(w*xi) - np.log(w)/xi**2)/w**(1/xi) +
          np.log(w)/xi**2)
    return np.sum(np.heaviside(w, 0.0)*rs - 1e18*np.heaviside(-w, rs))


@jit(target_backend='cuda')
def dlogpdf_ev_vec_mu(x, xi, logsig, mu):
    sig = np.exp(logsig)

    z = (x-mu)/sig
    dz = -1/sig
    w = (1 + xi*z)
    # res = - logsig - (1+1/xi)*np.log(w) - w**(-1/xi)
    rs0 = (-xi*(1/xi + 1)/w + w**(1/xi - 1)/w**(2/xi))
    rs = rs0*dz
    return np.sum(np.heaviside(w, 0.0)*rs - 1e18*np.heaviside(-w, rs))


@jit(target_backend='cuda')
def dlogpdf_ev_vec_logsig(x, xi, logsig, mu):
    sig = np.exp(logsig)

    z = (x-mu)/sig
    dz = -z  # D[(x-mu)e^(-logsig), logsig] = -(x-mu)e*(-logsig)
    w = (1 + xi*z)
    # res = - logsig - (1+1/xi)*np.log(w) - w**(-1/xi)
    rs0 = (-xi*(1/xi + 1)/w + w**(1/xi - 1)/w**(2/xi))
    rs = rs0*dz-1
    return np.sum(np.heaviside(w, 0.0)*rs - 1e18*np.heaviside(-w, rs))


def l_pdf(x, logprior, data):
    xi = x[0]/10
    mu = x[1]/1000
    logsig = -x[2]

    def f_aux(data):
        return logpdf_ev_vec(data, xi, logsig, mu)

    loglikelihood = f_aux(data)
    logpriors = logprior(xi, mu, logsig)
    w = loglikelihood + logpriors
    return w


def dl_pdf(x, dlogprior, data):
    xi = x[0]/10
    mu = x[1]/1000
    logsig = -x[2]

    def df_aux(data):
        z = dlogpdf(data, xi, logsig, mu)
        return np.array([z[0]/10, z[1]/1000, -z[2]])

    dloglikelihood = df_aux(data)
    dlogpriors = np.array([dlogprior(xi)/10,
                           dlogprior(mu)/1000,
                           -dlogprior(logsig)])
    w = dloglikelihood + dlogpriors
    return w

# We compute the monthly maximum absolute losses of the daily log returns


d0 = pd.read_csv('./AAPL.csv')
d0['Date'] = pd.to_datetime(d0['Date'])
d0.set_index('Date')
d0['yyyymm'] = d0['Date'].dt.year.astype('str') + d0['Date'].dt.month.astype(
    'str')
d0['diff1'] = np.log(d0['Close']).diff()

data = -d0.groupby('yyyymm').min()['diff1'].values


def logprior(xi, mu, logsig):
    return -(mu/100)**2-(logsig/100)**2-(xi/100)**2


def dlogprior(x):
    return -x/50


def f(x):
    y = l_pdf(x, logprior, data)
    return np.exp(y)


def df(x):
    # y = l_pdf(x, logprior, data)
    dy = dl_pdf(x, dlogprior, data)
    return dy


out = []

for i in range(N_TRACES):
    print(f'Trace {i}')

    x_0 = np.array([2.5,  30,  4])
    x_start = np.random.default_rng().normal(loc=x_0, scale=.015*x_0, size=3)

    L = 1
    delta = .014

    masses = np.diag(np.array([0.2, 0.02, 1]))

    sampler = HamiltonianMC(f, start_x=x_start, integrator='third_order',
                            masses=masses, J=df, w_plus=0.5, alpha=0.7)

    sampler.L = L
    sampler.delta = delta

    sampler.sample(2*N, warmup=0)

    y = sampler.points()

    rs = [elem[N:] for elem in y]

    np.savetxt(f'gev_fin_{i}.csv', rs)
    out.append(rs)

x = range(len(out[0][0]))
df = {r'$\xi$': {str(k): out[k][0] for k in range(N_TRACES)},
      r'$\mu$': {str(k): out[k][1] for k in range(N_TRACES)},
      r'$\sigma$': {str(k): out[k][2] for k in range(N_TRACES)}
      }

names = [r'$\xi$', r'$\mu$', r'$\sigma$']
fig, ax = plt.subplots(nrows=3, ncols=2)


for i in range(3):
    cols = sns.color_palette(palette=palette)
    for j in range(N_TRACES):
        ax[i][0].plot(x, out[j][i], alpha=0.5, color=cols[j])
        ax[i][0].set_xlim([0, N])
        ax[i][1].set_ylabel(names[i], rotation=0)
    sns.kdeplot(data=df[names[i]], ax=ax[i][1], alpha=0.5, palette=palette)
    # ax[i][0].set_rasterized(True)
    # ax[i][1].set_rasterized(True)
fig.tight_layout()
fig.savefig('test_trace.pdf')
plt.close(fig)


'''
if PLOT_FIGS == 1:

    fig = plt.figure()

    ax1 = fig.add_subplot(311)
    ax1.plot(x, y[0])

    ax2 = fig.add_subplot(312)
    ax2.plot(x, y[1])

    ax3 = fig.add_subplot(313)
    ax3.plot(x, y[2])

    fig.tight_layout()
    fig.savefig('trace_gev_fin.eps')
    plt.close(fig)

    fig, ax = plt.subplots(nrows=1, ncols=3)
    ax[0].hist(y[0], 50, density=True)
    ax[1].hist(y[1], 50, density=True)
    ax[2].hist(y[1], 50, density=True)
    fig.tight_layout()

    fig.savefig('gev_fin_hist.eps')
    plt.close(fig)

    np.savetxt('gev_fin.csv', y)

    fig, ax = plt.subplots(nrows=3, ncols=1)
    sns.kdeplot(x=y[0], y=y[1], ax=ax[0])
    sns.kdeplot(x=y[0], y=y[2], ax=ax[1])
    sns.kdeplot(x=y[1], y=y[2], ax=ax[2])
    fig.savefig('gev_fin_kde.eps')
    plt.close(fig)

'''
