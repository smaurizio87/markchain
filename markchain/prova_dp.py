from matplotlib import pyplot as plt
from jax import numpy as np
from scipy.stats import norm
import numpy as nn

alpha = 100 # the precision parameter
size = 500 # The dimension of the space

# size >> alpha

# Prior sampling


def stick_breaking(alpha, size):
    z = nn.random.beta(1, alpha, size)
    h0 = np.insert(np.cumprod(1-z), 0, 1)[:-1]
    h0.at[-1].set(1.0-np.sum(h0))
    y = z*h0
    # Sample our "infinite dimensional parameter" F0
    x0 = nn.random.normal(loc=0, scale=1, size=size)

    def f(x):
        h = np.array([np.sum(y*np.heaviside(elem-x0, 0.0)) for elem in x])
        return h
    return f


h = [stick_breaking(alpha, size) for _ in range(10)]


x = np.arange(-4, 4, 0.01)

y1 = [elem(x) for elem in h]

y0 = norm.cdf(x)

fig = plt.figure()
ax = fig.add_subplot(111)
for r in y1:
    ax.plot(x, r, alpha=0.6, color='navy')
ax.plot(x, y0, color='r', ls='--')
fig.savefig('prova_prior.pdf')
plt.close(fig)

nobs = 25
loc_obs = 5
scale_obs = 1
