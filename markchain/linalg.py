from timeit import default_timer as timer
from scipy.linalg import svd
from jax import numpy as np
from jax import jit
# from jax.lax.linalg import svd
import numpy as nn


def my_svd(a):
    tol_abs = 1e-2
    ak = np.copy(a)
    n = np.shape(a)[0]
    v = np.identity(n)
    w = v
    conv = 0
    for k in range(50000):
        q, r1 = np.linalg.qr(ak)
        pt, st = np.linalg.qr(r1.T)
        s = st.T
        ak = s
        w = np.dot(w, q)
        v = np.dot(v, pt)
        err = np.max(np.abs(s - np.diag(np.diag(s)))/np.abs(s))
        if err < tol_abs:
            conv = 1
            break
    if not conv:
        raise Exception('SVD did not converge')
    ss = np.diag(s)
    sgn = ss/np.abs(ss)
    u = np.dot(w, np.diag(sgn))
    # print('A' ,sgn)
    # s1 = u[0][0]/np.abs(u[0][0])
    # u = u*s1
    # v = v * s1
    return (u, np.abs(ss), v.T)

def deflate(x):
    tol = 1e-8
    mx = np.max(np.abs(x))
    y = x*np.heaviside(np.abs(x/mx)-tol, 0.0)
    return y


def cho_dec(a):
    p, t = tridiag(a)
    # t1 = deflate(t)
    l, _ = cho_l(t1)
    # l1 = deflate(l)
    q, r1 = qr((p@l).T)
    r = r1.T
    # rs = deflate(r.T)
    return r


def cholesky(A):
    return np.linalg.cholesky(A)
    # mx = np.max(np.abs(a))
    # return cho_dec(A)
    # n = np.shape(A)[0]
    # u, s, vt = np.linalg.svd(A)
    # sut = np.diag(np.sqrt(s))@u.T
    # q, r = np.linalg.qr(sut)
    # z = np.array([r.T[:, k]*np.sign(r[k, k]) for k in range(n)]).T
    # return z


def cho_solve(L, y):
    alpha = np.linalg.solve(L.T, np.linalg.solve(L, y))
    return alpha


def tridiag(a):
    ak = np.copy(a)
    n = np.shape(a)[0]
    ide = np.eye(n)
    u0 = ide
    for k in range(n):
        u = ak[k, :]
        sgn = u[k+1]/np.abs(u[k+1])
        alpha = -sgn*np.sqrt(u[k+1:]@u[k+1:])
        r = np.sqrt((alpha**2 - u[k+1]*alpha)/2.0)
        z = (u[k+1]-alpha)/(2*r)
        v = u/(2*r)
        for l in range(k+1):
            v = v.at[l].set(0.0)
        v = v.at[k+1].set(z)
        p = ide - 2.0*np.tensordot(v, v, axes=0)
        u0 = np.dot(u0, p)
        an = np.dot(np.dot(p, ak), p)
        ak = an
    return u0, ak

def cho_tridiag(a):
    n = np.shape(a)[0]
    ide = np.eye(n)
    lmat = ide
    dvec = np.zeros(n)
    dvec = dvec.at[0].set(a[0, 0])
    lmat = lmat.at[1, 0].set(a[1, 0]/dvec[0])
    for k in range(1, n):
        dvec = dvec.at[k].set(a[k, k] - a[k, k-1]**2/dvec[k-1])
        if k+1 < n:
            lmat = lmat.at[k+1, k].set(a[k+1, k]/dvec[k])
    return dvec, lmat

def cho_l(a):
    d, l = cho_tridiag(a)
    return np.dot(l, np.diag(np.sqrt(d))), l

def ql(j, nmax, tol=1e-4):
    jn = np.copy(j)
    n = np.shape(j)
    w = np.eye(n[0])
    u0 = np.eye(n[0])
    s0 = 0.0*u0
    for n in range(nmax):
        d, u = cho_tridiag(jn-s0)
        l = np.dot(u, np.diag(np.sqrt(d)))
        u0 = np.dot(u0, u.T)
        s0 = np.min(d)*w
        jh = np.dot(l.T, l) + s0
        err = np.max(np.abs(jn - jh))
        if err < tol:
            break
        jn = jh
    return jn - s0, n, err, u0


def qr(a):
    n = np.shape(a)[0]
    ide = np.eye(n)
    q0 = ide
    r0 = a
    for k in range(n-1):
        ek = np.zeros(n)
        ek = ek.at[k].set(1)
        s = r0[k, k]/np.abs(r0[k, k])
        r = np.sqrt(r0[k:, k]@r0[k:, k])
        q = r0[:, k]
        for l in range(k):
            q = q.at[l].set(0)
        v = q + s*r*ek
        q = ide - 2.0*np.tensordot(v, v, axes=0)/np.dot(v, v)
        sgn = np.dot(q, r0)[k, k]
        r0 = np.dot(q, r0)
        q0 = np.dot(q0, q)
    return q0, r0


z = nn.random.normal(size=(3, 3))
a0 = z
a = a0@a0.T
p, t = tridiag(a)
print(p)
print(t)
t1 = deflate(t)
print(t1)
q, r = np.linalg.qr(a)
print(q)
print(r)

# l, _ = cho_l(t)
# print(a0)
# print(np.dot(p, l).T)
#
u, s, v = np.linalg.svd(a)
sut = np.diag(np.sqrt(s))@u.T
print(sut)
print(u)
w, _ = cho_l(t1)
print(w)
#
# # print(sut)
#
# q, r = np.linalg.qr(sut)
# #  print(q)
# print(r)
# _, r1 = np.linalg.qr((p@l).T)
# print(r1)
# qa, ra = qr((p@l).T)
# #  print(ra)
# ch1 = ra.T
# ch = np.linalg.cholesky(a)
# print(ch)
# print(ch1)
# ch2 = cholesky(a)
# print(ch2)
# #  print(ch@ch.T)
# #  print(a-ch1@ch1.T)
#
#
# # # # print(q)
# # # print(r)
# # # print(r@r.T)
