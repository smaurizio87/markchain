from jax import numpy as jnp
import numpy as np
from numpy import random
from scipy.stats import multivariate_normal, uniform, multivariate_t
from scipy.stats import norm, t, cauchy, beta, binom, gamma, poisson
from markchain.distributions import (Model, Data, Normal, Exp, StudentT,
                                 Cauchy, Beta, Binom, Gamma, Poisson,
                                     MultivariateNormal, ProdList, Uniform,
                                     Pow, MultivariateT
                                     )

tol = 1e-3
mu_0 = Normal(var_name='mu_0', loc=0, scale=2)
mu_1 = Normal(var_name='mu_1', loc=0, scale=2)

ls_0 = Normal(var_name='ls_0', loc=0, scale=1)
ls_1 = Normal(var_name='ls_1', loc=0, scale=1)
corr = Uniform(var_name='corr', low=-1, high=1)

sig_0 = Pow(var=ls_0, var_name='sig_0', n=2)
sig_1 = Pow(var=ls_1, var_name='sig_1', n=2)
sig_diag = ProdList(var_list=[ls_0, ls_1, corr], var_name='sig_diag')

x = MultivariateNormal(var_name='x',
                        loc=['mu_0', 'mu_1'],
                        cov=[['sig_0', 'sig_diag'],
                            ['sig_diag', 'sig_1']])

for _ in range(5):
    data = np.random.multivariate_normal(mean=np.array([0.5, -0.5]),
                                        cov = np.diag(np.array([1, 1])),
                                        size=5)

    model = Model([mu_0, mu_1, ls_0, corr, ls_1, sig_0, sig_1, sig_diag, x],
                var_list=['mu_0', 'mu_1', 'ls_0', 'ls_1', 'corr'],
                data=Data(var_name='x', var_values=data))

    def ff(x):
        rs = (np.sum(multivariate_normal.logpdf(
            data,
            mean=np.array([x[0], x[1]]),
            cov = np.array([[x[2]**2, x[2]*x[3]*x[4]],
                            [x[2]*x[3]*x[4], x[3]**2]])
                                                ))
            + norm.logpdf(x[0], loc=0, scale=2)
            + norm.logpdf(x[1], loc=0, scale=2)
            + norm.logpdf(x[2], loc=0, scale=1)
            + norm.logpdf(x[3], loc=0, scale=1)
            + uniform.logpdf(x[4], loc=-1, scale=2)
            )
        return rs
    x0 = np.array([-2, 4, 2, 4, 0.8])
    assert np.abs(model.log_posterior(x0) - ff(x0)) < tol

    x1 = x0**2
    assert np.abs(model.log_posterior(x1) - ff(x1)) < tol

    x2 = x0/2+0.3
    assert np.abs(model.log_posterior(x2) - ff(x2)) < tol

    x0 = np.array([6, 1, 8, 1, -0.2])
    assert np.abs(model.log_posterior(x0) - ff(x0)) < tol

    x1 = x0**2
    assert np.abs(model.log_posterior(x1) - ff(x1)) < tol

    x2 = x0/2+0.3
    assert np.abs(model.log_posterior(x2) - ff(x2)) < tol
