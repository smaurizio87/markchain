import numpy as nn
from jax import numpy as np
from jax.lax.linalg import eigh, svd

n = 4

a = np.array([[2.0, 1.0], [1.0, 3.0]])
a = nn.random.normal(size=(n, n))
a = a + a.T

a /= np.max(np.abs(a))

a += 3*np.diag(nn.random.normal(size=n))

# a = np.array([[2.0, 1.0], [1.0, 3.0]])

u, s, vt = svd(a)

# print(u)

def build_m(a):
    out = []
    zero = 0.0*a[0]
    for row in a:
        r = np.concatenate((zero, row))
        out.append(r)
    for row in a.T:
        r = np.concatenate((row, zero))
        out.append(r)
    return np.array(out)

m_a = build_m(a)

q1, r1 = np.linalg.qr(m_a)


def eigen_qr(A, iterations=500):
    Ak = np.copy(A)
    n = Ak.shape[0]
    QQ = np.eye(n)
    for k in range(iterations):
        # s_k is the last item of the first diagonal
        s = Ak[n-1, n-1]
        smult = s * np.eye(n)
        # pe perform qr and subtract smult
        Q, R = np.linalg.qr(np.subtract(Ak, smult))
        # we add smult back in
        Ak = np.add(R @ Q, smult)
        QQ = QQ @ Q
    return Ak, QQ

ak, qq = eigen_qr(a)
s1 = np.diag(ak)
# print(s)
# print(s1)

# print(v, w)

# print(u)
# print(vt)
#
# print(q1)
# print(r1)
# q2, r2 = np.linalg.qr(np.dot(a, a.T))
# print(q2, r2)
#
# q3, r3 = np.linalg.qr(np.dot(a.T, a))
# print(q2, r2)


def my_svd(a):
    tol_abs = 1e-3
    ak = np.copy(a)
    n = np.shape(a)[0]
    v = np.identity(n)
    w = v
    conv = 0
    for k in range(500000):
        q, r1 = np.linalg.qr(ak)
        pt, st = np.linalg.qr(r1.T)
        s = st.T
        ak = s
        w = np.dot(w, q)
        v = np.dot(v, pt)
        err = np.max(np.abs(s - np.diag(np.diag(s))))
        if err < tol_abs:
            conv = 1
            break
    if not conv:
        raise Exception('SVD did not converge')
    ss = np.diag(s)
    sgn = ss/np.abs(ss)
    u = np.dot(w, np.diag(sgn))
    # print('A' ,sgn)
    # s1 = u[0][0]/np.abs(u[0][0])
    # u = u*s1
    # v = v * s1
    return (u, np.abs(ss), v.T)




def bidiag(a):
    ak = np.copy(a)
    n = np.shape(a)[0]
    e0 = np.array([1.0]+[0.0]*(n-1))
    ide = np.eye(n)
    for k in range(n):
        x0 = ak[k:, k]
        x = np.array([0]*(k)+list(x0))
        s = len(x)
        # x = np.array([0]*(k) + list(x0))
        v0 = [0]*n
        v0[k] = 1
        u = x + x[k]/np.abs(x[k])*np.sqrt(x@x)*np.array(v0)
        v = u / np.sqrt(u@u)
        p = ide - 2.0*np.tensordot(v, v, axes=0)
        ak = np.dot(p, ak)
        # Up to here 0 below the main diagonal
        # Now let's do the same above the second main diagonal
        if k <= n-2:
            x1 = ak[k, k+1:]
            r = len(x1)
            # x = np.array([0]*(k+1)+list(x1))
            v0 = [0]*r
            v0[0] = 1
            # v0[k+1] = 1  # check this line and the following
            u1 = x1 + x1[0]/np.abs(x1[0])*np.sqrt(x1@x1)*np.array(v0)
            v1 = u1/np.sqrt(u1@u1)
            w = np.dot(ak[k:, k+1:], v1)
            r = -2.0*np.tensordot(w, v1, axes=0)
            ak = ak.at[k:, k+1:].add(r)
    return ak


def bidiag_mat(a):
    ak = np.copy(a)
    n = np.shape(a)[0]
    e0 = np.array([1.0]+[0.0]*(n-1))
    ide = np.eye(n)
    umat = np.eye(n)
    vmat = np.eye(n)
    for k in range(n):
        x0 = ak[k:, k]
        x = np.array([0]*(k)+list(x0))
        s = len(x)
        # x = np.array([0]*(k) + list(x0))
        v0 = [0]*n
        v0[k] = 1
        u = x + x[k]/np.abs(x[k])*np.sqrt(x@x)*np.array(v0)
        v = u / np.sqrt(u@u)
        p = ide - 2.0*np.tensordot(v, v, axes=0)
        umat = np.dot(umat, p)
        ak = np.dot(p, ak)
        # Up to here 0 below the main diagonal
        # Now let's do the same above the second main diagonal
        if k <= n-2:
            x1 = ak[k, k+1:]
            x = np.array([0]*(k+1)+list(x1))
            v0 = [0]*n
            v0[k+1] = 1
            u1 = x + x[k+1]/np.abs(x[k+1])*np.sqrt(x@x)*np.array(v0)
            v1 = u1/np.sqrt(u1@u1)
            p = ide - 2.0*np.tensordot(v1, v1.T, axes=0)
            vmat = np.dot(p, vmat)
            ak = np.dot(ak, p)
    return ak, umat, vmat

u1, s1, vt1 = my_svd(a)
print('U')
print(u)
print(u1)

print('V')
print(vt)
print(vt1)

print('S')
print(s)
print(s1)
# print(s, s1)
# print(vt, vt1)

print('###############')
q, r = np.linalg.qr(a)

print(bidiag(a))
# print(qa)
# print(ra)
# print(np.max(np.abs(qa - np.diag(np.diag(qa)))))
# print(np.diag(qa))
# print(bidiag_mat(a) - bidiag(a))
# print(un)
# print(vn)
# print(ra)
# print(un.T - np.linalg.inv(un))
# print(vn.T - np.linalg.inv(vn))
# print(ak)


def ortho_even(m, mu: float=0):
    a = np.copy(m)
    s = len(np.diag(m))
    a -= mu*np.eye(np.shape(a)[0])
    z = np.diag(a)[::2]
    n = len(z)
    q = np.eye(s)
    z1 = np.array([a[k, k+1] for k in range(0, 2*n, 2)])
    cth = z/(np.sqrt(z**2+z1**2))
    sth = -z1/(np.sqrt(z**2+z1**2))
    for k in range(n):
        if 2*k +1 < s:
            print('E',k)
            q = q.at[2*k, 2*k].set(cth[k])
            q = q.at[2*k+1, 2*k+1].set(cth[k])
            q = q.at[2*k, 2*k+1].set(sth[k])
            q = q.at[2*k+1, 2*k].set(-sth[k])
    r = np.dot(q.T, a)
    an = np.dot(r, q) + mu*np.eye(np.shape(a)[0])
    # mu = np.max(np.abs(np.diag(an)))
    #  r = np.dot(w, a)
    mu = an[-1, -1]
    return r, q, an, mu

def ortho_odd(m, mu: float=0):
    a = np.copy(m)
    s = len(np.diag(m))
    a -= mu*np.eye(np.shape(a)[0])
    z = np.diag(a)[1::2]
    n = len(z)
    q = np.eye(s)
    z1 = np.array([a[k, k+1] for k in range(1, 2*n, 2)])
    cth = z/(np.sqrt(z**2+z1**2))
    sth = -z1/(np.sqrt(z**2+z1**2))
    for k in range(1, n-1):
        if 2*k +1 < s:
            print('O',k)
            q = q.at[2*k, 2*k].set(cth[k])
            q = q.at[2*k+1, 2*k+1].set(cth[k])
            q = q.at[2*k, 2*k+1].set(sth[k])
            q = q.at[2*k+1, 2*k].set(-sth[k])
    r = np.dot(q.T, a)
    an = np.dot(r, q) + mu*np.eye(np.shape(a)[0])
    # mu = np.max(np.abs(np.diag(an)))
    mu = an[-1, -1]
    return r, q, an, mu

def ortho_single(m, k, mu: float=0, tol: float=1e-8):
    a = np.copy(m)
    s = len(np.diag(m))
    q = np.eye(s)
    if not mu:
        mu = np.sqrt(np.abs(a[k, k]*a[k+1, k+1] - a[k, k+1]*a[k+1, k]))
    a -= mu*np.eye(np.shape(a)[0])
    z = a[k, k]
    z1 = a[k, k+1]
    cth = z/(np.sqrt(z**2+z1**2))
    sth = -z1/(np.sqrt(z**2+z1**2))
    q = q.at[k, k].set(cth)
    q = q.at[k+1, k+1].set(cth)
    q = q.at[k, k+1].set(sth)
    q = q.at[k+1, k].set(-sth)
    r = np.dot(q.T, a)
    an = np.dot(r, q) + mu*np.eye(np.shape(a)[0])
    # mu = np.max(np.abs(np.diag(an)))
    mu = an[-1, -1]
    return r, q, an, mu


def ortho_single_1(m, k, mu: float=0, tol: float=1e-8):
    a = np.copy(m)
    s = len(np.diag(m))
    q = np.eye(s)
    if not mu:
        mu = np.sqrt(np.abs(a[k, k]*a[k+1, k+1] - a[k, k+1]*a[k+1, k]))
    a -= mu*np.eye(np.shape(a)[0])
    z = a[k, k]
    z1 = a[k+1, k]
    cth = z/(np.sqrt(z**2+z1**2))
    sth = -z1/(np.sqrt(z**2+z1**2))
    q = q.at[k, k].set(cth)
    q = q.at[k+1, k+1].set(cth)
    q = q.at[k, k+1].set(sth)
    q = q.at[k+1, k].set(-sth)
    r = np.dot(q.T, a)
    an = np.dot(r, q) + mu*np.eye(np.shape(a)[0])
    # mu = np.max(np.abs(np.diag(an)))
    mu = an[-1, -1]
    return r, q, an, mu

def deflate(a0, k, tol):
    a = a0.copy()
    if np.abs(a[k, k+1])<tol:
        a = a.at[k, k+1].set(0.0)
    if np.abs(a[k+1, k])<tol:
        a = a.at[k+1, k].set(0.0)
    return a


ak, un, vn = bidiag_mat(a)
q, r = np.linalg.qr(ak)
ak = r
an, un1, vn1 = bidiag_mat(ak)
print(ak)
mu = np.max(np.abs(an))


#  QR algorithm with Wilkinson shift
# TODO: implement double shift to improve convergence, include deflation
tol = 1e-5
for _ in range(40):
    for k in range(n-1):
        r, q, an, mu = ortho_single(an, k)
        # an, un1, vn1 = bidiag_mat(an)
        # r, q, an, mu = ortho_single_1(an, k)
        # an, un1, vn1 = bidiag_mat(an)
        an, un1, vn1 = bidiag_mat(an)

def svn_square(a):
    ak, un, vn = bidiag_mat(a)
    q, r = np.linalg.qr(ak)
    un = np.dot(un, q)
    ak = r
    an, un1, vn1 = bidiag_mat(ak)
    un = np.dot(un, un1)
    vn = np.dot(vn, vn1) # check
    ak = an
    for _ in range(50):
        for k in range(n-1):
            r, q, an, mu = ortho_single(ak, k)
            un = np.dot(un, q)
            ak = an
            an, un1, vn1 = bidiag_mat(an)
            un = np.dot(un, un1)
            vn = np.dot(vn, vn1)
            ak = an
    return un, np.diag(ak), vn

ua, sa, va = svn_square(a)

print(ua)
print(sa)
print(va)

# for _ in range(20):
#     r, q, an, mu = ortho_single_1(an, 0, mu)
    # an, un1, vn1 = bidiag_mat(an)
# for k in range(50):
#     r, q, an, mu = ortho_single(an, mu)
# #     an, un1, vn1 = bidiag_mat(an)
# print(r)
# print(q) # -> np.eye(n)
# print(an)
# # print(np.diag(an))
#
# tol = 1e-4
# print(ak)
# print(a)
